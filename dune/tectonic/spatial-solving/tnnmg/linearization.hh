#ifndef DUNE_TECTONIC_LINEARIZATION_HH
#define DUNE_TECTONIC_LINEARIZATION_HH

#include <cstddef>

#include <dune/common/typetraits.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/matrix-vector/algorithm.hh>

#include <dune/istl/matrixindexset.hh>

//#include <dune/tnnmg/functionals/bcqfconstrainedlinearization.hh>

#include "../../utils/debugutils.hh"

/**
 * derived from Dune::TNNMG::BoxConstrainedQuadraticFunctionalConstrainedLinearization<F,BV>
 */
template<class F, class BV>
class Linearization {
public:
  using Matrix = typename F::Matrix;
  using Vector = typename F::Vector;
  using BitVector = BV;
  using ConstrainedMatrix = Matrix;
  using ConstrainedVector = Vector;
  using ConstrainedBitVector = BitVector;

private:
  using This = Linearization<F,BV>;

  template <class T>
  void determineRegularityTruncation(const Vector& x, BitVector& truncationFlags, const T& truncationTolerance)
  {
    //std::cout << "x: " << x << std::endl;
    size_t blocksize = truncationFlags[0].size();

    size_t count = 0;

    for (size_t i = 0; i < x.size(); ++i) {
      //if (truncationFlags[i].all())
        //  continue;

      //std::cout << f_.phi().regularity(i, x[i]) << " xnorm: " << x[i].two_norm() << std::endl;

      if (f_.phi().regularity(i, x[i]) > truncationTolerance) {
        for (size_t j=1; j<blocksize; j++) {
            truncationFlags[i][j] = true;
        }
        count++;
      }
    }

     //std::cout << "regularityTruncation: " << count << std::endl;
  }

  template<class NV, class NBV, class T>
  static void determineTruncation(const NV& x, const NV& lower, const NV& upper, NBV&& truncationFlags, const T& truncationTolerance)
  {
    namespace H = Dune::Hybrid;
    H::ifElse(Dune::IsNumber<NV>(), [&](auto id){
      if ((id(x) <= id(lower)+truncationTolerance) || (id(x) >= id(upper) - truncationTolerance)) {
        id(truncationFlags) = true;
      }
    }, [&](auto id){
      H::forEach(H::integralRange(H::size(id(x))), [&](auto&& i) {
        This::determineTruncation(x[i], lower[i], upper[i], truncationFlags[i], truncationTolerance);
      });
    });
  }

  template<class NV, class NBV>
  static void truncateVector(NV& x, const NBV& truncationFlags)
  {
    namespace H = Dune::Hybrid;
    H::ifElse(Dune::IsNumber<NV>(), [&](auto id){
      if (id(truncationFlags))
        id(x) = 0;
    }, [&](auto id){
      H::forEach(H::integralRange(H::size(id(x))), [&](auto&& i) {
        This::truncateVector(x[i], truncationFlags[i]);
      });
    });
  }

  template<class NM, class RBV, class CBV>
  static void truncateMatrix(NM& A, const RBV& rowTruncationFlags, const CBV& colTruncationFlags)
  {
    namespace H = Dune::Hybrid;
    using namespace Dune::MatrixVector;
    H::ifElse(Dune::IsNumber<NM>(), [&](auto id){
      if(id(rowTruncationFlags) or id(colTruncationFlags))
        A = 0;
    }, [&](auto id){
      H::forEach(H::integralRange(H::size(id(rowTruncationFlags))), [&](auto&& i) {
        auto&& Ai = A[i];
        sparseRangeFor(Ai, [&](auto&& Aij, auto&& j) {
            This::truncateMatrix(Aij, rowTruncationFlags[i], colTruncationFlags[j]);
        });
      });
    });
  }

public:
  Linearization(const F& f, const BitVector& ignore) :
      f_(f),
      ignore_(ignore),
      obstacleTruncationTolerance_(1e-10),
      regularityTruncationTolerance_(1e8)
  {}

  void bind(const Vector& x) {
      //std::cout << "Linearization::bind()" << std::endl;

      auto&& A = f_.quadraticPart();
      auto&& phi = f_.phi();

      // determine which components to truncate
      // ----------------
      BitVector obstacleTruncationFlags = ignore_;
      BitVector regularityTruncationFlags = ignore_;

      //regularityTruncationFlags.unsetAll();
      //std::cout << "ignore truncation: " << truncationFlags_.count();

      determineTruncation(x, f_.lowerObstacle(), f_.upperObstacle(), obstacleTruncationFlags, obstacleTruncationTolerance_); // obstacle truncation

      //print(obstacleTruncationFlags, "obstacleTruncationFlags:");

      //std::cout << "    ignore truncation: " << ignore_.count() << std::endl;
      //std::cout << "    obstacle truncation: " << truncationFlags_.count() << std::endl;
      //std::cout << " " << obstacleTruncationTolerance_;

      determineRegularityTruncation(x, regularityTruncationFlags, regularityTruncationTolerance_); // truncation due to regularity deficit of nonlinearity

      //std::cout << "    regularity truncation: " << truncationFlags_.count() << std::endl;
      //std::cout << " " << regularityTruncationTolerance_;

      //print(regularityTruncationFlags, "regularityTruncationFlags:");

      truncationFlags_ = obstacleTruncationFlags;
      size_t blocksize = truncationFlags_[0].size();
      for (size_t i=0; i<truncationFlags_.size(); i++) {
          for (size_t j=0; j<blocksize; j++) {
              truncationFlags_[i][j] = truncationFlags_[i][j] or regularityTruncationFlags[i][j];
          }
      }

      // compute hessian
      // ---------------

      // construct sparsity pattern for linearization
      Dune::MatrixIndexSet indices(A.N(), A.M());
      indices.import(A);
      phi.addHessianIndices(indices);

      // construct matrix from pattern and initialize it
      indices.exportIdx(hessian_);
      hessian_ = 0.0;

      // quadratic part
      for (size_t i = 0; i < A.N(); ++i) {
        auto const end = std::end(A[i]);
        for (auto it = std::begin(A[i]); it != end; ++it)
          hessian_[i][it.index()] += *it;
      }

      //truncateMatrix(hessian_, obstacleTruncationFlags, obstacleTruncationFlags);

      //print(hessian_, "Hessian: ");
      //std::cout << "--------" << (double) hessian_[21][21] << "------------" << std::endl;

      //std::cout << hessian_[0][0] << std::endl;

      auto B = hessian_;

      //print(x, "x: ");

      // nonlinearity part
      phi.addHessian(x, hessian_);

      //truncateMatrix(hessian_, regularityTruncationFlags, regularityTruncationFlags);

      //std::cout << x[0] << " " << hessian_[0][0] << std::endl;

      B -= hessian_;
      //truncateMatrix(B, regularityTruncationFlags, regularityTruncationFlags);
      //print(B, "phi hessian: ");

      // compute gradient
      // ----------------

      // quadratic part
      negativeGradient_ = derivative(f_)(x); // A*x - b

      //print(negativeGradient_, "gradient: ");
      auto C = negativeGradient_;


      // nonlinearity part
      phi.addGradient(x, negativeGradient_);

      C -= negativeGradient_;
      //truncateVector(C, truncationFlags_);

      /*for (size_t i=0; i<C.size(); i++) {
          std::cout << "dof " << i << std::endl;
          std::cout << "truncation: " << regularityTruncationFlags[i] << std::endl;
          std::cout << "C[i]: " << C[i] << std::endl;
      }*/
      //print(truncationFlags_, "truncationFlags_: ");
      //print(C, "phi gradient: ");

      // -grad is needed for Newton step
      negativeGradient_ *= -1;

      // truncate gradient
      truncateVector(negativeGradient_, truncationFlags_);
      truncateMatrix(hessian_, truncationFlags_, truncationFlags_);

      //print(hessian_, "hessian: ");
      //print(negativeGradient_, "negativeGradient: ");

      //print(truncationFlags_, "truncationFlags:");
  }


  void extendCorrection(ConstrainedVector& cv, Vector& v) const {
    v = cv;
    truncateVector(v, truncationFlags_);
  }

  /*const BitVector& truncated() const {
    return truncationFlags_;
  }*/

  const auto& negativeGradient() const {
    return negativeGradient_;
  }

  const auto& hessian() const {
    return hessian_;
  }

private:
  const F& f_;
  const BitVector& ignore_;

  double obstacleTruncationTolerance_;
  double regularityTruncationTolerance_;

  Vector negativeGradient_;
  Matrix hessian_;
  BitVector truncationFlags_;
};

#endif
