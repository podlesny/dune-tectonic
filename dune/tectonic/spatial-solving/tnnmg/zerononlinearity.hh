#ifndef DUNE_TECTONIC_ZERO_NONLINEARITY_HH
#define DUNE_TECTONIC_ZERO_NONLINEARITY_HH

#include <dune/solvers/common/interval.hh>

/** \file
 * \brief A dummy nonlinearity class representing the zero function
 */

class ZeroNonlinearity
{
public:
    ZeroNonlinearity()
    {}

    const auto& restriction(size_t i) const {
        return *this;
    }

    /** \brief Returns zero */
    template <class VectorType>
    double operator()(const VectorType& v) const
    {
        return 0.0;
    }

    template <class VectorType>
    void addGradient(const VectorType& v, VectorType& gradient) const {}

    template <class VectorType, class MatrixType>
    void addHessian(const VectorType& v, MatrixType& hessian) const {}

    template <class IndexSet>
    void addHessianIndices(IndexSet& indices) const {}

    /** \brief Returns the interval \f$ [0,0]\f$ */
    void subDiff(int i, double x, Dune::Solvers::Interval<double>& D) const
    {
        D[0] = 0.0;
        D[1] = 0.0;
    }

    template <class VectorType>
    void directionalSubDiff(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& subdifferential) const
    {
        subdifferential[0] = 0.0;
        subdifferential[1] = 0.0;
    }

    /** \brief Returns 0 */
    template <class VectorType>
    double regularity(int i, const VectorType& x) const
    {
        return 0.0;
    }

    void domain(int i, Dune::Solvers::Interval<double>& dom) const
    {
        dom[0] = -std::numeric_limits<double>::max();
        dom[1] = std::numeric_limits<double>::max();
        return;
    }

    template <class VectorType>
    void directionalDomain(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& dom) const
    {
        dom[0] = -std::numeric_limits<double>::max();
        dom[1] = std::numeric_limits<double>::max();
        return;
    }

    template <class BitVector>
    void setIgnore(const BitVector& ignore) {}

    template <class StateVector>
    void updateAlpha(const StateVector& alpha) {}

    template <class BitVector>
    void setActiveNodes(const BitVector& active) {}
};

#endif

