add_custom_target(tectonic_dune_spatial-solving_tnnmg SOURCES
  functional.hh
  linearcorrection.hh
  linearization.hh
  linesearchsolver.hh
  localbisectionsolver.hh
  zerononlinearity.hh
)

#install headers
install(FILES
  functional.hh
  linearcorrection.hh
  linearization.hh
  linesearchsolver.hh
  localbisectionsolver.hh
  zerononlinearity.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/tectonic)
