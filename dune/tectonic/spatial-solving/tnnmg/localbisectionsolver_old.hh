// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TECTONIC_LOCALBISECTIONSOLVER_HH
#define DUNE_TECTONIC_LOCALBISECTIONSOLVER_HH

#include <dune/matrix-vector/axpy.hh>

#include <dune/tnnmg/localsolvers/scalarobstaclesolver.hh>
#include <dune/tnnmg/problem-classes/bisection.hh>

#include "functional.hh"

#include "../../utils/debugutils.hh"

/**
 * \brief A local solver for quadratic obstacle problems with nonlinearity
 * using bisection
 */
class LocalBisectionSolver
{
public:
  template<class Vector, class Functional, class BitVector>
  void operator()(Vector& x, const Functional& f, const BitVector& ignore) const {
      double safety = 1e-14; //or (f.upperObstacle()-f.lowerObstacle()<safety)
      if (ignore.all()) {
          return;
      }

      auto maxEigenvalue = f.maxEigenvalues();

      auto origin = f.origin();
      auto linearPart = f.originalLinearPart();

      Dune::MatrixVector::addProduct(linearPart, maxEigenvalue, origin);

      for (size_t j = 0; j < ignore.size(); ++j)
        if (ignore[j])
          linearPart[j] = 0; // makes sure result remains in subspace after correction
        else
          origin[j] = 0; // shift affine offset

      double const linearPartNorm = linearPart.two_norm();
      if (linearPartNorm <= 0.0)
        return;
      linearPart /= linearPartNorm;

      std::cout << "maxEigenvalue " << maxEigenvalue << std::endl;
      print(linearPart, "linearPart:");
      std::cout << "linearPartNorm " << linearPartNorm << std::endl;
      print(origin, "origin:");

      using Nonlinearity = std::decay_t<decltype(f.phi())>;
      using Range = std::decay_t<decltype(f.maxEigenvalues())>;
      using FirstOrderFunctional = FirstOrderFunctional<Nonlinearity, Vector, Range>;

      auto lower = f.originalLowerObstacle();
      auto upper = f.originalUpperObstacle();

      //print(lower, "lower:");
      //print(upper, "upper:");

      FirstOrderFunctional firstOrderFunctional(maxEigenvalue, linearPartNorm, f.phi(),
                                                lower, upper,
                                                origin, linearPart);

      //std::cout << "lower: " << firstOrderFunctional.lowerObstacle() << " upper: " << firstOrderFunctional.upperObstacle() << std::endl;

      // scalar obstacle solver without nonlinearity
      typename Functional::Range alpha;
      //Dune::TNNMG::ScalarObstacleSolver obstacleSolver;
      //obstacleSolver(alpha, firstOrderFunctional, false);

      //direction *= alpha;

      /*const auto& A = f.quadraticPart();
      std::cout << "f.quadratic(): " << std::endl;
      for (size_t i=0; i<A.N(); i++) {
            for (size_t j=0; j<A.M(); j++) {
                std::cout << A[i][j] << " ";
            }
            std::cout << std::endl;
      }*/
      //std::cout << f.quadraticPart() << std::endl;
      //std::cout << "A: " << directionalF.quadraticPart() << " " << (directionalF.quadraticPart()==f.quadraticPart()[0][0]) << std::endl;
      /*std::cout << "b: " << directionalF.linearPart() << std::endl;
      std::cout << "domain: " << directionalF.domain()[0] << " " << directionalF.domain()[1] << std::endl;
      auto D = directionalF.subDifferential(0);
      std::cout << "subDiff at x: " << D[0] << " " << D[1] << std::endl;*/

      //std::cout << "domain: " << firstOrderFunctional.domain()[0] << " " << firstOrderFunctional.domain()[1] << std::endl;


      const auto& domain = firstOrderFunctional.domain();
      if (std::abs(domain[0]-domain[1])>safety) {
        int bisectionsteps = 0;
        const Bisection globalBisection(0.0, 1.0, 0.0, 0.0);

        alpha = globalBisection.minimize(firstOrderFunctional, 0.0, 0.0, bisectionsteps);
      } else {
          alpha = domain[0];
      }

      std::cout << "alpha: " << alpha << std::endl;
      linearPart *= alpha;

      std::cout << linearPart << std::endl;

#ifdef NEW_TNNMG_COMPUTE_ITERATES_DIRECTLY
      if (std::abs(alpha)> safety) {
        x = origin;
        x += linearPart;
      } else {
        x = f.origin();
      }
#else
      if (std::abs(alpha)> safety) {
        x = origin;
        x += linearPart;
        x -= f.origin();
      }
#endif
      std::cout << "new x: " << x << std::endl;
  }
};

#endif




