#ifndef MY_DIM
#error MY_DIM unset
#endif

#include <dune/solvers/solvers/loopsolver.hh>

#include "solverfactory_tmpl.cc"

using MyLinearSolver = Dune::Solvers::LoopSolver<Vector, BitVector>;

template class SolverFactory<MyFunctional, BitVector>;
template void SolverFactory<MyFunctional, BitVector>::build<MyLinearSolver>(std::shared_ptr<MyLinearSolver>&);

template class SolverFactory<MyZeroFunctional, BitVector>;
template void SolverFactory<MyZeroFunctional, BitVector>::build<MyLinearSolver>(std::shared_ptr<MyLinearSolver>&);


