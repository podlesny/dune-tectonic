#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../explicitgrid.hh"
#include "../explicitvectors.hh"

#include "../data-structures/friction/globalfriction.hh"
#include "tnnmg/functional.hh"
#include "tnnmg/zerononlinearity.hh"

#include "solverfactory.hh"

using MyLinearSolver = Dune::Solvers::LoopSolver<Vector, BitVector>;
using MyGlobalFriction = GlobalFriction<Matrix, Vector>;

using MyFunctional = Functional<Matrix&, Vector&, MyGlobalFriction&, Vector&, Vector&, double>;
using MySolverFactory = SolverFactory<MyFunctional, BitVector>;

using MyZeroFunctional = Functional<Matrix&, Vector&, ZeroNonlinearity&, Vector&, Vector&, double>;
using MyZeroSolverFactory = SolverFactory<MyZeroFunctional, BitVector>;
