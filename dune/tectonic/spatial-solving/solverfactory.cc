#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/solvers/common/wrapownshare.hh>
#include <dune/solvers/iterationsteps/blockgssteps.hh>
#include <dune/solvers/solvers/umfpacksolver.hh>

#include "solverfactory.hh"

#include "../utils/debugutils.hh"

template <class Functional, class BitVector>
SolverFactory<Functional, BitVector>::SolverFactory(
    const Dune::ParameterTree& parset,
    Functional& J,
    const BitVector& ignoreNodes) :
        parset_(parset),
        J_(Dune::Solvers::wrap_own_share<const Functional>(std::forward<Functional>(J))),
        ignoreNodes_(ignoreNodes)
{}

template <class Functional, class BitVector>
SolverFactory<Functional, BitVector>::SolverFactory(
    const Dune::ParameterTree& parset,
    std::shared_ptr<Functional> J,
    const BitVector& ignoreNodes) :
        parset_(parset),
        J_(Dune::Solvers::wrap_own_share<const Functional>(J)),
        ignoreNodes_(ignoreNodes)
{}

template <class Functional, class BitVector>
template <class LinearSolver>
void SolverFactory<Functional, BitVector>::build(std::shared_ptr<LinearSolver>& linearSolver) {
    nonlinearSmoother_ = std::make_shared<NonlinearSmoother>(*J_, dummyIterate_, LocalSolver());

    tnnmgStep_ = std::make_shared<Step>(*J_, dummyIterate_, nonlinearSmoother_, linearSolver, DefectProjection(), LineSearchSolver());
    tnnmgStep_->setPreSmoothingSteps(parset_.get<int>("main.pre"));
    tnnmgStep_->setIgnore(ignoreNodes_);
}

template <class Functional, class BitVector>
void SolverFactory<Functional, BitVector>::setProblem(Vector& x) {
    nonlinearSmoother_->setProblem(x);
    tnnmgStep_->setProblem(x);
}


template <class Functional, class BitVector>
auto SolverFactory<Functional, BitVector>::step()
-> std::shared_ptr<Step> {
    return tnnmgStep_;
}

#include "solverfactory_ex.cc"
