#ifndef SRC_SPATIAL_SOLVING_FIXEDPOINTITERATOR_HH
#define SRC_SPATIAL_SOLVING_FIXEDPOINTITERATOR_HH

#include <memory>

#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/solvers/norms/norm.hh>
#include <dune/solvers/solvers/solver.hh>

#include <dune/fufem/boundarypatch.hh>

#include <dune/contact/assemblers/nbodyassembler.hh>

#include "solverfactory.hh"

struct FixedPointIterationCounter {
  size_t iterations = 0;
  size_t multigridIterations = 0;

  void operator+=(FixedPointIterationCounter const &other);
};

std::ostream &operator<<(std::ostream &stream,
                         FixedPointIterationCounter const &fpic);

template <class Factory, class NBodyAssembler, class Updaters, class ErrorNorms>
class FixedPointIterator {
  using ScalarVector = typename Updaters::StateUpdater::ScalarVector;
  using Vector = typename Factory::Vector;
  using Matrix = typename Factory::Matrix;

  using Nonlinearity = typename Factory::Functional::Nonlinearity;

  const static int dims = Vector::block_type::dimension;

  using IgnoreVector = typename Factory::BitVector;
 // using Nonlinearity = typename ConvexProblem::NonlinearityType;
 //  using DeformedGrid = typename Factory::DeformedGrid;

public:
  using GlobalFriction = Nonlinearity;
  using BitVector = Dune::BitSetVector<1>;

private:
  void split(const Vector& v, std::vector<Vector>& v_rel) const;
  bool displacementCriterion(const Updaters& updaters, std::vector<Vector>& last_u) const;
  bool stateCriterion(const std::vector<ScalarVector>& alpha, const std::vector<ScalarVector>& newAlpha) const;

public:
  FixedPointIterator(const Dune::ParameterTree& parset,
                     const NBodyAssembler& nBodyAssembler,
                     const IgnoreVector& ignoreNodes,
                     GlobalFriction& globalFriction,
                     const std::vector<const BitVector*>& bodywiseNonmortarBoundaries,
                     const ErrorNorms& errorNorms);

  template <class LinearSolver>
  FixedPointIterationCounter run(Updaters updaters,
                                 std::shared_ptr<LinearSolver>& linearSolver,
                                 const std::vector<Matrix>& velocityMatrices,
                                 const std::vector<Vector>& velocityRHSs,
                                 std::vector<Vector>& velocityIterates);

private:
  const Dune::ParameterTree& parset_;
  const NBodyAssembler& nBodyAssembler_;
  const IgnoreVector& ignoreNodes_;

  GlobalFriction& globalFriction_;
  const std::vector<const BitVector*>& bodywiseNonmortarBoundaries_;


  size_t fixedPointMaxIterations_;
  double fixedPointTolerance_;
  double lambda_;
  const Dune::ParameterTree& solverParset_;

  const ErrorNorms& errorNorms_;
};

#endif
