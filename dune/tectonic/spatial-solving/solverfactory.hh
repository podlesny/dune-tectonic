#ifndef SRC_SPATIAL_SOLVING_SOLVERFACTORY_HH
#define SRC_SPATIAL_SOLVING_SOLVERFACTORY_HH

//#define NEW_TNNMG_COMPUTE_ITERATES_DIRECTLY

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/cgstep.hh>

#include <dune/tnnmg/iterationsteps/tnnmgstep.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>
#include <dune/tnnmg/projections/obstacledefectprojection.hh>
#include <dune/tnnmg/localsolvers/scalarobstaclesolver.hh>

#include "tnnmg/linearization.hh"
#include "tnnmg/linesearchsolver.hh"
#include "tnnmg/localbisectionsolver.hh"

template <class FunctionalTEMPLATE, class BitVectorType>
class SolverFactory {
public:
    using Functional = FunctionalTEMPLATE;
    using Matrix = typename Functional::Matrix;
    using Vector = typename Functional::Vector;
    using BitVector = BitVectorType;

    using LocalSolver = LocalBisectionSolver;
    using NonlinearSmoother = Dune::TNNMG::NonlinearGSStep<Functional, LocalBisectionSolver, BitVector>;
    using Linearization = Linearization<Functional, BitVector>;
    using DefectProjection = typename Dune::TNNMG::ObstacleDefectProjection;

    using Step = Dune::TNNMG::TNNMGStep<Functional, BitVector, Linearization, DefectProjection, LineSearchSolver>;

  SolverFactory(const Dune::ParameterTree&,
                Functional&,
                const BitVector&);

  SolverFactory(const Dune::ParameterTree&,
                std::shared_ptr<Functional>,
                const BitVector&);

  template <class LinearSolver>
  void build(std::shared_ptr<LinearSolver>& linearSolver);

  void setProblem(Vector& x);

  auto step() -> std::shared_ptr<Step>;

private:
  const Dune::ParameterTree& parset_;

  Vector dummyIterate_;
  std::shared_ptr<const Functional> J_;
  const BitVector& ignoreNodes_;

  // nonlinear smoother
  std::shared_ptr<NonlinearSmoother> nonlinearSmoother_;

  // TNNMGStep
  std::shared_ptr<Step> tnnmgStep_;
};

#endif
