// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_TECTONIC_SPATIAL_SOLVING_CONTACT_N_BODY_MORTAR_ASSEMBLER_HH
#define DUNE_TECTONIC_SPATIAL_SOLVING_CONTACT_N_BODY_MORTAR_ASSEMBLER_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/promotiontraits.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/solvers/common/boxconstraint.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/contact/assemblers/contactassembler.hh>
#include <dune/contact/common/couplingpair.hh>
#include <dune/contact/projections/contactprojection.hh>

#include "dualmortarcoupling.hh"

#ifdef HAVE_DUNE_GRID_GLUE
#include <dune/grid-glue/merging/merger.hh>
#endif


/**  \brief Assembler for mortar discretized contact problems with arbitrary number of bodies.
 *
 *  \tparam GridType Type of the corresponding grids.
 *  \tparam VectorType The vector type for the displacements.
 */
template <class GridType, class VectorType>
class NBodyAssembler : public Dune::Contact::ContactAssembler<GridType::dimension, typename VectorType::field_type>
{
protected:
    enum {dim = GridType::dimension};

    using field_type = typename Dune::PromotionTraits<typename VectorType::field_type,
                                                typename GridType::ctype>::PromotedType;

    using CouplingType = DualMortarCoupling<field_type, GridType>;

    using MatrixBlock = Dune::FieldMatrix<field_type, dim, dim>;
    using MatrixType = Dune::BCRSMatrix<MatrixBlock>;

    using RowType =  typename MatrixType::row_type;
    using ConstColumnIterator = typename RowType::ConstIterator;

    using LeafBoundaryPatch = BoundaryPatch<typename GridType::LeafGridView>;

public:
    /** \brief Construct assembler from number of couplings and grids.
     *
     *  \param nGrids The number of involved bodies.
     *  \param nCouplings The number of couplings.
     */
    NBodyAssembler(int nGrids, int nCouplings, field_type coveredArea = 0.99) :
      coveredArea_(coveredArea)
    {
        grids_.resize(nGrids);

        crosspoints_.resize(nGrids);

        coupling_.resize(nCouplings);
        contactCoupling_.resize(nCouplings);
        // initialise the couplings with DualMortarCoupling
        for (int i=0; i<nCouplings; i++)
            contactCoupling_[i] = std::make_shared<CouplingType>();
    }

    /** \brief Get the number of couplings.*/
    int nCouplings() const {return coupling_.size();}

    /** \brief Get the number of involved bodies.*/
    int nGrids() const {return grids_.size();}

    /** \brief Get the total number of degrees of freedom of the leaf grids. */
    int numDofs() const {
        int n=0;
        for (int i=0; i<nGrids(); i++)
            n += grids_[i]->size(dim);
        return n;
    }

    void setCrosspoints();

    /** \brief Setup the patches for the contact boundary and compute the obstacles. */
    void assembleObstacle();

    /** \brief Assemble the mortar matrices and compute the basis transformation.*/
    void assembleTransferOperator();

    /** \brief Turn initial solutions from nodal basis to the transformed basis.
     *         i.e. transformedX = O*B^{-T}nodalX
     *  \param x Initial solution vector containing nodal coefficients.
     *  \param transformedX Coefficient vector for all bodies in mortar transformed coordinates.
     */
    template <class VectorTypeContainer>
    void nodalToTransformed(const VectorTypeContainer& x,
                            VectorType& transformedX) const;

    /** \brief Compute stiffness matrices in mortar transformed coordinates.
     *         i.e. transformedA = O*B*nodalA*B^T*O^T
     *  \param submat Stiffness matrices w.r.t the nodal finite element basis.
     *  \param totalMatrix Reference to mortar transformed stiffness matrix for all bodies.
     */
    void assembleJacobian(const std::vector<const MatrixType*>& submat,
                          MatrixType& totalMatrix) const;

    /** \brief Compute the right hand side in mortar transformed coordinates.
     *         i.e. totalRhs = O*B*nodalRhs
     *  \param  rhs Right hand side coefficients w.r.t. the nodal finite element basis.
     *  \param  totalRhs Reference to coefficient vector w.r.t. the transformed basis.
     */
    template <class VectorTypeContainer>
    void assembleRightHandSide(const VectorTypeContainer& rhs,
                               VectorType& totalRhs) const;

    /** \brief Transform a vector from local coordinates to canonical coordinates.
     *
     *  \param totalX Coefficient vector of the mortar transformed basis.
     *  \param x    Reference to target vector for the standard nodal coefficients of each body.
     */
    template <class VectorTypeContainer>
    void postprocess(const VectorType& totalX, VectorTypeContainer& x) const;

    /** \brief Concatenate a family of vectors .
     *
     * \param parts A vector of vectors.
     * \param whole The vector to contain the concatenated family
     */
    template <class VectorTypeContainer>
    static void concatenateVectors(const VectorTypeContainer& parts, VectorType& whole);

    /** \brief Get the contact couplings. */
    const auto& getContactCouplings() const {
        return contactCoupling_;
    }

    /** \brief Get the contact couplings. */
    auto& getContactCouplings() {
        return contactCoupling_;
    }

    /** \brief Set the contact couplings. */
    void setContactCouplings(std::vector<std::shared_ptr<CouplingType> >& contactCouplings) {

        contactCoupling_.resize(contactCouplings.size());
        for (size_t i=0; i<contactCouplings.size(); i++)
            contactCoupling_[i] = contactCouplings[i];
    }

    /** \brief Get the transposed of the mortar transformation matrix B^T.*/
    const MatrixType& getTransformationMatrix() const {return BT_;}

    /** \brief Get the grids. */
    const std::vector<const GridType*> getGrids() const { return grids_; }

    /** \brief Set the grids. */
    void setGrids(const std::vector<const GridType*>& grids) {grids_ = grids;}

    /** \brief Set grids. */
    void setCoupling(const Dune::Contact::CouplingPair<GridType, GridType, field_type>& coupling, size_t i) {
        coupling_[i] = coupling;
    }

    /** \brief Get reference to i'th coupling. */
    const auto& getCoupling(size_t i) const {return coupling_[i];}

    void setDirichletVertices(std::vector<std::shared_ptr<Dune::BitSetVector<1>>> dirichletVertices) {
        dirichletVertices_ = dirichletVertices;
    }
protected:
    /** \brief Compute the transposed mortar transformation matrix. */
    void computeTransformationMatrix();

    /** \brief Setup the Householder reflections. */
    void assembleHouseholderReflections();

public:
    std::vector<std::shared_ptr<Dune::BitSetVector<1>>> dirichletVertices_;

    /** \brief Bitvector for all bodies whose flags are set if a dof has an obstacle.*/
    Dune::BitSetVector<dim> totalHasObstacle_;

    /** \brief Obstacles for all bodies on the leafview.*/
    std::vector<BoxConstraint<field_type,dim> > totalObstacles_;

    std::vector<std::set<size_t>> crosspoints_;

    /** \brief The mortar couplings between grid pairs */
    std::vector<std::shared_ptr<CouplingType> > contactCoupling_;

    /** \brief The coupling pairs. */
    std::vector<Dune::Contact::CouplingPair<GridType,GridType,field_type> > coupling_;

    /** \brief Vector containing the involved grids. */
    std::vector<const GridType*> grids_;

    /** \brief The transposed of the mortar transformation matrix. */
    MatrixType BT_;

    /** \brief Dismiss all faces that are not at least covered by the grid-glue projection for this
     *         much percentage ranging between one - for total coverage and zero for taking all faces.
     */
    field_type coveredArea_;
};

#include "nbodyassembler.cc"

#endif
