// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef HIERARCHIC_LEVEL_ITERATOR_HH
#define HIERARCHIC_LEVEL_ITERATOR_HH

#include <dune/common/fvector.hh>
#include <dune/common/iteratorfacades.hh>

#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/referenceelements.hh>

/** \brief Hierarchic leaf iterator.
 *
 *  This iterator loops over all children of a given coarse element and only returns the ones that are leaf.
 *  If the starting element is leaf itself, then the returned iterator returns the element itself.
 *  This class also provides a geometry, mapping local coordinates of the children to local coordinates
 *  in the coarse element. 
*/
template <class GridImp>
class HierarchicLevelIterator :
    public Dune::ForwardIteratorFacade<HierarchicLevelIterator<GridImp>, const typename GridImp::template Codim<0>::Entity>
{
    typedef typename GridImp::template Codim<0>::Entity Element;
    typedef typename GridImp::HierarchicIterator HierarchicIterator;
    enum {dim = GridImp::dimension};
    enum {dimworld = GridImp::dimensionworld};
public:
    typedef Dune::CachedMultiLinearGeometry<typename GridImp::ctype, dim, dimworld> LocalGeometry;

    enum PositionFlag {begin, end};

    HierarchicLevelIterator(const GridImp& grid, const Element& element, 
                            PositionFlag flag, int maxlevel, bool nested = true)
        : element_(element), maxlevel_(maxlevel), hIt_(element.hend(maxlevel_)),
            flag_(flag), nested_(nested)
    {

        // if the element itself is leaf, then we don't have to iterate over the children
        if (flag==begin && !(element_.level()==maxlevel_)) {
            hIt_ = element_.hbegin(maxlevel_);

            //NOTE This class by now assumes that possible non-nestedness of the grid levels only arises
            // due to boundary parametrisation
            if (!nested_)  {
                // Check if the element is a boundary element, and set the nested flag correspondingly
                // If the element is not at the boundary, then we can neglect the nested flag
                typename GridImp::LevelIntersectionIterator lIt = grid.levelGridView(element.level()).ibegin(element);
                typename GridImp::LevelIntersectionIterator lEndIt = grid.levelGridView(element.level()).ibegin(element);
                nested_ = true;
                for (; lIt != lEndIt; ++lIt)
                    if (lIt->boundary()) {
                        nested_ = false;
                        break;
                    }
            }

            if (!(hIt_->level()==maxlevel_))
                increment();
        }
    }

    //! Copy constructor
    HierarchicLevelIterator(const HierarchicLevelIterator<GridImp>& other) :
        element_(other.element_),maxlevel_(other.maxlevel_), hIt_(other.hIt_),
        flag_(other.flag_), nested_(other.nested_)
    {}

    //! Equality
    bool equals(const HierarchicLevelIterator<GridImp>& other) const {
        return  (element_ == other.element_)  && 
                ((flag_==other.flag_ && flag_==end) || (hIt_ == other.hIt_ && flag_==begin && other.flag_==flag_));
    }

    //! Prefix increment
    void increment() {

        HierarchicIterator hEndIt = element_.hend(maxlevel_);

        if (hIt_ == hEndIt) {
            flag_ = end;
            return;
        }

        // Increment until we hit a leaf child element
        do {
            ++hIt_;
        } while(hIt_ != hEndIt && (!(hIt_->level()==maxlevel_)));
    
        if (hIt_ == hEndIt)
            flag_ = end;
    }

    //! Dereferencing
    const Element& dereference() const {
        if (flag_ == end)
            DUNE_THROW(Dune::Exception,"HierarchicLevelIterator: Cannot dereference end iterator!");
        return (element_.level()==maxlevel_) ? element_ : *hIt_;
    }

    //! Compute the local geometry mapping from the leaf child to the starting element
    LocalGeometry geometryInAncestor() {

        //NOTE: We assume here that the type of the child and the ancestors are the same!
        const Dune::ReferenceElement<typename GridImp::ctype, dim>& ref = Dune::ReferenceElements<typename GridImp::ctype,dim>::general(element_.type());

        // store local coordinates of the leaf child element within the coarse starting element
        std::vector<Dune::FieldVector<typename GridImp::ctype,dim> > corners(ref.size(dim));
        for (int i=0; i<corners.size(); i++)
            corners[i] = ref.position(i,dim);

        // If the element is leaf, then return an Identity mapping
        if (element_.level()==maxlevel_)
            return LocalGeometry(ref,corners);

        if (nested_) {
            const typename Element::Geometry leafGeom = hIt_->geometry();
            const typename Element::Geometry coarseGeom = element_.geometry();

            for (int i=0; i<corners.size();i++) 
                corners[i] = coarseGeom.local(leafGeom.corner(i));
            return LocalGeometry(ref,corners);
        }
 
        Element father(*hIt_);
        while (father != element_) {

            const typename Element::LocalGeometry fatherGeom = hIt_->geometryInFather();
            father = father->father();

            for (int i=0; i<corners.size(); i++)
                corners[i] = fatherGeom.global(corners[i]);
        }

        return LocalGeometry(ref,corners);
    }

private:

    //! The starting element
    const Element element_;

    //! The grids maxlevel
    int maxlevel_;

    //! The actual hierarchic iterator
    HierarchicIterator hIt_;

    //! Position flag
    PositionFlag flag_;

    //! Flag that is set true if the grid levels are conforming (i.e. no parametrised boundaries)
    bool nested_;

};
#endif
