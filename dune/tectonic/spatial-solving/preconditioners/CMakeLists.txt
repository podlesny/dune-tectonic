add_custom_target(tectonic_dune_spatial-solving_preconditioners SOURCES
  hierarchicleveliterator.hh
  levelpatchpreconditioner.hh
  patchproblem.hh
  multilevelpatchpreconditioner.hh
  supportpatchfactory.hh
)

#install headers
install(FILES
  hierarchicleveliterator.hh
  levelpatchpreconditioner.hh
  patchproblem.hh
  multilevelpatchpreconditioner.hh
  supportpatchfactory.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/tectonic)
