#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../explicitgrid.hh"
#include "../explicitvectors.hh"

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>

#include "../spatial-solving/solverfactory.hh"
#include "../spatial-solving/tnnmg/zerononlinearity.hh"
#include "../data-structures/network/contactnetwork.hh"
#include "../data-structures/friction/globalfriction.hh"

#include "tnnmg/functional.hh"

#include "../time-stepping/rate/rateupdater.hh"
#include "../time-stepping/state/stateupdater.hh"
#include "../time-stepping/updaters.hh"

using MyContactNetwork = ContactNetwork<Grid, Vector>;

using BoundaryNodes = typename MyContactNetwork::BoundaryNodes;
using BoundaryFunctions = typename MyContactNetwork::BoundaryFunctions;

using MyStateUpdater = StateUpdater<ScalarVector, Vector>;
using MyRateUpdater = RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>;
using MyUpdaters = Updaters<MyRateUpdater, MyStateUpdater>;

using LinearSolver = Dune::Solvers::LoopSolver<Vector>;
using ErrorNorms = typename MyContactNetwork::StateEnergyNorms;
using MyNBodyAssembler = typename MyContactNetwork::NBodyAssembler;

using MyGlobalFriction = GlobalFriction<Matrix, Vector>;
using MyFunctional = Functional<Matrix&, Vector&, MyGlobalFriction&, Vector&, Vector&, double>;
using MySolverFactory = SolverFactory<MyFunctional, BitVector>;

template class FixedPointIterator<MySolverFactory, MyNBodyAssembler, MyUpdaters, ErrorNorms>;
template FixedPointIterationCounter FixedPointIterator<MySolverFactory, MyNBodyAssembler, MyUpdaters, ErrorNorms>::run<LinearSolver>(
    MyUpdaters, std::shared_ptr<LinearSolver>&, const std::vector<Matrix>&, const std::vector<Vector>&, std::vector<Vector>&);

using NoFriction = ZeroNonlinearity;
using NoFrictionFunctional = Functional<Matrix&, Vector&, NoFriction&, Vector&, Vector&, double>;
using NoFrictionSolverFactory = SolverFactory<NoFrictionFunctional, BitVector>;

template class FixedPointIterator<NoFrictionSolverFactory, MyNBodyAssembler, MyUpdaters, ErrorNorms>;
template FixedPointIterationCounter FixedPointIterator<NoFrictionSolverFactory, MyNBodyAssembler, MyUpdaters, ErrorNorms>::run<LinearSolver>(
    MyUpdaters, std::shared_ptr<LinearSolver>&, const std::vector<Matrix>&, const std::vector<Vector>&, std::vector<Vector>&);
