#ifndef SRC_EXPLICITVECTORS_HH
#define SRC_EXPLICITVECTORS_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

using ctype = double;

using LocalVector = Dune::FieldVector<ctype, MY_DIM>;
using LocalMatrix = Dune::FieldMatrix<ctype, MY_DIM, MY_DIM>;
using Vector = Dune::BlockVector<LocalVector>;
using Matrix = Dune::BCRSMatrix<LocalMatrix>;
using ScalarVector = Dune::BlockVector<Dune::FieldVector<ctype, 1>>;
using ScalarMatrix = Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, 1>>;
using BitVector = Dune::BitSetVector<MY_DIM>;

#endif
