add_subdirectory("contacttest")
add_subdirectory("hdf5test")
#add_subdirectory("tnnmgtest")

dune_add_test(SOURCES globalfrictioncontainertest.cc)
dune_add_test(SOURCES gridgluefrictiontest.cc)
dune_add_test(SOURCES nodalweightstest.cc)
dune_add_test(SOURCES supportpatchfactorytest.cc)
dune_add_test(SOURCES solverfactorytest.cc)
