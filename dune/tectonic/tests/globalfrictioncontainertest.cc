#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <vector>

#include <exception>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/stdstreams.hh>

#include "../data-structures/globalfrictioncontainer.hh"

struct Body {
    size_t id;
    size_t localAlpha;

    void updateAlpha(size_t lAlpha) {
        localAlpha = lAlpha;
    }
};

int main(int argc, char *argv[]) {
    try {
        Dune::MPIHelper::instance(argc, argv);

        std::cout << "--------------------------" << std::endl;
        std::cout << "-- GlobalFriction Test: --" << std::endl;
        std::cout << "--------------------------" << std::endl << std::endl;

        // have to be <10
        size_t n = 5;
        size_t m = 3;
        size_t l = 2;

        std::vector<std::vector<int>> ref;
        ref.resize(n);
        for (size_t i=0; i<n; i++) {
            ref[i].resize(m);
            for (size_t j=0; j<m; j++) {
                ref[i][j] = 10*i + j;
            }
        }

        // resize() test
        bool resizeTest = true;
        {
            GlobalFrictionContainer<Body,1> globalFriction1;
            globalFriction1.resize({n});
            resizeTest = resizeTest & (globalFriction1.size() == n);

            GlobalFrictionContainer<Body,2> globalFriction2;
            globalFriction2.resize({n});
            resizeTest = resizeTest & (globalFriction2.size() == n);

            for (size_t i=0; i<globalFriction2.size(); i++) {
                resizeTest = resizeTest & (globalFriction2[i].size() == 0);
            }
        }

        // operator[] test
        bool operatorTest = true;
        {
            GlobalFrictionContainer<Body,2> globalFriction;
            globalFriction.resize({n, m});
            operatorTest = operatorTest & (globalFriction.size() == n);

            for (size_t i=0; i<n; i++) {
                for (size_t j=0; j<m; j++) {
                    operatorTest = operatorTest & (globalFriction[i][j] == nullptr);
                }
            }
        }

        // updateAlpha() test
        bool updateTest = true;
        {
            GlobalFrictionContainer<Body,3> globalFriction;
            globalFriction.resize({n,m,l});

            for (size_t i=0; i<n; i++) {
                for (size_t j=0; j<m; j++) {
                    for (size_t k=0; k<l; k++) {
                        globalFriction[i][j][k] = std::make_shared<Body>();
                        globalFriction[i][j][k]->id = 10*i + j;
                    }
                }
            }

            globalFriction.updateAlpha(ref);

            for (size_t i=0; i<n; i++) {
                for (size_t j=0; j<m; j++) {
                    for (size_t k=0; k<l; k++) {
                        updateTest = updateTest & (globalFriction[i][j][k]->id == globalFriction[i][j][k]->localAlpha);
                    }
                }
            }

        }

        std::cout << "resize(): " << resizeTest << std::endl;
        std::cout << "operator[]: " << operatorTest << std::endl;
        std::cout << "updateAlpha(): " << updateTest << std::endl;
        std::cout << "-------------------------" << std::endl << std::endl;

        bool passed = resizeTest & operatorTest & updateTest;

        std::cout << "Overall the test " << (passed ? "was successful!" : "failed!") << std::endl;

        return passed ? 0 : 1;

    } catch (Dune::Exception &e) {
        Dune::derr << "Dune reported error: " << e << std::endl;
    } catch (std::exception &e) {
        std::cerr << "Standard exception: " << e.what() << std::endl;
    }
}
