// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TECTONIC_SRC_TESTS_COMMON_HH
#define DUNE_TECTONIC_SRC_TESTS_COMMON_HH

#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/fufem/referenceelementhelper.hh>

// utility functions
bool isClose(double a, double b) {
  return std::abs(a - b) < 1e-14;
}

template <class Vector>
bool xyCollinear(Vector const &a, Vector const &b, Vector const &c) {
  return isClose((b[0] - a[0]) * (c[1] - a[1]), (b[1] - a[1]) * (c[0] - a[0]));
}

template <class Vector>
bool xyBoxed(Vector const &v1, Vector const &v2, Vector const &x) {
  auto const minmax0 = std::minmax(v1[0], v2[0]);
  auto const minmax1 = std::minmax(v1[1], v2[1]);

  if (minmax0.first - 1e-14 > x[0] or
      x[0] > minmax0.second + 1e-14)
    return false;
  if (minmax1.first - 1e-14 > x[1] or
      x[1] > minmax1.second + 1e-14)
    return false;

  return true;
}

template <class Vector>
bool xyBetween(Vector const &v1, Vector const &v2, Vector const &x) {
  return xyCollinear(v1, v2, x) && xyBoxed(v1, v2, x);
}

// build cube grid given by lowerLeft and upperRight point in global coordinates
template <class GlobalCoords, class GridType>
void buildGrid(const GlobalCoords& lowerLeft, const GlobalCoords& upperRight, const int n, std::shared_ptr<GridType>& grid, bool simplexGrid = true) {
    std::array<unsigned int, GridType::dimension> elements;
    std::fill(elements.begin(), elements.end(), n);

    Dune::GridFactory<GridType> factory;
    if (!simplexGrid) {
        Dune::StructuredGridFactory<GridType>::createCubeGrid(factory, lowerLeft, upperRight, elements);
        grid = std::move(factory.createGrid());
    } else {
        Dune::StructuredGridFactory<GridType>::createSimplexGrid(factory, lowerLeft, upperRight, elements);
        grid = std::move(factory.createGrid());
    }
}

template <class Intersection>
bool containsInsideSubentity(const Intersection& nIt, int subEntity, int codim) {
    return ReferenceElementHelper<double, Intersection::GridView::dim>::subEntityContainsSubEntity(nIt.inside().type(), nIt.indexInInside(), 1, subEntity, codim);
}

#endif
