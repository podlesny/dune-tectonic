#ifndef SRC_NODALWEIGHTS_HH
#define SRC_NODALWEIGHTS_HH


/**
 * let merged contact boundary Gamma be given by GridGlue object;
 * computes
 *                  int_Gamma lambda_i dx,
 * where lambda_i is nodal basis of merged contact boundary whose
 * dofs are given by nonmortar and mortar dofs;
 *
 *
 * NOTE: works only for P1 nodal bases
 **/

template <class Basis0, class Basis1>
class NodalWeights {
private:
    enum {dim = Basis0::GridView::Grid::dimension};

    template <class Basis, class Element, class GlobalCoords>
    auto basisDof(const Basis& basis, const Element& elem, const GlobalCoords& vertex) const;

public:
    NodalWeights(const Basis0& basis0, const Basis1& basis1);

    template <class GridGlue, class ScalarVector>
    void assemble(const GridGlue& glue, ScalarVector& weights0, ScalarVector& weights1, bool initializeVector = true) const;

private:
    const Basis0& basis0_;
    const Basis1& basis1_;
};

#include "nodalweights.cc"

#endif
