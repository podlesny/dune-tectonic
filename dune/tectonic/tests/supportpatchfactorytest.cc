#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define MY_DIM 2

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/fufem/formatstring.hh>

#include "../assemblers.hh"
#include "../gridselector.hh"
#include "../explicitgrid.hh"
#include "../explicitvectors.hh"

#include "../factories/threeblocksfactory.cc"

#include "../spatial-solving/preconditioners/supportpatchfactory.hh"
#include "../utils/debugutils.hh"



//#include <dune/tectonic/transformedglobalratestatefriction.hh>

#include "common.hh"

const int dim = MY_DIM;
const int n = 5;
const bool simplexGrid = true;

const std::string path = "";
const std::string outputFile = "supportpatchfactorytest.log";

Dune::ParameterTree getParameters(int argc, char *argv[]) {
  Dune::ParameterTree parset;
  Dune::ParameterTreeParser::readINITree("/home/mi/podlesny/software/dune/dune-tectonic/src/multi-body-problem.cfg", parset);
  Dune::ParameterTreeParser::readINITree(
      Dune::Fufem::formatString("/home/mi/podlesny/software/dune/dune-tectonic/src/multi-body-problem-%dD.cfg", dim), parset);
  Dune::ParameterTreeParser::readOptions(argc, argv, parset);
  return parset;
}

template <class SupportPatchFactory>
void testSuite(const SupportPatchFactory& supportPatchFactory, const size_t bodyID, const int patchDepth = 0) {
/*    const auto& coarseContactNetwork = supportPatchFactory.coarseContactNetwork();
    const auto& gridView = coarseContactNetwork.body(bodyID)->gridView();

    for (const auto& e : elements(gridView)) {

    }

    using Patch = typename SupportPatchFactory::Patch;
    Patch patch0, patch1, patch2, patch3;

    // (const size_t bodyID, const Element& coarseElement, const size_t localVertex, Patch& patchDofs, const int patchDepth = 0)

    // interior patch inside of one body
    supportPatchFactory.build(0, const Element& coarseElement, const size_t localVertex, patch0, patchDepth);

    // patch at friction interface with two bodies in contact
    supportPatchFactory.build(0, const Element& coarseElement, const size_t localVertex, patch1 patchDepth);

    // patch at friction interface with two bodies in contact and with dirichlet boundary
    supportPatchFactory.build(0, const Element& coarseElement, const size_t localVertex, patch2, patchDepth);

    // crosspoint patch, where all 3 bodies are in contact
    supportPatchFactory.build(0, const Element& coarseElement, const size_t localVertex, patch3, patchDepth);*/
}

int main(int argc, char *argv[]) { try {
    Dune::MPIHelper::instance(argc, argv);

    std::ofstream out(path + outputFile);
    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buffer
    std::cout.rdbuf(out.rdbuf()); //redirect std::cout to outputFile

    std::cout << "-------------------------------" << std::endl;
    std::cout << "-- SupportPatchFactory Test: --" << std::endl;
    std::cout << "-------------------------------" << std::endl << std::endl;

    const auto parset = getParameters(argc, argv);

    // set up contact network
    ThreeBlocksFactory<Grid, Vector> threeBlocksFactory(parset);
    //using ContactNetwork = typename ThreeBlocksFactory<Grid, Vector>::ContactNetwork;
    //threeBlocksFactory.build();

    //ContactNetwork& contactNetwork = threeBlocksFactory.contactNetwork();
   // threeBlocksFactory.build();

   // auto& contactNetwork = threeBlocksFactory.contactNetwork();
    //const size_t bodyCount = contactNetwork.nBodies();

    //for (size_t i=0; i<bodyCount; i++) {
        //printDofLocation(levelContactNetwork.leafView(i));

        /*auto& levelViews = levelContactNetwork.levelViews(i);

        for (size_t j=0; j<levelViews.size(); j++) {
            writeToVTK(*levelViews[j], "", "body_" + std::to_string(i) + "_level_" + std::to_string(j));
        }

        writeToVTK(levelContactNetwork.leafView(i), "", "body_" + std::to_string(i) + "_leaf"); */
    //}

    // assemble levelContactNetwork
    //contactNetwork.assemble();

  /*  const auto& coarseContactNetwork = contactNetwork.level(0);
    const auto& fineContactNetwork = contactNetwork.level(1);

    SupportPatchFactory<decltype(coarseContactNetwork)> supportPatchFactory(coarseContactNetwork, fineContactNetwork);


    // set levelContactNetwork
    LevelContactNetwork levelContactNetwork;
    NBodyAssembler nBodyAssembler(grids.size(), 1);

    std::vector<const GridType*> grids_ptr(grids.size());
    for (size_t i=0; i<grids_ptr.size(); i++) {
        grids_ptr[i] = grids[i].get();
    }

    nBodyAssembler.setGrids(grids_ptr);
    nBodyAssembler.setCoupling(coupling, 0);

    nBodyAssembler.assembleTransferOperator();
    nBodyAssembler.assembleObstacle();*/

    bool passed = true;

    std::cout << "Overall, the test " << (passed ? "was successful!" : "failed!") << std::endl;

    std::cout.rdbuf(coutbuf); //reset to standard output again
    return passed ? 0 : 1;

} catch (Dune::Exception &e) {
    Dune::derr << "Dune reported error: " << e << std::endl;
} catch (std::exception &e) {
    std::cerr << "Standard exception: " << e.what() << std::endl;
} // end try
} // end main
