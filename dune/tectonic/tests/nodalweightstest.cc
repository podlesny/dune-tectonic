#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <exception>
#include <fstream>

#include <dune/grid/uggrid.hh>

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/localassemblers/neumannboundaryassembler.hh>
#include <dune/fufem/functions/constantfunction.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>

#include <dune/contact/projections/normalprojection.hh>
#include <dune/contact/common/couplingpair.hh>
#include <dune/contact/assemblers/dualmortarcoupling.hh>

#include "../utils/debugutils.hh"
#include "common.hh"

#include "../nodalweights.hh"

const int dim = 2;
const int n = 5;
const bool simplexGrid = true;

const std::string path = "";
const std::string outputFile = "nodalweightstest.log";

#if HAVE_UG
    using GridType = typename Dune::UGGrid<dim>;
#else
#error No UG!
#endif
    using LevelView = typename GridType::LevelGridView;
    using LeafView = typename GridType::LeafGridView;
    using LevelBoundaryPatch = BoundaryPatch<LevelView>;
    using LeafBoundaryPatch = BoundaryPatch<LeafView>;

    using c_type = double;
    using GlobalCoords = Dune::FieldVector<c_type, dim>;
    using BlockVector = Dune::BlockVector<GlobalCoords>;

    using ScalarVector = Dune::FieldVector<c_type, 1>;
    using ScalarBlockVector = Dune::BlockVector<ScalarVector>;

    using CouplingPair = Dune::Contact::CouplingPair<GridType, GridType, c_type>;
    using CouplingType = Dune::Contact::DualMortarCoupling<c_type, GridType>;

int main(int argc, char *argv[]) { try {
    Dune::MPIHelper::instance(argc, argv);

    std::ofstream out(path + outputFile);
    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buffer
    std::cout.rdbuf(out.rdbuf()); //redirect std::cout to outputFile

    std::cout << "------------------------" << std::endl;
    std::cout << "-- NodalWeights Test: --" << std::endl;
    std::cout << "------------------------" << std::endl << std::endl;

    // building grids
    std::vector<std::shared_ptr<GridType>> grids(2);

    GlobalCoords lowerLeft0({0, 0});
    GlobalCoords upperRight0({2, 1});
    buildGrid(lowerLeft0, upperRight0, n, grids[0]);

    GlobalCoords lowerLeft1({0, 1});
    GlobalCoords upperRight1({2, 2});
    buildGrid(lowerLeft1, upperRight1, n, grids[1]);

    // writing grids
    for (size_t i=0; i<grids.size(); i++) {
        const auto& levelView = grids[i]->levelGridView(0);
        writeToVTK(levelView, path, "body_" + std::to_string(i) + "_level0");
    }

    // compute coupling boundaries
    LevelView gridView0 = grids[0]->levelGridView(0);
    LevelView gridView1 = grids[1]->levelGridView(0);
    LevelBoundaryPatch upper(gridView0);
    LevelBoundaryPatch lower(gridView1);

    lower.insertFacesByProperty([&](typename LevelView::Intersection const &in) {
        return xyBetween(lowerLeft1, {upperRight1[0], lowerLeft1[1]}, in.geometry().center());
    });

    upper.insertFacesByProperty([&](typename LevelView::Intersection const &in) {
        return xyBetween({lowerLeft0[0], upperRight0[1]}, upperRight0, in.geometry().center());
    });

    // set contact coupling
    Dune::Contact::NormalProjection<LeafBoundaryPatch> contactProjection;

    CouplingPair coupling;
    coupling.set(0, 1, upper, lower, 0.1, CouplingPair::CouplingType::STICK_SLIP, contactProjection, nullptr);

    double coveredArea_ = 0.8;
    CouplingType contactCoupling;
    contactCoupling.setGrids(*grids[0], *grids[1]);
    contactCoupling.setupContactPatch(*coupling.patch0(),*coupling.patch1());
    contactCoupling.gridGlueBackend_ = coupling.backend();
    contactCoupling.setCoveredArea(coveredArea_);
    contactCoupling.setup();

    using Basis = P1NodalBasis<LevelView, c_type>;
    Basis basis0(grids[0]->levelGridView(0));
    Basis basis1(grids[1]->levelGridView(0));

    printBasisDofLocation(basis0);
    printBasisDofLocation(basis1);

    ScalarBlockVector nWeights0, nWeights1;
    NodalWeights<Basis, Basis> nodalWeights(basis0, basis1);
    nodalWeights.assemble(*contactCoupling.getGlue(), nWeights0, nWeights1, true);

    ScalarBlockVector weights0;
    {
        Assembler<Basis, Basis> assembler(basis0, basis0);
        NeumannBoundaryAssembler<GridType, typename ScalarBlockVector::block_type> boundaryAssembler(
                     std::make_shared<ConstantFunction<
                     GlobalCoords, typename ScalarBlockVector::block_type>>(1));
             assembler.assembleBoundaryFunctional(boundaryAssembler, weights0, upper);
    }

    ScalarBlockVector weights1;
    {
        Assembler<Basis, Basis> assembler(basis1, basis1);
        NeumannBoundaryAssembler<GridType, typename ScalarBlockVector::block_type> boundaryAssembler(
                     std::make_shared<ConstantFunction<
                     GlobalCoords, typename ScalarBlockVector::block_type>>(1));
             assembler.assembleBoundaryFunctional(boundaryAssembler, weights1, lower);
    }

    print(weights0, "assembled weights0: ");
    print(nWeights0, "nWeights0: ");

    print(weights1, "assembled weights1: ");
    print(nWeights1, "nWeights1: ");

    bool sizePassed = true;
    if (weights0.size() != nWeights0.size() | weights1.size() != nWeights1.size()) {
        sizePassed = false;
    }

    bool entriesPassed0 = true;
    for (size_t i=0; i<weights0.size(); i++) {
        entriesPassed0 = entriesPassed0 & isClose(weights0[i], nWeights0[i]);
    }

    bool entriesPassed1 = true;
    for (size_t i=0; i<weights1.size(); i++) {
        entriesPassed1 = entriesPassed1 & isClose(weights1[i], nWeights1[i]);
    }

    bool passed = sizePassed & entriesPassed0 & entriesPassed1;

    std::cout << "Overall, the test " << (passed ? "was successful!" : "failed!") << std::endl;

    std::cout.rdbuf(coutbuf); //reset to standard output again
    return passed ? 0 : 1;

} catch (Dune::Exception &e) {
    Dune::derr << "Dune reported error: " << e << std::endl;
} catch (std::exception &e) {
    std::cerr << "Standard exception: " << e.what() << std::endl;
} // end try
} // end main
