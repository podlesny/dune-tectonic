#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_IPOPT
#undef HAVE_IPOPT
#endif

#define MY_DIM 2

#include <atomic>
#include <cmath>
#include <csignal>
#include <exception>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <memory>

#include <dune/common/bitsetvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/function.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>


#include <dune/fufem/formatstring.hh>

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/blockgssteps.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>

#include <dune/tectonic/assemblers.hh>
#include <dune/tectonic/gridselector.hh>
#include <dune/tectonic/explicitgrid.hh>
#include <dune/tectonic/explicitvectors.hh>

#include <dune/tectonic/data-structures/enumparser.hh>
#include <dune/tectonic/data-structures/enums.hh>
#include <dune/tectonic/data-structures/network/contactnetwork.hh>
#include <dune/tectonic/data-structures/matrices.hh>

#include <dune/tectonic/factories/twoblocksfactory.hh>
#include <dune/tectonic/factories/threeblocksfactory.hh>
#include <dune/tectonic/factories/stackedblocksfactory.hh>

#include <dune/tectonic/io/vtk.hh>

#include <dune/tectonic/spatial-solving/tnnmg/functional.hh>
#include <dune/tectonic/spatial-solving/tnnmg/zerononlinearity.hh>
#include <dune/tectonic/spatial-solving/tnnmg/localbisectionsolver.hh>
#include <dune/tectonic/spatial-solving/solverfactory.hh>
#include <dune/tectonic/spatial-solving/contact/nbodycontacttransfer.hh>

#include <dune/tectonic/utils/debugutils.hh>

// for getcwd
#include <unistd.h>

size_t const dims = MY_DIM;

Dune::ParameterTree getParameters(int argc, char *argv[]) {
  Dune::ParameterTree parset;
  Dune::ParameterTreeParser::readINITree("/home/mi/podlesny/software/dune/dune-tectonic/dune/tectonic/tests/contacttest/staticcontacttest.cfg", parset);
  Dune::ParameterTreeParser::readINITree(
      Dune::Fufem::formatString("/home/mi/podlesny/software/dune/dune-tectonic/dune/tectonic/tests/contacttest/staticcontacttest-%dD.cfg", dims), parset);
  Dune::ParameterTreeParser::readOptions(argc, argv, parset);
  return parset;
}

int main(int argc, char *argv[]) {
  try {
    Dune::MPIHelper::instance(argc, argv);

    char buffer[256];
    char *val = getcwd(buffer, sizeof(buffer));
    if (val) {
        std::cout << buffer << std::endl;
        std::cout << argv[0] << std::endl;
    }

    std::ofstream out("staticcontacttest.log");
    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buffer
    std::cout.rdbuf(out.rdbuf()); //redirect std::cout to log.txt

    auto const parset = getParameters(argc, argv);

    // ----------------------
    // set up contact network
    // ----------------------
    using BlocksFactory = StackedBlocksFactory<Grid, Vector>;
    BlocksFactory blocksFactory(parset);

    using ContactNetwork = typename BlocksFactory::ContactNetwork;
    blocksFactory.build();

    auto& contactNetwork = blocksFactory.contactNetwork();

    const size_t bodyCount = contactNetwork.nBodies();

   /* for (size_t i=0; i<contactNetwork.nLevels(); i++) {
        // printDofLocation(contactNetwork.body(i)->gridView());

        //Vector def(contactNetwork.deformedGrids()[i]->size(dims));
        //def = 1;
        //deformedGridComplex.setDeformation(def, i);

        const auto& level = *contactNetwork.level(i);

        for (size_t j=0; j<level.nBodies(); j++) {
            writeToVTK(level.body(j)->gridView(), "../debug_print/bodies/", "body_" + std::to_string(j) + "_level_" + std::to_string(i));
        }
    }*/

    for (size_t i=0; i<bodyCount; i++) {
        writeToVTK(contactNetwork.body(i)->gridView(), "", "initial_body_" + std::to_string(i) + "_leaf");
    }

    // ----------------------------
    // assemble contactNetwork
    // ----------------------------
    contactNetwork.assemble();

    //printMortarBasis<Vector>(contactNetwork.nBodyAssembler());


    // ----------------------------
    // compute minimal stress solution
    // ----------------------------
    using Matrix = typename ContactNetwork::Matrix;
    const auto& nBodyAssembler = contactNetwork.nBodyAssembler();

    // Initial displacement: Start from a situation of minimal stress,
    // which is automatically attained in the case [v = 0 = a].
    // Assuming dPhi(v = 0) = 0, we thus only have to solve Au = ell0
    BitVector dirichletNodes;
    contactNetwork.totalNodes("dirichlet", dirichletNodes);

    // minimal stress displacement
    std::vector<Vector> u(bodyCount);
    for (size_t i=0; i<bodyCount; i++) {
        u[i].resize(contactNetwork.body(i)->nVertices());
        u[i] = 0.0;
    }

    // set rhs
    std::vector<Vector> ell0(bodyCount);
    for (size_t i=0; i<bodyCount; i++) {
        ell0[i].resize(u[i].size());
        ell0[i] = 0.0;

        contactNetwork.body(i)->externalForce()(0.0, ell0[i]);
    }

    // set elasticity operator
    const auto& matrices = contactNetwork.matrices().elasticity;
    std::vector<const Matrix*> matrices_ptr(matrices.size());
    for (size_t i=0; i<matrices_ptr.size(); i++) {
        matrices_ptr[i] = matrices[i].get();
    }

    // assemble full global contact problem
    Matrix bilinearForm;

    nBodyAssembler.assembleJacobian(matrices_ptr, bilinearForm);

    Vector totalRhs;
    nBodyAssembler.assembleRightHandSide(ell0, totalRhs);

    Vector totalX;
    nBodyAssembler.nodalToTransformed(u, totalX);

    // get lower and upper obstacles
    const auto& totalObstacles = nBodyAssembler.totalObstacles_;
    Vector lower(totalObstacles.size());
    Vector upper(totalObstacles.size());

    for (size_t j=0; j<totalObstacles.size(); ++j) {
        const auto& totalObstaclesj = totalObstacles[j];
        auto& lowerj = lower[j];
        auto& upperj = upper[j];
        for (size_t d=0; d<dims; ++d) {
            lowerj[d] = totalObstaclesj[d][0];
            upperj[d] = totalObstaclesj[d][1];
        }
    }

    // print problem
    print(bilinearForm, "bilinearForm");
    print(totalRhs, "totalRhs");
    print(dirichletNodes, "ignore");

    std::vector<const Dune::BitSetVector<1>*> frictionNodes;
    contactNetwork.frictionNodes(frictionNodes);

    for (size_t i=0; i<frictionNodes.size(); i++) {
        print(*frictionNodes[i], "frictionNodes_body_" + std::to_string(i));
    }

    print(totalObstacles, "totalObstacles");
    print(lower, "lower");
    print(upper, "upper");

    // set up functional
    using Functional = Functional<Matrix&, Vector&, ZeroNonlinearity&, Vector&, Vector&, typename Matrix::field_type>;
    Functional J(bilinearForm, totalRhs, ZeroNonlinearity(), lower, upper); //TODO

       /* std::vector<BitVector> bodyDirichletNodes;
        nBodyAssembler.postprocess(dirichletNodes, bodyDirichletNodes);
        for (size_t i=0; i<bodyDirichletNodes.size(); i++) {
          print(bodyDirichletNodes[i], "bodyDirichletNodes_" + std::to_string(i) + ": ");
        }*/

       /* print(bilinearForm, "matrix: ");
        print(totalX, "totalX: ");
        print(totalRhs, "totalRhs: ");*/

    // make linear solver for linear correction in TNNMGStep
    const auto& solverParset = parset.sub("u0.solver");

    using Norm =  EnergyNorm<Matrix, Vector>;
    using LinearSolver = typename Dune::Solvers::LoopSolver<Vector>;

    // set multigrid solver
    auto smoother = TruncatedBlockGSStep<Matrix, Vector>();

    using TransferOperator = NBodyContactTransfer<ContactNetwork, Vector>;
    using TransferOperators = std::vector<std::shared_ptr<TransferOperator>>;

    TransferOperators transfer(contactNetwork.nLevels()-1);
    for (size_t i=0; i<transfer.size(); i++) {
        transfer[i] = std::make_shared<TransferOperator>();
        transfer[i]->setup(contactNetwork, i, i+1);
    }

    // Remove any recompute filed so that initially the full transferoperator is assembled
    for (size_t i=0; i<transfer.size(); i++)
        std::dynamic_pointer_cast<TruncatedMGTransfer<Vector> >(transfer[i])->setRecomputeBitField(nullptr);

    auto linearMultigridStep = std::make_shared<Dune::Solvers::MultigridStep<Matrix, Vector> >();
    linearMultigridStep->setMGType(1, 3, 3);
    linearMultigridStep->setSmoother(smoother);
    linearMultigridStep->setTransferOperators(transfer);

    Norm norm(*linearMultigridStep);

    auto linearSolver = std::make_shared<LinearSolver>(linearMultigridStep, parset.get<int>("solver.tnnmg.main.multi"), parset.get<double>("solver.tnnmg.preconditioner.basesolver.tolerance"), norm, Solver::QUIET);

    // set up TNMMG solver
    using Factory = SolverFactory<Functional, BitVector>;
    Factory factory(parset.sub("solver.tnnmg"), J, dirichletNodes);

    factory.build(linearSolver);
    auto tnnmgStep = factory.step();
    factory.setProblem(totalX);

    //const EnergyNorm<Matrix, Vector> norm(bilinearForm);

    std::cout << "solving linear problem for u..." << std::endl;
    LoopSolver<Vector> solver(
            *tnnmgStep.get(), solverParset.get<size_t>("maximumIterations"),
            solverParset.get<double>("tolerance"), norm,
            solverParset.get<Solver::VerbosityMode>("verbosity")); // absolute error

    solver.preprocess();
    solver.solve();

    nBodyAssembler.postprocess(tnnmgStep->getSol(), u);

    print(tnnmgStep->getSol(), "totalX:");
    print(u, "u:");

    for (size_t i=0; i<bodyCount; i++) {
      auto& body = contactNetwork.body(i);
      body->setDeformation(u[i]);
      writeToVTK(body->gridView(), "", "body_" + std::to_string(i) + "_leaf");
    }

    /*
    using BoundaryFunctions = typename ContactNetwork::BoundaryFunctions;
    using BoundaryNodes = typename ContactNetwork::BoundaryNodes;
    using Updaters = Updaters<RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>,
                               StateUpdater<ScalarVector, Vector>>;

    BoundaryFunctions velocityDirichletFunctions;
    contactNetwork.boundaryFunctions("dirichlet", velocityDirichletFunctions);

    BoundaryNodes dirichletNodes;
    contactNetwork.boundaryNodes("dirichlet", dirichletNodes);

    for (size_t i=0; i<dirichletNodes.size(); i++) {
        for (size_t j=0; j<dirichletNodes[i].size(); j++) {
        print(*dirichletNodes[i][j], "dirichletNodes_body_" + std::to_string(i) + "_boundary_" + std::to_string(j));
        }
    }*/

    std::cout.rdbuf(coutbuf); //reset to standard output again

  } catch (Dune::Exception &e) {
    Dune::derr << "Dune reported error: " << e << std::endl;
  } catch (std::exception &e) {
    std::cerr << "Standard exception: " << e.what() << std::endl;
  }
}
