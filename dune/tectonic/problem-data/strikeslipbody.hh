#ifndef SRC_MULTI_BODY_PROBLEM_DATA_MYBODYDATA_HH
#define SRC_MULTI_BODY_PROBLEM_DATA_MYBODYDATA_HH

#include <dune/common/function.hh>
#include <dune/common/fvector.hh>

#include <dune/fufem/functions/constantfunction.hh>

#include "../data-structures/body/bodydata.hh"
#include "gravity.hh"
#include "grid/cuboidgeometry.hh"


class SegmentedFunction
    : public Dune::VirtualFunction<Dune::FieldVector<double, MY_DIM>,
                                   Dune::FieldVector<double, 1>> {
private:
  bool distFromDiagonal(const Dune::FieldVector<double, MY_DIM>& z) const {
    return std::abs(z[0] - z[1])/std::sqrt(2);
  }

  bool insideViscousRegion(Dune::FieldVector<double, MY_DIM> const &z) const {
    return distFromDiagonal(z) > distFromDiag_;
  }

  const double distFromDiag_;

  const double v1_;
  const double v2_;

public:
  SegmentedFunction(double v1, double v2, double distFromDiag = 0.1) :
      distFromDiag_(distFromDiag) , v1_(v1), v2_(v2) {}

  void evaluate(Dune::FieldVector<double, MY_DIM> const &x,
                Dune::FieldVector<double, 1> &y) const {
    y = insideViscousRegion(x) ? v2_ : v1_;
  }
};


template <int dimension> class MyBodyData : public BodyData<dimension> {
  using typename BodyData<dimension>::ScalarFunction;
  using typename BodyData<dimension>::VectorField;

public:
  MyBodyData(Dune::ParameterTree const &parset, const double gravity, const Dune::FieldVector<double, dimension>& zenith)
      : poissonRatio_(parset.get<double>("poissonRatio")),
        youngModulus_(3.0 * parset.get<double>("bulkModulus") *
                      (1.0 - 2.0 * poissonRatio_)),
        zenith_(zenith),
        shearViscosityField_(
            parset.get<double>("elastic.shearViscosity"),
            parset.get<double>("viscoelastic.shearViscosity"),
            parset.get<double>("elastic.distFromDiag")),
        bulkViscosityField_(
            parset.get<double>("elastic.bulkViscosity"),
            parset.get<double>("viscoelastic.bulkViscosity"),
            parset.get<double>("elastic.distFromDiag")),
        densityField_(parset.get<double>("elastic.density"),
                      parset.get<double>("viscoelastic.density"),
                      parset.get<double>("elastic.distFromDiag")),
        gravityField_(densityField_, zenith_,
                      gravity) {}

  double getPoissonRatio() const override { return poissonRatio_; }
  double getYoungModulus() const override { return youngModulus_; }
  ScalarFunction const &getShearViscosityField() const override {
    return shearViscosityField_;
  }
  ScalarFunction const &getBulkViscosityField() const override {
    return bulkViscosityField_;
  }
  ScalarFunction const &getDensityField() const override {
    return densityField_;
  }
  VectorField const &getGravityField() const override { return gravityField_; }

private:
  double const poissonRatio_;
  double const youngModulus_;
  Dune::FieldVector<double, dimension> zenith_;

  SegmentedFunction const shearViscosityField_;
  SegmentedFunction const bulkViscosityField_;
  SegmentedFunction const densityField_;
  Gravity<dimension> const gravityField_;
};
#endif
