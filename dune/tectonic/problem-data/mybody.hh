#ifndef SRC_MULTI_BODY_PROBLEM_DATA_MYBODYDATA_HH
#define SRC_MULTI_BODY_PROBLEM_DATA_MYBODYDATA_HH

#include <dune/common/fvector.hh>

#include <dune/fufem/functions/constantfunction.hh>

#include "../data-structures/body/bodydata.hh"
#include "gravity.hh"
#include "grid/cuboidgeometry.hh"
#include "segmented-function.hh"

template <int dimension> class MyBodyData : public BodyData<dimension> {
  using typename BodyData<dimension>::ScalarFunction;
  using typename BodyData<dimension>::VectorField;

public:
  MyBodyData(Dune::ParameterTree const &parset, const double gravity, const Dune::FieldVector<double, dimension>& zenith)
      : poissonRatio_(parset.get<double>("poissonRatio")),
        youngModulus_(3.0 * parset.get<double>("bulkModulus") *
                      (1.0 - 2.0 * poissonRatio_)),
        zenith_(zenith),
        shearViscosityField_(
            parset.get<double>("elastic.shearViscosity"),
            parset.get<double>("viscoelastic.shearViscosity")),
        bulkViscosityField_(
            parset.get<double>("elastic.bulkViscosity"),
            parset.get<double>("viscoelastic.bulkViscosity")),
        densityField_(parset.get<double>("elastic.density"),
                      parset.get<double>("viscoelastic.density")),
        gravityField_(densityField_, zenith_,
                      gravity) {}

  double getPoissonRatio() const override { return poissonRatio_; }
  double getYoungModulus() const override { return youngModulus_; }
  ScalarFunction const &getShearViscosityField() const override {
    return shearViscosityField_;
  }
  ScalarFunction const &getBulkViscosityField() const override {
    return bulkViscosityField_;
  }
  ScalarFunction const &getDensityField() const override {
    return densityField_;
  }
  VectorField const &getGravityField() const override { return gravityField_; }

private:
  double const poissonRatio_;
  double const youngModulus_;
  Dune::FieldVector<double, dimension> zenith_;

  SegmentedFunction const shearViscosityField_;
  SegmentedFunction const bulkViscosityField_;
  SegmentedFunction const densityField_;
  Gravity<dimension> const gravityField_;
};
#endif
