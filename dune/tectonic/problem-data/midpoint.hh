#ifndef SRC_MIDPOINT_HH
#define SRC_MIDPOINT_HH

#include <dune/matrix-vector/axpy.hh>

template <class Vector> Vector midPoint(Vector const &x, Vector const &y) {
  Vector ret(0);
  Dune::MatrixVector::addProduct(ret, 0.5, x);
  Dune::MatrixVector::addProduct(ret, 0.5, y);
  return ret;
}
#endif
