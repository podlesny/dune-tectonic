#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fstream>

#ifdef HAVE_CAIROMM
#include <cairomm/context.h>
#include <cairomm/fontface.h>
#include <cairomm/surface.h>
#endif

#include "cuboidgeometry.hh"

#if MY_DIM == 3
template <class ctype>
CuboidGeometry<ctype>::CuboidGeometry(const GlobalCoords& origin,
                                      const double length = 1.00, const double height = 0.27, const double depth = 0.60) :
    length_(length*lengthScale()),
    height_(height*lengthScale()),
    depth_(depth*lengthScale()),
    lowerLeft_(origin),
    lowerRight_({origin[0]+length_, origin[1], 0}),
    upperRight_({origin[0]+length_, origin[1]+height_, 0}),
    upperLeft_({origin[0], origin[1]+height_, 0})
    {}
#else

template <class ctype>
CuboidGeometry<ctype>::CuboidGeometry(const GlobalCoords& origin,
                                      const double length, const double height) :
    length_(length*lengthScale()),
    height_(height*lengthScale()),
    lowerLeft_(origin),
    lowerRight_({origin[0]+length_, origin[1]}),
    upperRight_({origin[0]+length_, origin[1]+height_}),
    upperLeft_({origin[0], origin[1]+height_})
    {}
#endif

template <class ctype>
void CuboidGeometry<ctype>::addWeakeningRegion(const WeakeningRegion& weakeningRegion) {
    weakeningRegions_.emplace_back(weakeningRegion);
}

template <class ctype>
void CuboidGeometry<ctype>::addWeakeningPatch(const Dune::ParameterTree& parset, const GlobalCoords& vertex0, const GlobalCoords& vertex1) {
    WeakeningRegion weakPatch;

#if MY_DIM == 3 // TODO: Does not work yet
    if (vertex0 != vertex1) {
        weakPatch.vertices.resize(4);
        weakPatch.vertices[0] = weakPatch.vertices[2] = vertex0;
        weakPatch.vertices[1] = weakPatch.vertices[3] = vertex1;

        for (size_t k = 0; k < 2; ++k) {
            weakPatch.vertices[k][2] = -depth_ / 2.0;
            weakPatch.vertices[k + 2][2] = depth_ / 2.0;
        }
        switch (parset.get<Config::PatchType>("patchType")) {
            case Config::Rectangular:
                break;
            case Config::Trapezoidal:
                weakPatch.vertices[1][0] += 0.05 * lengthScale();
                weakPatch.vertices[3][0] -= 0.05 * lengthScale();
                break;
            default:
                assert(false);
        }
        addWeakeningRegion(weakPatch);
    }
#else
    if (vertex0 != vertex1) {
        weakPatch.vertices.resize(2);
        weakPatch.vertices[0] = vertex0;
        weakPatch.vertices[1] = vertex1;
        addWeakeningRegion(weakPatch);
    }
#endif
}

template <class ctype>
void CuboidGeometry<ctype>::write() const {
    std::fstream writer("geometry", std::fstream::out);
    writer << "lowerLeft = " << lowerLeft_ << std::endl;
    writer << "lowerRight = " << lowerRight_ << std::endl;
    writer << "upperRight = " << upperRight_ << std::endl;
    writer << "upperLeft = " << upperLeft_ << std::endl;
}

/*
template <class ctype>
void CuboidGeometry<ctype>::render() const {
#ifdef HAVE_CAIROMM
  std::string const filename = "geometry.png";
  double const width = 600;
  double const height = 400;
  double const widthScale = 400;
  double const heightScale = 400;

  auto surface =
      Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, width, height);
  auto cr = Cairo::Context::create(surface);

  auto const setRGBColor = [&](int colour) {
    cr->set_source_rgb(((colour & 0xFF0000) >> 16) / 255.0,
                       ((colour & 0x00FF00) >> 8) / 255.0,
                       ((colour & 0x0000FF) >> 0) / 255.0);
  };
  auto const moveTo = [&](LocalVector2D const &v) { cr->move_to(v[0], -v[1]); };
  auto const lineTo = [&](LocalVector2D const &v) { cr->line_to(v[0], -v[1]); };

  cr->scale(widthScale, heightScale);
  cr->translate(0.1, 0.1);
  cr->set_line_width(0.0025);

  // triangle
  {
    moveTo(reference::A);
    lineTo(reference::B);
    lineTo(reference::C);
    cr->close_path();
    cr->stroke();
  }

  // dashed lines
  {
    cr->save();
    std::vector<double> dashPattern = { 0.005 };
    cr->set_dash(dashPattern, 0);
    moveTo(reference::Z);
    lineTo(reference::Y);
    moveTo(reference::U);
    lineTo(reference::X);
    cr->stroke();
    cr->restore();
  }

  // fill viscoelastic region
  {
    cr->save();
    setRGBColor(0x0097E0);
    moveTo(reference::B);
    lineTo(reference::K);
    lineTo(reference::M);
    cr->fill();
    cr->restore();
  }

  // mark weakening region
  {
    cr->save();
    setRGBColor(0x7AD3FF);
    cr->set_line_width(0.005);
    moveTo(reference::X);
    lineTo(reference::Y);
    cr->stroke();
    cr->restore();
  }

  // mark points
  {
    auto const drawCircle = [&](LocalVector2D const &v) {
      cr->arc(v[0], -v[1], 0.0075, -M_PI, M_PI); // x,y,radius,angle1,angle2
      cr->fill();
    };

    cr->save();
    setRGBColor(0x002F47);
    drawCircle(reference::A);
    drawCircle(reference::B);
    drawCircle(reference::C);
    drawCircle(reference::Y);
    drawCircle(reference::X);
    drawCircle(reference::Z);
    drawCircle(reference::U);
    drawCircle(reference::K);
    drawCircle(reference::M);
    drawCircle(reference::G);
    drawCircle(reference::H);
    drawCircle(reference::J);
    drawCircle(reference::I);
    cr->restore();
  }

  // labels
  {
    auto const label = [&](LocalVector2D const &v, std::string l) {
      moveTo(v);
      cr->rel_move_to(0.005, -0.02);
      cr->show_text(l);
    };
    auto font = Cairo::ToyFontFace::create(
        "monospace", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);

    cr->save();
    cr->set_font_face(font);
    cr->set_font_size(0.03);

    label(A, "A");
    label(B, "B");
    label(C, "C");
    label(D, "D");
    label(X, "X");
    label(Y, "Y");

    cr->restore();
  }

  surface->write_to_png(filename);
#endif
}
*/

#include "cuboidgeometry_tmpl.cc"
