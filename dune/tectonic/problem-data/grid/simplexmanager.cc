#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "simplexmanager.hh"

#if MY_DIM == 3
SimplexManager::SimplexManager(unsigned int shift) : shift_(shift) {}
#endif

// back-to-front, front-to-back, front-to-back
void SimplexManager::addFromVerticesFBB(unsigned int U, unsigned int V,
                                        unsigned int W) {
#if MY_DIM == 3
  unsigned int const U2 = U + shift_;
  unsigned int const V2 = V + shift_;
  unsigned int const W2 = W + shift_;

  simplices_.push_back({ U, V, W, U2 });
  simplices_.push_back({ V, V2, W2, U2 });
  simplices_.push_back({ W, W2, U2, V });
#else
  simplices_.push_back({ U, V, W });
#endif
}

// back-to-front, back-to-front, front-to-back
void SimplexManager::addFromVerticesFFB(unsigned int U, unsigned int V,
                                        unsigned int W) {
#if MY_DIM == 3
  unsigned int const U2 = U + shift_;
  unsigned int const V2 = V + shift_;
  unsigned int const W2 = W + shift_;

  simplices_.push_back({ U, V, W, U2 });
  simplices_.push_back({ V, V2, W, U2 });
  simplices_.push_back({ V2, W, U2, W2 });
#else
  simplices_.push_back({ U, V, W });
#endif
}

auto SimplexManager::getSimplices() -> SimplexList const & {
  return simplices_;
}
