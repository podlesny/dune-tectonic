#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"
#include "../../explicitvectors.hh"
#include "trianglegeometry.hh"

template class GridsConstructor<Grid>;

template struct MyFaces<DeformedGrid::LeafGridView>;
template struct MyFaces<DeformedGrid::LevelGridView>;

template MyFaces<DeformedGrid::LeafGridView> GridsConstructor<Grid>::constructFaces(
    const DeformedGrid::LeafGridView&,
    const TriangleGeometry<typename Grid::ctype>&);

template MyFaces<DeformedGrid::LevelGridView> GridsConstructor<Grid>::constructFaces(
    const DeformedGrid::LevelGridView&,
    const TriangleGeometry<typename Grid::ctype>&);

template void refine<Grid, LocalVector>(
    Grid &grid, ConvexPolyhedron<LocalVector> const &weakPatch,
    double smallestDiameter, double lengthScale);
