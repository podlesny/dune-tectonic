#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"
#include "cuboidgeometry.hh"

template class CuboidGeometry<typename Grid::ctype>;
