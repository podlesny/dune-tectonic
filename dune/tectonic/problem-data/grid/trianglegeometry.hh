#ifndef SRC_MULTI_BODY_PROBLEM_DATA_TRIANGLEGEOMETRY_HH
#define SRC_MULTI_BODY_PROBLEM_DATA_TRIANGLEGEOMETRY_HH

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/fufem/geometry/convexpolyhedron.hh>

//            C
//          / |
//         /  |
//        /   |height
//       /    |
//      A --- B
//       length

template <class ctype = double>
class TriangleGeometry {
public:
    typedef Dune::FieldVector<ctype, MY_DIM> GlobalCoords;
    using WeakeningRegion = ConvexPolyhedron<GlobalCoords>;

    static constexpr double lengthScale() {
        return 1.0;
    } // scaling factor

private:
    const ctype length_;
    const ctype height_;
#if MY_DIM == 3
    const ctype depth_;
#endif

    // corners of triangle
    const GlobalCoords A_;
    const GlobalCoords B_;
    const GlobalCoords C_;

    // weakening regions
    std::vector<WeakeningRegion> weakeningRegions_;

public:
#if MY_DIM == 3
    TriangleGeometry(const GlobalCoords& origin,
                   const double length = 0.5, const double height = 0.5, const double depth = 0.12);

    const auto& depth() const {
        return depth_;
    }
#else
    TriangleGeometry(const GlobalCoords& origin,
                       const double length = 0.5, const double height = 0.5);
#endif

    void addWeakeningRegion(const WeakeningRegion& weakeningRegion);
    void addWeakeningPatch(const Dune::ParameterTree& parset, const GlobalCoords& vertex0, const GlobalCoords& vertex1);

    const auto& A() const {
        return A_;
    }

    const auto& B() const {
        return B_;
    }

    const auto& C() const {
        return C_;
    }

    const auto& weakeningRegions() const {
        return weakeningRegions_;
    }

    const auto& length() const {
        return length_;
    }

    const auto& height() const {
        return height_;
    }

    void write() const;

   // void render() const;
};
#endif
