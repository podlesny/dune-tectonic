#ifndef SRC_MULTI_BODY_PROBLEM_DATA_CUBOIDGEOMETRY_HH
#define SRC_MULTI_BODY_PROBLEM_DATA_CUBOIDGEOMETRY_HH

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/fufem/geometry/convexpolyhedron.hh>

template <class ctype = double>
class CuboidGeometry {
public:
    typedef Dune::FieldVector<ctype, MY_DIM> GlobalCoords;
    using WeakeningRegion = ConvexPolyhedron<GlobalCoords>;

    static constexpr double lengthScale() {
        return 1.0;
    } // scaling factor

private:
    const ctype length_;
    const ctype height_;
#if MY_DIM == 3
    const ctype depth_;
#endif

    // corners of cube
    const GlobalCoords lowerLeft_;
    const GlobalCoords lowerRight_;
    const GlobalCoords upperRight_;
    const GlobalCoords upperLeft_;

    // weakening regions
    std::vector<WeakeningRegion> weakeningRegions_;

public:
#if MY_DIM == 3
    CuboidGeometry(const GlobalCoords& origin,
                   const double length = 1.00, const double height = 0.27, const double depth = 0.60);

    const auto& depth() const {
        return depth_;
    }
#else
    CuboidGeometry(const GlobalCoords& origin,
                       const double length = 1.00, const double height = 0.27);
#endif

    void addWeakeningRegion(const WeakeningRegion& weakeningRegion);
    void addWeakeningPatch(const Dune::ParameterTree& parset, const GlobalCoords& vertex0, const GlobalCoords& vertex1);

    const auto& lowerLeft() const {
        return lowerLeft_;
    }

    const auto& lowerRight() const {
        return lowerRight_;
    }

    const auto& upperRight() const {
        return upperRight_;
    }

    const auto& upperLeft() const {
        return upperLeft_;
    }

    const auto& weakeningRegions() const {
        return weakeningRegions_;
    }

    void write() const;

   // void render() const;
};
#endif
