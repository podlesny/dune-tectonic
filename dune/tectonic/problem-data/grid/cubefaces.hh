#ifndef SRC_MULTI_BODY_PROBLEM_DATA_CUBEFACES_HH
#define SRC_MULTI_BODY_PROBLEM_DATA_CUBEFACES_HH

#include <dune/fufem/boundarypatch.hh>

#include "cube.hh"

template <class GridView>
class CubeFaces {
private:
    using Cube = Cube<GridView::dimensionworld>;

    bool isClose(double a, double b) {
      return std::abs(a - b) < 1e-14 * lengthScale_;
    }

    bool isClose2(double a, double b) {
      return std::abs(a - b) <
             1e-14 * lengthScale_ * lengthScale_;
    }

    template <class Vector>
    bool xyBoxed(Vector const &v1, Vector const &v2, Vector const &x);

    template <class Vector>
    bool xyCollinear(Vector const &a, Vector const &b, Vector const &c);

    template <class Vector>
    bool xyBetween(Vector const &v1, Vector const &v2, Vector const &x);

public:
    CubeFaces(const GridView& gridView, const Cube<GridView::dimensionworld>& cube, double lengthScale);

    const BoundaryPatch<GridView>& lower() const {
        return lower_;
    }

    const BoundaryPatch<GridView>& right() const {
        return right_;
    }

    const BoundaryPatch<GridView>& upper() const {
        return upper_;
    }

    const BoundaryPatch<GridView>& left() const {
        return left_;
    }

#if MY_DIM == 3
    const BoundaryPatch<GridView>& front() const {
        return front_;
    }
    const BoundaryPatch<GridView>& back() const {
        return back_;
    }
#endif

private:
  BoundaryPatch<GridView> lower_;
  BoundaryPatch<GridView> right_;
  BoundaryPatch<GridView> upper_;
  BoundaryPatch<GridView> left_;

#if MY_DIM == 3
  BoundaryPatch<GridView> front_;
  BoundaryPatch<GridView> back_;
#endif

  const Cube& cube_;
  const double lengthScale_;
};

#endif


