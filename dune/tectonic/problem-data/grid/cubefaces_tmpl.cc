#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"

template struct CubeFaces<DeformedGrid::LeafGridView>;
template struct CubeFaces<DeformedGrid::LevelGridView>;
