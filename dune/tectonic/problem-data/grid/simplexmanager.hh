#ifndef SRC_MULTI_BODY_PROBLEM_DATA_SIMPLEXMANAGER_HH
#define SRC_MULTI_BODY_PROBLEM_DATA_SIMPLEXMANAGER_HH

#include <vector>

class SimplexManager {
public:
  using SimplexList = std::vector<std::vector<unsigned int>>;

#if MY_DIM == 3
  SimplexManager(unsigned int shift);
#endif

  void addFromVerticesFBB(unsigned int U, unsigned int V, unsigned int W);
  void addFromVerticesFFB(unsigned int U, unsigned int V, unsigned int W);

  auto getSimplices() -> SimplexList const &;

private:
  SimplexList simplices_;

#if MY_DIM == 3
  unsigned int const shift_;
#endif
};

#endif


