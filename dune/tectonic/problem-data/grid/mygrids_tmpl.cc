#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"
#include "../../explicitvectors.hh"
#include "cuboidgeometry.hh"

template class GridsConstructor<Grid>;

template struct MyFaces<DeformedGrid::LeafGridView>;
template struct MyFaces<DeformedGrid::LevelGridView>;

template MyFaces<DeformedGrid::LeafGridView> GridsConstructor<Grid>::constructFaces(
    DeformedGrid::LeafGridView const &gridView, CuboidGeometry<typename Grid::ctype> const &CuboidGeometry_);

template MyFaces<DeformedGrid::LevelGridView> GridsConstructor<Grid>::constructFaces(
    DeformedGrid::LevelGridView const &gridView, CuboidGeometry<typename Grid::ctype> const &CuboidGeometry_);

template void refine<Grid, LocalVector>(
    Grid &grid, ConvexPolyhedron<LocalVector> const &weakPatch,
    double smallestDiameter, double lengthScale);
