#ifndef SRC_MULTI_BODY_PROBLEM_DATA_CUBE_HH
#define SRC_MULTI_BODY_PROBLEM_DATA_CUBE_HH


// works in any space dimension
template <int dim>
class Cube {
private:
    using Vector = Dune::FieldVector<double, dim>;

    // generate a combination of unit basis vectors from binary number
    void parseBinary(int binary, Vector& shift) const;

public:
    Cube(bool invariant = false);
    Cube(const Vector& A, const Vector& B, bool invariant = false);

    void setCorners(const Vector& A, const Vector& B) {
        A_ = A;
        B_ = B;
    }

    void setParent(Cube* parent) {
        parent_ = parent;
    }

    const std::vector<std::shared_ptr<Cube>>& children() const {
        return children_;
    }

    // constructs child cubes and sets children_
    void split();

private:
    using Vector = Dune::FieldVector<double, dim>;

    Vector A_; // two corners that are diagonal define dim-cube in any space dimension
    Vector B_;

    const bool invariant_; // flag to set if Cube can be split()

    std::shared_ptr<Cube<dim>> parent_ = nullptr;
    std::vector<std::shared_ptr<Cube<dim>>> children_;
    int nChildren_;
};

#endif


