#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"
#include "trianglegeometry.hh"

template class TriangleGeometry<typename Grid::ctype>;
