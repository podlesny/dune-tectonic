#ifndef SRC_MULTI_BODY_PROBLEM_DATA_CUBEGRIDCONSTRUCTOR_HH
#define SRC_MULTI_BODY_PROBLEM_DATA_CUBEGRIDCONSTRUCTOR_HH

#include "gridconstructor.hh"

#include <dune/common/fmatrix.hh>

#include <dune/fufem/boundarypatch.hh>


template <class GridType>
class CubeGridConstructor : public GridConstructor {    
public:
    using Cube = Cube<GridType::dimensionworld>;
    
    CubeGridConstructor(std::vector<std::shared_ptr<CuboidGeometry>> const &cuboidGeometries_);

    template <class GridView>
    void constructFaces(const GridView& gridView, CuboidGeometry const &cuboidGeometry, CubeFaces<GridView>& cubeFaces);

private:
  std::vector<std::shared_ptr<CuboidGeometry>> const &cuboidGeometries;
};


#endif


