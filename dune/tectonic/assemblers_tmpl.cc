#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "explicitgrid.hh"
#include "explicitvectors.hh"

#include <dune/fufem/assemblers/localassemblers/neumannboundaryassembler.hh>
#include <dune/fufem/boundarypatch.hh>

#include "assemblers.hh"

template class MyAssembler<DefLeafGridView, MY_DIM>;


using MyNeumannBoundaryAssembler = NeumannBoundaryAssembler<DeformedGrid, typename ScalarVector::block_type>;

template void MyAssembler<DefLeafGridView, MY_DIM>::assembleBoundaryFunctional<MyNeumannBoundaryAssembler, ScalarVector>(
        MyNeumannBoundaryAssembler& localAssembler,
        ScalarVector& b,
        const BoundaryPatch<DefLeafGridView>& boundaryPatch,
        bool initializeVector) const;
