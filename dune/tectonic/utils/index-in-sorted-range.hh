#ifndef DUNE_TECTONIC_INDEX_IN_SORTED_RANGE_HH
#define DUNE_TECTONIC_INDEX_IN_SORTED_RANGE_HH

#include <algorithm>

// returns v.size() if value does not exist
template <typename T>
size_t indexInSortedRange(std::vector<T> const &v, T value) {
  size_t const specialReturnValue = v.size();

  auto const b = std::begin(v);
  auto const e = std::end(v);
  auto const lb = std::lower_bound(b, e, value);

  if (lb == e) // all elements are strictly smaller
    return specialReturnValue;

  if (value < *lb) // value falls between to elements
    return specialReturnValue;

  return std::distance(b, lb);
}
#endif
