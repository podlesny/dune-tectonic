#ifndef DUNE_TECTONIC_GEOCOORDINATE_HH
#define DUNE_TECTONIC_GEOCOORDINATE_HH

// tiny helper to make a common piece of code pleasanter to read

template <class Geometry>
typename Geometry::GlobalCoordinate geoToPoint(Geometry geo) {
  assert(geo.corners() == 1);
  return geo.corner(0);
}

#endif
