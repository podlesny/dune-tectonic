#ifndef SRC_ALMOSTEQUAL_HH
#define SRC_ALMOSTEQUAL_HH

#include <type_traits>
#include <limits>
#include <math.h>

template <typename ctype>
typename std::enable_if<!std::numeric_limits<ctype>::is_integer, bool>::type almost_equal(ctype x, ctype y, int ulp) {
    return std::abs(x-y) < std::numeric_limits<ctype>::epsilon() * std::abs(x+y) * ulp || std::abs(x-y) < std::numeric_limits<ctype>::min();
}

#endif
