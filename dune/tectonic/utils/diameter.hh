#ifndef SRC_DIAMETER_HH
#define SRC_DIAMETER_HH

template <class Geometry> double diameter(Geometry const &geometry) {
  auto const numCorners = geometry.corners();
  std::vector<typename Geometry::GlobalCoordinate> corners(numCorners);

  double diameter = 0.0;
  for (int i = 0; i < numCorners; ++i) {
    corners[i] = geometry.corner(i);
    for (int j = 0; j < i; ++j)
      diameter = std::max(diameter, (corners[i] - corners[j]).two_norm());
  }
  return diameter;
}
#endif
