#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/fufem/geometry/convexpolyhedron.hh>

#include <dune/contact/projections/normalprojection.hh>

#include "../problem-data/bc.hh"
#include "../problem-data/myglobalfrictiondata.hh"

#include "../utils/diameter.hh"

#include "threeblocksfactory.hh"

template <class HostGridType, class VectorTEMPLATE>
void ThreeBlocksFactory<HostGridType, VectorTEMPLATE>::setBodies() {
    // set up cuboid geometries

    std::array<double, 3> lengths = {2.0, 1.0, 1.0};
    std::array<double, 3> heights = {1.0, 1.0, 1.0};

    std::array<GlobalCoords, 3> origins;

    const auto& subParset = this->parset_.sub("boundary.friction.weakening");

#if MY_DIM == 3 // TODO: not implemented
        double const depth = 0.60;

        origins[0] = {0, 0, 0};
        origins[1] = {0, origins[0][1] + heights[0], 0};
        origins[2] = {origins[1][0] + lengths[1], origins[0][1] + heights[0], 0};

        cuboidGeometries_[0] = std::make_shared<CuboidGeometry>(origins[0], lengths[0], heights[0], depth);
        cuboidGeometries_[0]->addWeakeningPatch(subParset, {origins[0][0], origins[0][1]+ heights[0], 0}, {origins[0][0] + lengths[0], origins[0][1]+ heights[0], 0});

        cuboidGeometries_[1] = std::make_shared<CuboidGeometry>(origins[1], lengths[1], heights[1], depth);
        cuboidGeometries_[1]->addWeakeningPatch(subParset, origins[1], {origins[1][0] + lengths[1], origins[1][1], 0});
        cuboidGeometries_[1]->addWeakeningPatch(subParset, origins[1], {origins[1][0] + lengths[1], origins[1][1] + heights[1], 0});

        cuboidGeometries_[2] = std::make_shared<CuboidGeometry>(origins[2], lengths[2], heights[2], depth);
        cuboidGeometries_[2]->addWeakeningPatch(subParset, origins[2], {origins[2][0] + lengths[2], origins[2][1], 0});
        cuboidGeometries_[2]->addWeakeningPatch(subParset, origins[2], {origins[2][0], origins[2][1] + widths[2], 0});
#elif MY_DIM == 2
        origins[0] = {0, 0};
        origins[1] = {0, origins[0][1] + heights[0]};
        origins[2] = {origins[1][0] + lengths[1], origins[0][1] + heights[0]};

        cuboidGeometries_[0] = std::make_shared<CuboidGeometry>(origins[0], lengths[0], heights[0]);
        cuboidGeometries_[0]->addWeakeningPatch(subParset, {origins[0][0], origins[0][1]+ heights[0]}, {origins[0][0] + lengths[0], origins[0][1]+ heights[0]});

        cuboidGeometries_[1] = std::make_shared<CuboidGeometry>(origins[1], lengths[1], heights[1]);
        cuboidGeometries_[1]->addWeakeningPatch(subParset, origins[1], {origins[1][0] + lengths[1], origins[1][1]});
        cuboidGeometries_[1]->addWeakeningPatch(subParset, {origins[1][0] + lengths[1], origins[1][1]}, {origins[1][0] + lengths[1], origins[1][1] + heights[1]});

        cuboidGeometries_[2] = std::make_shared<CuboidGeometry>(origins[2], lengths[2], heights[2]);
        cuboidGeometries_[2]->addWeakeningPatch(subParset, origins[2], {origins[2][0] + lengths[2], origins[2][1]});
        cuboidGeometries_[2]->addWeakeningPatch(subParset, origins[2], {origins[2][0], origins[2][1] + heights[2]});
#else
#error CuboidGeometry only supports 2D and 3D!"
#endif

    // set up reference grids
    gridConstructor_ = std::make_unique<GridsConstructor<HostGridType>>(cuboidGeometries_);
    auto& grids = gridConstructor_->getGrids();

    for (size_t i=0; i<this->bodyCount_; i++) {
        const auto& cuboidGeometry = *cuboidGeometries_[i];

        // define weak patch and refine grid
        const auto& weakeningRegions = cuboidGeometry.weakeningRegions();
        for (size_t j=0; j<weakeningRegions.size(); j++) {
            refine(*grids[i], weakeningRegions[j], this->parset_.template get<double>("boundary.friction.smallestDiameter"), CuboidGeometry::lengthScale());
        }

        // determine minDiameter and maxDiameter
        double minDiameter = std::numeric_limits<double>::infinity();
        double maxDiameter = 0.0;
        for (auto &&e : elements(grids[i]->leafGridView())) {
          auto const geometry = e.geometry();
          auto const diam = diameter(geometry);
          minDiameter = std::min(minDiameter, diam);
          maxDiameter = std::max(maxDiameter, diam);
        }
        std::cout << "Grid" << i << " min diameter: " << minDiameter << std::endl;
        std::cout << "Grid" << i << " max diameter: " << maxDiameter << std::endl;
    }

    for (size_t i=0; i<this->bodyCount_; i++) {
        this->bodies_[i] = std::make_shared<typename Base::LeafBody>(bodyData_, grids[i]);
    }
}

template <class HostGridType, class VectorTEMPLATE>
void ThreeBlocksFactory<HostGridType, VectorTEMPLATE>::setLevelBodies() {
    const size_t maxLevel = std::max({this->bodies_[0]->grid()->maxLevel(), this->bodies_[1]->grid()->maxLevel(), this->bodies_[2]->grid()->maxLevel()});

    for (size_t l=0; l<=maxLevel; l++) {
        std::vector<size_t> bodyLevels(3, l);
        this->contactNetwork_.addLevel(bodyLevels, l);
    }
}

template <class HostGridType, class VectorTEMPLATE>
void ThreeBlocksFactory<HostGridType, VectorTEMPLATE>::setCouplings() {
    for (size_t i=0; i<this->bodyCount_; i++) {
        const auto& cuboidGeometry = *cuboidGeometries_[i];
        std::cout << this->bodies_[i]->nVertices() << std::endl;
        std::cout << (leafFaces_[i] == nullptr) << std::endl;
        leafFaces_[i] = std::make_shared<LeafFaces>(this->bodies_[i]->gridView(), cuboidGeometry);
        levelFaces_[i] = std::make_shared<LevelFaces>(this->bodies_[i]->grid()->levelGridView(0), cuboidGeometry);
    }

    auto contactProjection = std::make_shared<Dune::Contact::NormalProjection<LeafBoundaryPatch>>();
    std::shared_ptr<typename Base::FrictionCouplingPair::BackEndType> backend = nullptr;

    std::array<std::array<size_t, 2>, 3> couplingIndices;

    nonmortarPatch_[0] = std::make_shared<LevelBoundaryPatch>(levelFaces_[1]->lower);
    mortarPatch_[0] = std::make_shared<LevelBoundaryPatch>(levelFaces_[0]->upper);
    weakPatches_[0] = std::make_shared<WeakeningRegion>(cuboidGeometries_[1]->weakeningRegions()[0]);
    couplingIndices[0] = {1, 0};

    nonmortarPatch_[1] = std::make_shared<LevelBoundaryPatch>(levelFaces_[2]->lower);
    mortarPatch_[1] = std::make_shared<LevelBoundaryPatch>(levelFaces_[0]->upper);
    weakPatches_[1] = std::make_shared<WeakeningRegion>(cuboidGeometries_[2]->weakeningRegions()[0]);
    couplingIndices[1] = {2, 0};

    nonmortarPatch_[2] = std::make_shared<LevelBoundaryPatch>(levelFaces_[1]->right);
    mortarPatch_[2] = std::make_shared<LevelBoundaryPatch>(levelFaces_[2]->left);
    weakPatches_[2] = std::make_shared<WeakeningRegion>(cuboidGeometries_[1]->weakeningRegions()[1]);
    couplingIndices[2] = {1, 2};

    for (size_t i=0; i<this->couplings_.size(); i++) {
      auto& coupling = this->couplings_[i];
      coupling = std::make_shared<typename Base::FrictionCouplingPair>();

      coupling->set(couplingIndices[i][0], couplingIndices[i][1], nonmortarPatch_[i], mortarPatch_[i], 0.1, Base::FrictionCouplingPair::CouplingType::STICK_SLIP, contactProjection, backend);
      coupling->setWeakeningPatch(weakPatches_[i]);
      coupling->setFrictionData(std::make_shared<MyGlobalFrictionData<GlobalCoords>>(this->parset_.sub("boundary.friction"), *weakPatches_[i], CuboidGeometry::lengthScale()));
    }
}

template <class HostGridType, class VectorTEMPLATE>
void ThreeBlocksFactory<HostGridType, VectorTEMPLATE>::setBoundaryConditions() {
    using LeafBoundaryCondition = BoundaryCondition<LeafGridView, dim>;

    using Function = Dune::VirtualFunction<double, double>;
    std::shared_ptr<Function> neumannFunction = std::make_shared<NeumannCondition>();
    std::shared_ptr<Function> velocityDirichletFunction = std::make_shared<VelocityDirichletCondition>(this->parset_.template get<double>("boundary.dirichlet.finalVelocity"));

    const double lengthScale = CuboidGeometry::lengthScale();

    for (size_t i=0; i<this->bodyCount_; i++) {
        const auto& body = this->contactNetwork_.body(i);
        const auto& leafVertexCount = body->nVertices();

        // Neumann boundary
        std::shared_ptr<LeafBoundaryCondition> neumannBoundary = std::make_shared<LeafBoundaryCondition>(std::make_shared<LeafBoundaryPatch>(body->gridView()), neumannFunction, "neumann");
        body->addBoundaryCondition(neumannBoundary);

        // upper Dirichlet Boundary
        // identify Dirichlet nodes on leaf level
        if (i>0) {
            std::shared_ptr<Dune::BitSetVector<dim>> velocityDirichletNodes = std::make_shared<Dune::BitSetVector<dim>>(leafVertexCount);
            for (int j=0; j<leafVertexCount; j++) {
                if (leafFaces_[i]->upper.containsVertex(j))
                    (*velocityDirichletNodes)[j][0] = true;

                #if MY_DIM == 3 //TODO: wrong, needs revision
                if (leafFaces_[i]->front.containsVertex(j) || leafFaces_[i]->back.containsVertex(j))
                    zeroDirichletNodes->at(j)[2] = true;
                #endif
            }

            std::shared_ptr<LeafBoundaryCondition> velocityDirichletBoundary = std::make_shared<LeafBoundaryCondition>("dirichlet");

            velocityDirichletBoundary->setBoundaryPatch(body->gridView(), velocityDirichletNodes);
            velocityDirichletBoundary->setBoundaryFunction(velocityDirichletFunction);
            body->addBoundaryCondition(velocityDirichletBoundary);
        }
    }

    // lower Dirichlet Boundary
    const auto& firstBody = this->contactNetwork_.body(0);
    const auto& firstLeafVertexCount = firstBody->nVertices();
    std::shared_ptr<Dune::BitSetVector<dim>> zeroDirichletNodes = std::make_shared<Dune::BitSetVector<dim>>(firstLeafVertexCount);
    for (int j=0; j<firstLeafVertexCount; j++) {
        if (leafFaces_[0]->lower.containsVertex(j)) {
            for (size_t d=0; d<dim; d++) {
              (*zeroDirichletNodes)[j][d] = true;
            }
        }
    }

    std::shared_ptr<LeafBoundaryCondition> zeroDirichletBoundary = std::make_shared<LeafBoundaryCondition>("dirichlet");
    zeroDirichletBoundary->setBoundaryPatch(firstBody->gridView(), zeroDirichletNodes);

    std::shared_ptr<Function> zeroFunction = std::make_shared<NeumannCondition>();
    zeroDirichletBoundary->setBoundaryFunction(zeroFunction);
    firstBody->addBoundaryCondition(zeroDirichletBoundary);
}

#include "threeblocksfactory_tmpl.cc"
