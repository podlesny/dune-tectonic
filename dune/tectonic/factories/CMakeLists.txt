add_custom_target(tectonic_dune_factories SOURCES
  cantorfactory.hh
  cantorfactory.cc 
  contactnetworkfactory.hh
  levelcontactnetworkfactory.hh
  stackedblocksfactory.hh
  stackedblocksfactory.cc 
  strikeslipfactory.hh
  strikeslipfactory.cc
  threeblocksfactory.hh
  threeblocksfactory.cc 
  twoblocksfactory.hh
  twoblocksfactory.cc 
)

#install headers
install(FILES
  cantorfactory.hh
  contactnetworkfactory.hh
  levelcontactnetworkfactory.hh
  stackedblocksfactory.hh
  strikeslipfactory.hh
  threeblocksfactory.hh
  twoblocksfactory.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/tectonic)
