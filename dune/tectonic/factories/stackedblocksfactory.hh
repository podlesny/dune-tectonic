#ifndef SRC_STACKEDBLOCKSFACTORY_HH
#define SRC_STACKEDBLOCKSFACTORY_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/function.hh>
#include <dune/common/fvector.hh>

#include <dune/fufem/boundarypatch.hh>

#include "contactnetworkfactory.hh"

#include "../problem-data/mybody.hh"
#include "../problem-data/grid/cuboidgridconstructor.hh"
#include "../problem-data/grid/cuboidgeometry.hh"

template <class HostGridType, class VectorType> class StackedBlocksFactory : public ContactNetworkFactory<HostGridType, VectorType>{
private:
    using Base = ContactNetworkFactory<HostGridType, VectorType>;

public:
    using ContactNetwork = typename Base::ContactNetwork;

private:
    using GlobalCoords = typename ContactNetwork::LocalVector;

    using LeafGridView = typename ContactNetwork::GridView;
    using LevelGridView = typename ContactNetwork::GridType::LevelGridView;

    using LevelBoundaryPatch = BoundaryPatch<LevelGridView>;
    using LeafBoundaryPatch = BoundaryPatch<LeafGridView>;

    using LeafFaces = MyFaces<LeafGridView>;
    using LevelFaces = MyFaces<LevelGridView>;

    using CuboidGeometry= CuboidGeometry<typename GlobalCoords::field_type>;
    using WeakeningRegion = typename CuboidGeometry::WeakeningRegion;

    static const int dim = ContactNetwork::dim;

    const std::shared_ptr<MyBodyData<dim>> bodyData_;     // material properties of bodies

    std::vector<std::shared_ptr<CuboidGeometry>> cuboidGeometries_;

    std::vector<std::shared_ptr<LeafFaces>> leafFaces_;
    std::vector<std::shared_ptr<LevelFaces>> levelFaces_;

    std::vector<std::shared_ptr<WeakeningRegion>> weakPatches_;

    std::vector<std::shared_ptr<LevelBoundaryPatch>> nonmortarPatch_;
    std::vector<std::shared_ptr<LevelBoundaryPatch>> mortarPatch_;

public:
    StackedBlocksFactory(const Dune::ParameterTree& parset) :
        Base(parset, parset.get<size_t>("problem.bodyCount"), parset.get<size_t>("problem.bodyCount")-1),
        bodyData_(std::make_shared<MyBodyData<dim>>(this->parset_.sub("body"), this->parset_.template get<double>("general.gravity"), zenith_())),
        cuboidGeometries_(this->bodyCount_),
        leafFaces_(this->bodyCount_),
        levelFaces_(this->bodyCount_),
        weakPatches_(this->bodyCount_),
        nonmortarPatch_(this->couplingCount_),
        mortarPatch_(this->couplingCount_)
    {}

    void setBodies();
    void setLevelBodies();
    void setCouplings();
    void setLevelCouplings() {}
    void setBoundaryConditions();

    auto& weakPatches() {
        return weakPatches_;
    }

private:
    static constexpr Dune::FieldVector<double, MY_DIM> zenith_() {
        #if MY_DIM == 2
        return {0, 1};
        #elif MY_DIM == 3
        return {0, 1, 0};
        #endif
    }
};
#endif


