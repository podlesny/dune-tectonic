#ifndef SRC_CONTACTNETWORKFACTORY_HH
#define SRC_CONTACTNETWORKFACTORY_HH

#include <dune/common/parametertree.hh>

#include "../data-structures/levelcontactnetwork.hh"

template <class HostGridType>
class ContactNetworkFactory {
public:
    using LevelContactNetwork = LevelContactNetwork<GridType, dims>;

protected:
    using Body = typename LevelContactNetwork::Body;
    using FrictionCouplingPair = typename LevelContactNetwork::FrictionCouplingPair;

    const Dune::ParameterTree& parset_;
    const size_t bodyCount_;
    const size_t couplingCount_;

    std::vector<std::shared_ptr<Body>> bodies_;
    std::vector<std::shared_ptr<FrictionCouplingPair>> couplings_;

    LevelContactNetwork levelContactNetwork_;

private:
    virtual void setBodies() = 0;
    virtual void setCouplings() = 0;
    virtual void setBoundaryConditions() = 0;

public:
    LevelContactNetworkFactory(const Dune::ParameterTree& parset, size_t bodyCount, size_t couplingCount) :
        parset_(parset),
        bodyCount_(bodyCount),
        couplingCount_(couplingCount),
        bodies_(bodyCount_),
        couplings_(couplingCount_),
        levelContactNetwork_(bodyCount_, couplingCount_) {}

    void build() {
        setBodies();
        levelContactNetwork_.setBodies(bodies_);

        setCouplings();
        levelContactNetwork_.setCouplings(couplings_);

        setBoundaryConditions();
    }

    LevelContactNetwork& levelContactNetwork() {
        return levelContactNetwork_;
    }
};
#endif
