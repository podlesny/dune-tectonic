#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>

#include <dune/fufem/geometry/convexpolyhedron.hh>

#include <dune/contact/projections/normalprojection.hh>

#include "../problem-data/bc.hh"
#include "../problem-data/myglobalfrictiondata.hh"

#include "../utils/diameter.hh"

#include "twoblocksfactory.hh"

template <class HostGridType, class VectorTEMPLATE>
void TwoBlocksFactory<HostGridType, VectorTEMPLATE>::setBodies() {
    // set up cuboid geometries

    std::array<double, 2> lengths = {this->parset_.template get<double>("body0.length"), this->parset_.template get<double>("body1.length")};
    std::array<double, 2> heights = {this->parset_.template get<double>("body0.height"), this->parset_.template get<double>("body1.height")};

    std::array<GlobalCoords, 2> origins;

    const auto& frictionParset = this->parset_.sub("boundary.friction");

#if MY_DIM == 3 // TODO: not implemented
        std::array<double, 2> depths = {this->parset_.template get<double>("body0.depth"), this->parset_.template get<double>("body1.depth")};

        origins[0] = {0, 0, 0};
        origins[1] = {lengths[0]/2.0, origins[0][1] + heights[0], 0};

        cuboidGeometries_[0] = std::make_shared<CuboidGeometry>(origins[0], lengths[0], heights[0], depths[0]);
        cuboidGeometries_[0]->addWeakeningPatch(frictionParset, {origins[0][0], origins[0][1]+ heights[0], 0}, {origins[0][0] + lengths[0], origins[0][1]+ heights[0], 0});

        cuboidGeometries_[1] = std::make_shared<CuboidGeometry>(origins[1], lengths[1], heights[1], depths[1]);
        cuboidGeometries_[1]->addWeakeningPatch(frictionParset, origins[1], {origins[1][0] + lengths[1], origins[1][1], 0});
#elif MY_DIM == 2
        origins[0] = {-lengths[0]/2.0, 0};
        origins[1] = {-lengths[1]/2.0, origins[0][1] + heights[0]};

        cuboidGeometries_[0] = std::make_shared<CuboidGeometry>(origins[0], lengths[0], heights[0]);
        cuboidGeometries_[0]->addWeakeningPatch(frictionParset, {origins[0][0], origins[0][1]+ heights[0]}, {origins[0][0] + lengths[0], origins[0][1]+ heights[0]});

        cuboidGeometries_[1] = std::make_shared<CuboidGeometry>(origins[1], lengths[1], heights[1]);
        cuboidGeometries_[1]->addWeakeningPatch(frictionParset, origins[1], {origins[1][0] + lengths[1], origins[1][1]});
#else
#error CuboidGeometry only supports 2D and 3D!"
#endif

    const auto gravity = this->parset_.template get<double>("general.gravity");

    for (size_t i=0; i<this->bodyCount_; i++) {
        std::string body_str = "body" + std::to_string(i);
        const auto& cuboidGeometry = *cuboidGeometries_[i];

        std::array<unsigned int, 2> initialElements = {
            { this->parset_.template get<size_t>("body" + std::to_string(i) + ".initialXElements"),
              this->parset_.template get<size_t>("body" + std::to_string(i) + ".initialYElements") } };

        // set up reference grid
        DefaultCuboidGridConstructor<HostGridType> gridConstructor(cuboidGeometry, initialElements);
        gridConstructor.refine(this->parset_.template get<size_t>(body_str + ".refinements"));

        bodyData_[i] = std::make_shared<MyBodyData<dim>>(this->parset_.sub(body_str), gravity, zenith_());
        this->bodies_[i] = std::make_shared<typename Base::LeafBody>(bodyData_[i], gridConstructor.grid());
    }
}

template <class HostGridType, class VectorTEMPLATE>
void TwoBlocksFactory<HostGridType, VectorTEMPLATE>::setLevelBodies() {
    const size_t maxLevel = std::max({this->bodies_[0]->grid()->maxLevel(), this->bodies_[1]->grid()->maxLevel()});

    for (size_t l=0; l<=maxLevel; l++) {
        std::vector<size_t> bodyLevels(2, l);
        this->contactNetwork_.addLevel(bodyLevels, l);
    }
}

template <class HostGridType, class VectorTEMPLATE>
void TwoBlocksFactory<HostGridType, VectorTEMPLATE>::setCouplings() {
    for (size_t i=0; i<this->bodyCount_; i++) {
        const auto& cuboidGeometry = *cuboidGeometries_[i];
        leafFaces_[i] = std::make_shared<LeafFaces>(this->bodies_[i]->gridView(), cuboidGeometry);
        levelFaces_[i] = std::make_shared<LevelFaces>(this->bodies_[i]->grid()->levelGridView(0), cuboidGeometry);
    }

    auto contactProjection = std::make_shared<Dune::Contact::NormalProjection<LeafBoundaryPatch>>();

    nonmortarPatch_[0] = std::make_shared<LevelBoundaryPatch>(levelFaces_[1]->lower);
    mortarPatch_[0] = std::make_shared<LevelBoundaryPatch>(levelFaces_[0]->upper);
    weakPatches_[0] = std::make_shared<WeakeningRegion>(cuboidGeometries_[1]->weakeningRegions()[0]);

    auto& coupling = this->couplings_[0];
    coupling = std::make_shared<typename Base::FrictionCouplingPair>();

    coupling->set(1, 0, nonmortarPatch_[0], mortarPatch_[0], 0.1, Base::FrictionCouplingPair::CouplingType::STICK_SLIP, contactProjection, nullptr);
    coupling->setWeakeningPatch(weakPatches_[0]);
    coupling->setFrictionData(std::make_shared<MyGlobalFrictionData<GlobalCoords>>(this->parset_.sub("boundary.friction"), *weakPatches_[0], CuboidGeometry::lengthScale()));
}

template <class HostGridType, class VectorTEMPLATE>
void TwoBlocksFactory<HostGridType, VectorTEMPLATE>::setBoundaryConditions() {
    using LeafBoundaryCondition = BoundaryCondition<LeafGridView, dim>;

    using Function = Dune::VirtualFunction<double, double>;
    std::shared_ptr<Function> neumannFunction = std::make_shared<NeumannCondition>();
    //std::shared_ptr<Function> velocityDirichletFunction = std::make_shared<VelocityDirichletConditionLinearLoading>(this->parset_.template get<double>("boundary.dirichlet.finalVelocity"), 0.25);
    std::shared_ptr<Function> velocityDirichletFunction = std::make_shared<VelocityDirichletCondition>(this->parset_.template get<double>("boundary.dirichlet.finalVelocity"), 0.25);
    //std::shared_ptr<Function> velocityDirichletFunction = std::make_shared<VelocityStepDirichletCondition>(this->parset_.template get<double>("boundary.dirichlet.finalVelocity"), 10*this->parset_.template get<double>("boundary.dirichlet.finalVelocity"), 0.25, 0.5);

    const double lengthScale = CuboidGeometry::lengthScale();

    // body0: Dirichlet boundary (lower)
    const auto& body0 = this->contactNetwork_.body(0);
    const auto& leafVertexCount0 = body0->nVertices();
    std::shared_ptr<Dune::BitSetVector<dim>> zeroDirichletNodes = std::make_shared<Dune::BitSetVector<dim>>(leafVertexCount0);
    for (int j=0; j<leafVertexCount0; j++) {
        if (leafFaces_[0]->lower.containsVertex(j)) {
            for (size_t d=0; d<dim; d++) {
              (*zeroDirichletNodes)[j][d] = true;
            }
        }
    }

    std::shared_ptr<LeafBoundaryCondition> zeroDirichletBoundary = std::make_shared<LeafBoundaryCondition>("dirichlet");
    zeroDirichletBoundary->setBoundaryPatch(body0->gridView(), zeroDirichletNodes);

    std::shared_ptr<Function> zeroFunction = std::make_shared<NeumannCondition>();
    zeroDirichletBoundary->setBoundaryFunction(zeroFunction);
    body0->addBoundaryCondition(zeroDirichletBoundary);

    // body1: Dirichlet boundary (upper)
    const auto& body1 = this->contactNetwork_.body(1);
    const auto& leafVertexCount1 = body1->nVertices();
    std::shared_ptr<Dune::BitSetVector<dim>> velocityDirichletNodes = std::make_shared<Dune::BitSetVector<dim>>(leafVertexCount1);
    for (size_t j=0; j<leafVertexCount1; j++) {
        if (leafFaces_[1]->upper.containsVertex(j))
            (*velocityDirichletNodes)[j][0] = true;

        #if MY_DIM == 3 //TODO: wrong, needs revision
        if (leafFaces_[1]->front.containsVertex(j) || leafFaces_[1]->back.containsVertex(j))
            zeroDirichletNodes->at(j)[2] = true;
        #endif
    }

    std::shared_ptr<LeafBoundaryCondition> velocityDirichletBoundary = std::make_shared<LeafBoundaryCondition>("dirichlet");

    velocityDirichletBoundary->setBoundaryPatch(body1->gridView(), velocityDirichletNodes);
    velocityDirichletBoundary->setBoundaryFunction(velocityDirichletFunction);
    body1->addBoundaryCondition(velocityDirichletBoundary);

    // body0, body1: natural boundary conditions
    for (size_t i=0; i<this->bodyCount_; i++) {
        const auto& body = this->contactNetwork_.body(i);

        // Neumann boundary
        auto neumannBoundary = std::make_shared<LeafBoundaryCondition>("neumann");
        auto neumannPatch = std::make_shared<LeafBoundaryPatch>(body->gridView(), true);
        neumannBoundary->setBoundaryPatch(neumannPatch);
        neumannBoundary->setBoundaryFunction(neumannFunction);
        body->addBoundaryCondition(neumannBoundary);
    }

    // body1: Neumann boundary (upper)
    // normal load
    std::shared_ptr<Function> constantFunction = std::make_shared<NeumannCondition>(this->parset_.template get<double>("boundary.neumann.sigmaN"));

    std::shared_ptr<Dune::BitSetVector<dim>> loadNeumannNodes = std::make_shared<Dune::BitSetVector<dim>>(leafVertexCount1);
    for (size_t j=0; j<leafVertexCount1; j++) {
        if (leafFaces_[1]->upper.containsVertex(j))
            (*loadNeumannNodes)[j][1] = true;

        #if MY_DIM == 3 //TODO: wrong, needs revision
        if (leafFaces_[1]->front.containsVertex(j) || leafFaces_[1]->back.containsVertex(j))
            zeroDirichletNodes->at(j)[2] = true;
        #endif
    }

    auto loadNeumannBoundary = std::make_shared<LeafBoundaryCondition>("neumann");
    loadNeumannBoundary->setBoundaryPatch(body1->gridView(), loadNeumannNodes);
    loadNeumannBoundary->setBoundaryFunction(constantFunction);
    body1->addBoundaryCondition(loadNeumannBoundary);
}

#include "twoblocksfactory_tmpl.cc"
