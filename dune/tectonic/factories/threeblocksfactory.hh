#ifndef SRC_THREEBLOCKSFACTORY_HH
#define SRC_THREEBLOCKSFACTORY_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/function.hh>
#include <dune/common/fvector.hh>

#include <dune/fufem/boundarypatch.hh>

#include "contactnetworkfactory.hh"

#include "../problem-data/mybody.hh"
#include "../problem-data/grid/mygrids.hh"
#include "../problem-data/grid/cuboidgeometry.hh"

template <class HostGridType, class VectorType>
class ThreeBlocksFactory : public ContactNetworkFactory<HostGridType, VectorType>{
private:
    using Base = ContactNetworkFactory<HostGridType, VectorType>;

public:
    using ContactNetwork = typename Base::ContactNetwork;

private:
    using GlobalCoords = typename ContactNetwork::LocalVector;

    using LeafGridView = typename ContactNetwork::GridView;
    using LevelGridView = typename ContactNetwork::GridType::LevelGridView;

    using LevelBoundaryPatch = BoundaryPatch<LevelGridView>;
    using LeafBoundaryPatch = BoundaryPatch<LeafGridView>;

    using LeafFaces = MyFaces<LeafGridView>;
    using LevelFaces = MyFaces<LevelGridView>;

    using CuboidGeometry= CuboidGeometry<typename GlobalCoords::field_type>;
    using WeakeningRegion = typename CuboidGeometry::WeakeningRegion;

    static const int dim = ContactNetwork::dim;

    const std::shared_ptr<MyBodyData<dim>> bodyData_;     // material properties of bodies

    std::unique_ptr<GridsConstructor<HostGridType>> gridConstructor_;

    std::vector<std::shared_ptr<CuboidGeometry>> cuboidGeometries_;

    std::vector<std::shared_ptr<LeafFaces>> leafFaces_;
    std::vector<std::shared_ptr<LevelFaces>> levelFaces_;

    std::vector<std::shared_ptr<WeakeningRegion>> weakPatches_;

    std::vector<std::shared_ptr<LevelBoundaryPatch>> nonmortarPatch_;
    std::vector<std::shared_ptr<LevelBoundaryPatch>> mortarPatch_;

public:
    ThreeBlocksFactory(const Dune::ParameterTree& parset) :
        Base(parset, 3, 3),
        bodyData_(std::make_shared<MyBodyData<dim>>(this->parset_.sub("body"), this->parset_.template get<double>("gravity"), zenith_())),
        cuboidGeometries_(3),
        leafFaces_(3),
        levelFaces_(3),
        weakPatches_(3),
        nonmortarPatch_(3),
        mortarPatch_(3)
    {}

    void setBodies();
    void setLevelBodies();
    void setCouplings();
    void setLevelCouplings() {}
    void setBoundaryConditions();

    auto& weakPatches() {
        return weakPatches_;
    }
private:
    static constexpr Dune::FieldVector<double, MY_DIM> zenith_() {
        #if MY_DIM == 2
        return {0, 1};
        #elif MY_DIM == 3
        return {0, 1, 0};
        #endif
    }
};
#endif


