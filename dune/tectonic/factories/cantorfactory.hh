#ifndef SRC_CANTORFACTORY_HH
#define SRC_CANTORFACTORY_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/function.hh>
#include <dune/common/fvector.hh>

#include <dune/fufem/boundarypatch.hh>

#include "contactnetworkfactory.hh"

#include "../multi-body-problem-data/mybody.hh"
#include "../multi-body-problem-data/grid/mygrids.hh"




template <class GridType, int dim> class CantorFactory : public ContactNetworkFactory<GridType, dim>{
private:
    using Base = ContactNetworkFactory<GridType, dim>;

    using LevelCubes = std::vector<std::shared_ptr<Cube>>;

    using LocalVector = typename Base::ContactNetwork::LocalVector;

    using DeformedLeafGridView = typename Base::ContactNetwork::DeformedLeafGridView;
    using DeformedLevelGridView = typename Base::ContactNetwork::DeformedLevelGridView;

    using LevelBoundaryPatch = BoundaryPatch<DeformedLevelGridView>;
    using LeafBoundaryPatch = BoundaryPatch<DeformedLeafGridView>;

    using LeafFaces = MyFaces<DeformedLeafGridView>;
    using LevelFaces = MyFaces<DeformedLevelGridView>;

private:
    const std::shared_ptr<MyBodyData<dim>> bodyData_;     // material properties of bodies

    GridsConstructor<GridType>* gridConstructor_;

    std::vector<LevelCubes> cubes_;

    std::vector<std::shared_ptr<LeafFaces>> leafFaces_;
    std::vector<std::shared_ptr<LevelFaces>> levelFaces_;

    std::vector<std::shared_ptr<ConvexPolyhedron<LocalVector>>> lowerWeakPatches_;
    std::vector<std::shared_ptr<ConvexPolyhedron<LocalVector>>> upperWeakPatches_;

    std::vector<std::shared_ptr<LevelBoundaryPatch>> nonmortarPatch_;
    std::vector<std::shared_ptr<LevelBoundaryPatch>> mortarPatch_;

public:
    CantorFactory(const Dune::ParameterTree& parset) :
        Base(parset, parset.get<size_t>("problem.bodyCount"), parset.get<size_t>("problem.bodyCount")-1),
        bodyData_(std::make_shared<MyBodyData<dim>>(this->parset_, zenith_())),
        cuboidGeometries_(this->bodyCount_),
        leafFaces_(this->bodyCount_),
        levelFaces_(this->bodyCount_),
        lowerWeakPatches_(this->bodyCount_),
        upperWeakPatches_(this->bodyCount_),
        nonmortarPatch_(this->couplingCount_),
        mortarPatch_(this->couplingCount_)
    {}

    ~CantorFactory() {
        delete gridConstructor_;
    }

    void setBodies();
    void setCouplings();
    void setBoundaryConditions();

private:
    static constexpr Dune::FieldVector<double, MY_DIM> zenith_() {
        #if MY_DIM == 2
        return {0, 1};
        #elif MY_DIM == 3
        return {0, 1, 0};
        #endif
    }
};
#endif


