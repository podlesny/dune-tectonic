#ifndef DUNE_TECTONIC_FACTORIES_STRIKESLIPFACTORY_HH
#define DUNE_TECTONIC_FACTORIES_STRIKESLIPFACTORY_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/function.hh>
#include <dune/common/fvector.hh>

#include <dune/fufem/boundarypatch.hh>

#include "contactnetworkfactory.hh"

#include "../problem-data/strikeslipbody.hh"
#include "../problem-data/grid/trianglegrids.hh"
#include "../problem-data/grid/trianglegeometry.hh"

template <class HostGridType, class VectorType> class StrikeSlipFactory : public ContactNetworkFactory<HostGridType, VectorType>{
private:
    using Base = ContactNetworkFactory<HostGridType, VectorType>;

public:
    using ContactNetwork = typename Base::ContactNetwork;

private:
    using GlobalCoords = typename ContactNetwork::LocalVector;

    using LeafGridView = typename ContactNetwork::GridView;
    using LevelGridView = typename ContactNetwork::GridType::LevelGridView;

    using LevelBoundaryPatch = BoundaryPatch<LevelGridView>;
    using LeafBoundaryPatch = BoundaryPatch<LeafGridView>;

    using LeafFaces = MyFaces<LeafGridView>;
    using LevelFaces = MyFaces<LevelGridView>;

    using TriangleGeometry= TriangleGeometry<typename GlobalCoords::field_type>;
    using WeakeningRegion = typename TriangleGeometry::WeakeningRegion;

    static const int dim = ContactNetwork::dim;

    std::vector<std::shared_ptr<MyBodyData<dim>>> bodyData_;     // material properties of bodies

    std::unique_ptr<GridsConstructor<HostGridType>> gridConstructor_;

    std::vector<std::shared_ptr<TriangleGeometry>> triangleGeometries_;

    std::vector<std::shared_ptr<LeafFaces>> leafFaces_;
    std::vector<std::shared_ptr<LevelFaces>> levelFaces_;

    std::vector<std::shared_ptr<WeakeningRegion>> weakPatches_;

    std::vector<std::shared_ptr<LevelBoundaryPatch>> nonmortarPatch_;
    std::vector<std::shared_ptr<LevelBoundaryPatch>> mortarPatch_;

public:
    StrikeSlipFactory(const Dune::ParameterTree& parset) :
        Base(parset, 2, 1),
        bodyData_(2),
        triangleGeometries_(2),
        leafFaces_(2),
        levelFaces_(2),
        weakPatches_(2),
        nonmortarPatch_(1),
        mortarPatch_(1)
    {}

    void setBodies();
    void setLevelBodies();
    void setCouplings();
    void setLevelCouplings() {}
    void setBoundaryConditions();

    auto& weakPatches() {
        return weakPatches_;
    }

private:
    static constexpr Dune::FieldVector<double, MY_DIM> zenith_() {
        #if MY_DIM == 2
            return {0, 0};
        #else
            return {0, 0, 1};
        #endif
    }

    std::vector<double> parsePatchString(const std::string& patch_str);
};
#endif


