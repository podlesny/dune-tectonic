#ifndef SRC_VTK_BODYWRITER_HH
#define SRC_VTK_BODYWRITER_HH

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/fufem/functions/vtkbasisgridfunction.hh>
#include "../utils/debugutils.hh"

template <class VertexBasis, class CellBasis> class VTKBodyWriter {
private:
  CellBasis const &cellBasis_;
  VertexBasis const &vertexBasis_;
  std::string const prefix_;

public:
  VTKBodyWriter(CellBasis const &cellBasis, VertexBasis const &vertexBasis, std::string prefix) :
    cellBasis_(cellBasis), vertexBasis_(vertexBasis), prefix_(prefix)
  {}

  template <class Vector, class ScalarVector>
  void write(size_t record, Vector const &u, Vector const &v,
             ScalarVector const &alpha, ScalarVector const &stress) const {
      Dune::VTKWriter<typename VertexBasis::GridView> writer(
          vertexBasis_.getGridView());

      auto const displacementPointer =
          std::make_shared<VTKBasisGridFunction<VertexBasis, Vector> const>(
              vertexBasis_, u, "displacement");
      writer.addVertexData(displacementPointer);

      auto const velocityPointer =
          std::make_shared<VTKBasisGridFunction<VertexBasis, Vector> const>(
              vertexBasis_, v, "velocity");
      writer.addVertexData(velocityPointer);

      auto const AlphaPointer =
          std::make_shared<VTKBasisGridFunction<VertexBasis, ScalarVector> const>(
              vertexBasis_, alpha, "Alpha");
      writer.addVertexData(AlphaPointer);

      auto const stressPointer =
          std::make_shared<VTKBasisGridFunction<CellBasis, ScalarVector> const>(
              cellBasis_, stress, "stress");
      writer.addCellData(stressPointer);

      std::string const filename = prefix_ + "_" + std::to_string(record);
      writer.write(filename.c_str(), Dune::VTK::appendedraw);
  }

  void writeGrid() const {
      Dune::VTKWriter<typename VertexBasis::GridView> writer(
          vertexBasis_.getGridView());

      std::string const filename = prefix_ + "_grid";
      writer.write(filename.c_str(), Dune::VTK::appendedraw);
  }
};

#endif
