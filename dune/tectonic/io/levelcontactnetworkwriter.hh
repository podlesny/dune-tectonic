#ifndef LEVEL_INTERFACE_NETWORK_WRITER
#define LEVEL_INTERFACE_NETWORK_WRITER

#include <string>
#include <dune/common/fvector.hh>
#include <dune/grid/common/mcmgmapper.hh>
#include <dune/fufem/boundaryiterator.hh>
//#include <dune/faultnetworks/levelinterfacenetwork.hh>

class LevelContactNetworkWriter {
    private:

        //! Parameter for mapper class
        template<int dim>
        struct FaceMapperLayout
        {
            bool contains (Dune::GeometryType gt)
            {
                return gt.dim() == dim-1;
            }
        };

        template <typename Intersection>
        void writeIntersection(const Intersection& intersection, std::ofstream& file, const std::string colorTag) {
            typedef typename Dune::FieldVector<typename Intersection::ctype, Intersection::dimensionworld > GlobalCoordinates;

            const auto& geometry = intersection.geometry();

            std::vector<GlobalCoordinates> vertices;
            vertices.resize(geometry.corners());

            for (int i=0; i<geometry.corners(); i++) {
                const auto& vertex = geometry.corner(i);
                vertices[i] = vertex;
            }

            for (size_t i=0; i<vertices.size(); i++) {
                for (size_t j=i+1; j<vertices.size(); j++) {
                    file << "\\draw[" << colorTag << "] ";
                    writeVertex(vertices[i], file);
                    file << " -- ";
                    writeVertex(vertices[j], file);
                    file << ";%\n";
                }
            }
        }

        template <typename GlobalCoordinates>
        void writeVertex(const GlobalCoordinates& vertex, std::ofstream& file) {
            size_t lastIndex = vertex.size()-1;

            file << "(";
            for (size_t i=0; i<lastIndex; i++) {
                file << vertex[i] << ", ";
            }
            file << vertex[lastIndex] << ")";
        }


        template <typename Body>
        void writeBody(const int bodyIdx, const Body& body, std::ofstream& file, bool writeGrid) {
            const auto& gridView = body.gridView();

            std::string colorTag;

            using GridView = typename std::decay<decltype(gridView)>::type;

            typedef typename Dune::MultipleCodimMultipleGeomTypeMapper<GridView, FaceMapperLayout > FaceMapper;
            FaceMapper intersectionMapper(gridView);
            std::vector<bool> intersectionHandled(intersectionMapper.size(),false);

            // write grid if flag set
            colorTag = "grid" + std::to_string(bodyIdx) + ", very thin";
            if (writeGrid) {
                for (const auto& elem:elements(gridView)) {
                    for (const auto& isect:intersections(gridView, elem)) {

                        if (intersectionHandled[intersectionMapper.subIndex(elem, isect.indexInInside(),1)])
                            continue;

                        intersectionHandled[intersectionMapper.subIndex(elem, isect.indexInInside(),1)] = true;

                        if (isect.boundary())
                            continue;

                        writeIntersection(isect, file, colorTag);
                    }
                }
            }

            // write boundary
            colorTag = "boundary" + std::to_string(bodyIdx) + ", very thin";
            BoundaryIterator<GridView> bIt(gridView, BoundaryIterator<GridView>::begin);
            BoundaryIterator<GridView> bEnd(gridView, BoundaryIterator<GridView>::end);
            for(; bIt!=bEnd; ++bIt) {
                writeIntersection(*bIt, file, colorTag);
            }
        }

        template <typename BoundaryPatch>
        void writePatch(const int bodyIdx, const BoundaryPatch& patch, std::ofstream& file) {
            std::string colorTag;

            auto it = patch.begin();
            const auto itEnd = patch.end();
            for(; it!=itEnd; ++it) {
                colorTag = "contact" + std::to_string(bodyIdx) + ", thin";
                writeIntersection(*it, file, colorTag);
            }
        }

        template <typename Coupling>
        void writeCoupling(const Coupling& coupling, std::ofstream& file) {
            const auto& bodies = coupling.gridIdx_;
            writePatch(bodies[0], *coupling.patch0(), file);
            writePatch(bodies[1], *coupling.patch1(), file);
        }

    public:
        LevelContactNetworkWriter() {}

        // works only in 1D and 2D
        template <typename LevelContactNetwork>
        void write(const LevelContactNetwork& levelContactNetwork, const std::string filePath, bool writeGrid = false) {
            std::ofstream file (filePath);

            if (file.is_open()) {
                // define custom MATLAB colors
                file << "\\definecolor{MATLABred}{RGB}{162,20,47}%\n";
                file << "\\definecolor{MATLABblue}{RGB}{0,114,189}%\n";
                file << "\\definecolor{MATLABgreen}{RGB}{60,140,40}%\n";
                file << "\\definecolor{MATLABorange}{RGB}{212,83,25}%\n";
                file << "\\definecolor{MATLAByellow}{RGB}{237,177,32}%\n";
                file << "\\definecolor{lightGray}{RGB}{211,211,211}%\n";
                file << "%\n";

                for (size_t i=0; i<=levelContactNetwork.nBodies()-1; i++) {
                    file << "\\colorlet{boundary" << i << "}{black}%\n";
                    file << "\\colorlet{grid" << i << "}{lightGray}%\n";
                    file << "\\colorlet{contact" << i << "}{MATLABorange}%\n";
                    file << "%\n";
                }

                file << "\\begin{tikzpicture}[scale=\\scale]%\n";

                for (size_t i=0; i<levelContactNetwork.nBodies(); i++) {
                    writeBody(i, *levelContactNetwork.body(i), file, writeGrid);
                }

                for (size_t i=0; i<levelContactNetwork.nCouplings(); i++) {
                    writeCoupling(*levelContactNetwork.coupling(i), file);
                }

                file << "\\end{tikzpicture}%";
                file.close();
            } else {
                DUNE_THROW(Dune::Exception, "Unable to open " << filePath << " for writing!");
            }
        }
};
#endif
