#ifndef SRC_VTK_HH
#define SRC_VTK_HH

#include <string>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/fufem/functions/vtkbasisgridfunction.hh>

#include "../utils/debugutils.hh"
#include "vtk-bodywriter.hh"

template <class VertexBasis, class CellBasis> class MyVTKWriter {
private:
  std::vector<VTKBodyWriter<VertexBasis, CellBasis>* > bodyVTKWriters_;
  std::string const prefix_;

public:
  MyVTKWriter(const std::vector<const CellBasis* >& cellBases, const std::vector<const VertexBasis* >& vertexBases,
              std::string prefix): bodyVTKWriters_(vertexBases.size()), prefix_(prefix) {

      for (size_t i=0; i<bodyVTKWriters_.size(); i++) {
        bodyVTKWriters_[i] = new VTKBodyWriter<VertexBasis, CellBasis>(*cellBases[i], *vertexBases[i], prefix_+std::to_string(i));
      }
  }

  ~MyVTKWriter() {
      for (size_t i=0; i<bodyVTKWriters_.size(); i++) {
        delete bodyVTKWriters_[i];
      }
  }

  template <class Vector, class ScalarVector>
  void write(size_t record, const std::vector<Vector>& u, const std::vector<Vector>& v,
             const std::vector<ScalarVector>& alpha, const std::vector<ScalarVector>& stress) const {

      for (size_t i=0; i<bodyVTKWriters_.size(); i++) {
        bodyVTKWriters_[i]->write(record, u[i], v[i], alpha[i], stress[i]);
      }
    }

  void writeGrids() const {
      for (size_t i=0; i<bodyVTKWriters_.size(); i++) {
        bodyVTKWriters_[i]->writeGrid();
      }
  }
};

#endif
