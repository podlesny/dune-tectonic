#ifndef SRC_HDF5_BODYWRITER_HH
#define SRC_HDF5_BODYWRITER_HH

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/geometry/convexpolyhedron.hh>
#include <dune/fufem/hdf5/file.hh>

#include "hdf5/frictionalboundary-writer.hh"
//#include "hdf5/patchinfo-writer.hh"

template <class ProgramState, class VertexBasis, class GridView>
class HDF5BodyWriter {
private:
    using Vector = typename ProgramState::Vector;
    using ScalarVector = typename ProgramState::ScalarVector;
    //using PatchInfoWriter = PatchInfoWriter<ProgramState, VertexBasis, GridView>;

public:
    using VertexCoordinates = typename ProgramState::Vector;
    using Patch = typename FrictionalBoundaryWriter<GridView>::Patch;
    //using WeakPatches = std::vector<const ConvexPolyhedron<LocalVector>* >;

    using LocalVector = typename VertexCoordinates::block_type;

    //friend class HDF5LevelWriter<ProgramState, VertexBasis, GridView>;

    HDF5BodyWriter(
            HDF5::File& file,
            const size_t bodyID,
            const VertexCoordinates& vertexCoordinates,
            const VertexBasis& vertexBasis,
            const Patch& frictionPatch) :
           // const WeakPatches& weakPatches) :
        id_(bodyID),
        /*#if MY_DIM == 3
                patchInfoWriters_(patchCount_),
        #endif*/
        group_(file, "body"+std::to_string(id_)) {

        frictionBoundaryWriter_ = std::make_unique<FrictionalBoundaryWriter<GridView>>(group_, vertexCoordinates, frictionPatch);

        /*#if MY_DIM == 3
            patchInfoWriters_[i] = std::make_unique<PatchInfoWriter>(file_, vertexBasis, *frictionPatches[i], *weakPatches[i], i);
        #endif*/
    }

    template <class Friction>
    void reportSolution(const ProgramState& programState, const Vector& v, const ScalarVector& frictionCoeff, const Friction& friction) {
        frictionBoundaryWriter_->write(programState.timeStep, programState.u[id_], v, programState.alpha[id_], frictionCoeff, friction);

        /*#if MY_DIM == 3
        patchInfoWriters_[i]->write(programState);
        #endif*/
    }

    void reportWeightedNormalStress(const ProgramState& programState) {
        frictionBoundaryWriter_->writeWeightedNormalStress(programState.timeStep, programState.weightedNormalStress[id_], programState.weights[id_]);
    }

    auto id() const {
        return id_;
    }

private:
    const size_t id_;
    HDF5::Group group_;

/*#if MY_DIM == 3
    std::vector<std::unique_ptr<PatchInfoWriter>> patchInfoWriters_;
#endif*/

    std::unique_ptr<FrictionalBoundaryWriter<GridView>> frictionBoundaryWriter_;
};
#endif
