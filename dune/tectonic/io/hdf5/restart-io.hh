#ifndef SRC_IO_HDF_RESTART_IO_HH
#define SRC_IO_HDF_RESTART_IO_HH

#include <vector>

#include <dune/fufem/hdf5/file.hh>
#include <dune/fufem/hdf5/sequenceio.hh>

#include "restartbody-io.hh"

template <class ProgramState> class RestartIO {
private:
  std::vector<RestartBodyIO<ProgramState>* > bodyIO_;

  HDF5::SequenceIO<0> relativeTimeWriter_;
  HDF5::SequenceIO<0> relativeTimeIncrementWriter_;

public:
  RestartIO(HDF5::Grouplike& group, const std::vector<size_t>& vertexCounts)
      : bodyIO_(vertexCounts.size()),
        relativeTimeWriter_(group, "relativeTime"),
        relativeTimeIncrementWriter_(group, "relativeTimeIncrement") {

    for (size_t i=0; i<bodyIO_.size(); i++) {
      bodyIO_[i] = new RestartBodyIO<ProgramState>(group, vertexCounts[i], i);
    }
  }

  ~RestartIO() {
    for (size_t i=0; i<bodyIO_.size(); i++) {
      delete bodyIO_[i];
    }
  }

  void write(ProgramState const &programState) {
    assert(programState.size() == bodyIO_.size());

    for (size_t i=0; i<bodyIO_.size(); i++) {
      bodyIO_[i]->write(programState);
    }

    addEntry(relativeTimeWriter_, programState.timeStep,
             programState.relativeTime);
    addEntry(relativeTimeIncrementWriter_, programState.timeStep,
             programState.relativeTau);
  }

  void read(size_t timeStep, ProgramState& programState) {
    assert(programState.size() == bodyIO_.size());

    programState.timeStep = timeStep;

    for (size_t i=0; i<bodyIO_.size(); i++) {
      bodyIO_[i]->read(timeStep, programState);
    }

    readEntry(relativeTimeWriter_, timeStep, programState.relativeTime);
    readEntry(relativeTimeIncrementWriter_, timeStep, programState.relativeTau);
  }
};

#endif
