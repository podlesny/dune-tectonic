#ifndef DUNE_TECTONIC_HDF5_TIME_WRITER_HH
#define DUNE_TECTONIC_HDF5_TIME_WRITER_HH

#include <dune/fufem/hdf5/file.hh>
#include <dune/fufem/hdf5/sequenceio.hh>

template <class ProgramState>
class TimeWriter {
public:
  TimeWriter(HDF5::Grouplike &file) : file_(file),
      relativeTimeWriter_(file_, "relativeTime"),
      relativeTimeIncrementWriter_(file_, "relativeTimeIncrement") {}

  void write(ProgramState const &programState)  {
      addEntry(relativeTimeWriter_, programState.timeStep,
               programState.relativeTime);
      addEntry(relativeTimeIncrementWriter_, programState.timeStep,
               programState.relativeTau);
  }

private:
  HDF5::Grouplike &file_;

  HDF5::SequenceIO<0> relativeTimeWriter_;
  HDF5::SequenceIO<0> relativeTimeIncrementWriter_;
};
#endif
