#ifndef SRC_HDF5_RESTRICT_HH
#define SRC_HDF5_RESTRICT_HH

#include <cassert>

#include <dune/common/bitsetvector.hh>

#include "../../utils/tobool.hh"

template <class Vector, class Patch>
Vector restrictToSurface(const Vector& v1, const Patch& patch) {
    auto const &vertices = *patch.getVertices();
    assert(vertices.size() == v1.size());

    Vector ret(vertices.count());
    auto target = ret.begin();
    for (size_t i = 0; i < v1.size(); ++i)
      if (toBool(vertices[i]))
        *(target++) = v1[i];
    assert(target == ret.end());
    return ret;
}
#endif
