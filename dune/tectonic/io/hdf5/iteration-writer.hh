#ifndef SRC_HDF5_ITERATION_WRITER_HH
#define SRC_HDF5_ITERATION_WRITER_HH


#include "../../time-stepping/adaptivetimestepper.hh"
#include <dune/fufem/hdf5/file.hh>
#include <dune/fufem/hdf5/sequenceio.hh>

class IterationWriter {
public:
  IterationWriter(HDF5::Grouplike& file)
      : group_(file, "iterations"),
        fpiSubGroup_(group_, "fixedPoint"),
        mgSubGroup_(group_, "multiGrid"),
        finalMGIterationWriter_(mgSubGroup_, "final"),
        finalFPIIterationWriter_(fpiSubGroup_, "final"),
        totalMGIterationWriter_(mgSubGroup_, "total"),
        totalFPIIterationWriter_(fpiSubGroup_, "total") {}

  void write(size_t timeStep, const IterationRegister& iterationCount) {
    addEntry(finalMGIterationWriter_, timeStep,
             iterationCount.finalCount.multigridIterations);
    addEntry(finalFPIIterationWriter_, timeStep,
             iterationCount.finalCount.iterations);
    addEntry(totalMGIterationWriter_, timeStep,
             iterationCount.totalCount.multigridIterations);
    addEntry(totalFPIIterationWriter_, timeStep,
             iterationCount.totalCount.iterations);
  }

private:
  HDF5::Group group_;
  HDF5::Group fpiSubGroup_;
  HDF5::Group mgSubGroup_;

  HDF5::SequenceIO<0, size_t> finalMGIterationWriter_;
  HDF5::SequenceIO<0, size_t> finalFPIIterationWriter_;
  HDF5::SequenceIO<0, size_t> totalMGIterationWriter_;
  HDF5::SequenceIO<0, size_t> totalFPIIterationWriter_;
};

#endif
