#ifndef SRC_HDF_RESTART_BODY_HDF_HH
#define SRC_HDF_RESTART_BODY_HDF_HH

#include <vector>

#include <dune/fufem/hdf5/file.hh>
#include <dune/fufem/hdf5/sequenceio.hh>

template <class ProgramState> class RestartBodyIO {
private:
  const size_t id_;

  HDF5::SequenceIO<2> displacementWriter_;
  HDF5::SequenceIO<2> velocityWriter_;
  HDF5::SequenceIO<2> accelerationWriter_;
  HDF5::SequenceIO<1> stateWriter_;
  HDF5::SequenceIO<1> weightedNormalStressWriter_;

public:
RestartBodyIO(HDF5::Grouplike &group, const size_t vertexCount, const size_t id)
    : id_(id),
      displacementWriter_(group, "displacement"+std::to_string(id_), vertexCount,
                          ProgramState::Vector::block_type::dimension),
      velocityWriter_(group, "velocity"+std::to_string(id_), vertexCount,
                      ProgramState::Vector::block_type::dimension),
      accelerationWriter_(group, "acceleration"+std::to_string(id_), vertexCount,
                          ProgramState::Vector::block_type::dimension),
      stateWriter_(group, "state"+std::to_string(id_), vertexCount),
      weightedNormalStressWriter_(group, "weightedNormalStress"+std::to_string(id_), vertexCount) {}


void write(const ProgramState& programState) {
  addEntry(displacementWriter_, programState.timeStep, programState.u[id_]);
  addEntry(velocityWriter_, programState.timeStep, programState.v[id_]);
  addEntry(accelerationWriter_, programState.timeStep, programState.a[id_]);
  addEntry(stateWriter_, programState.timeStep, programState.alpha[id_]);
  addEntry(weightedNormalStressWriter_, programState.timeStep,
           programState.weightedNormalStress[id_]);
}


void read(size_t timeStep, ProgramState& programState) {
  readEntry(displacementWriter_, timeStep, programState.u[id_]);
  readEntry(velocityWriter_, timeStep, programState.v[id_]);
  readEntry(accelerationWriter_, timeStep, programState.a[id_]);
  readEntry(stateWriter_, timeStep, programState.alpha[id_]);
  readEntry(weightedNormalStressWriter_, timeStep,
            programState.weightedNormalStress[id_]);
}

};
#endif
