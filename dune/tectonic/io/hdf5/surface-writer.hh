#ifndef SRC_HDF5_SURFACE_WRITER_HH
#define SRC_HDF5_SURFACE_WRITER_HH

#include <dune/fufem/boundarypatch.hh>
#include "restrict.hh"

#include <dune/fufem/hdf5/file.hh>
#include <dune/fufem/hdf5/sequenceio.hh>
#include <dune/fufem/hdf5/singletonwriter.hh>

template <class ProgramState, class GridView> class SurfaceWriter {
  using Vector = typename ProgramState::Vector;
  using Patch = BoundaryPatch<GridView>;

public:
  SurfaceWriter(HDF5::Grouplike &file, Vector const &vertexCoordinates,
                Patch const &surface, size_t const id) :
      id_(id),
      group_(file, "surface"+std::to_string(id_)),
      surface_(surface),
      surfaceDisplacementWriter_(group_, "displacement", surface.numVertices(),
                                 Vector::block_type::dimension),
      surfaceVelocityWriter_(group_, "velocity", surface.numVertices(),
                             Vector::block_type::dimension) {

  auto const surfaceCoordinates = restrictToSurface(vertexCoordinates, surface);
  HDF5::SingletonWriter<2> surfaceCoordinateWriter(group_, "coordinates",
                                                 surfaceCoordinates.size(),
                                                 Vector::block_type::dimension);
  setEntry(surfaceCoordinateWriter, surfaceCoordinates);
}

  void write(ProgramState const &programState) {

      auto const surfaceDisplacements = restrictToSurface(programState.u[id_], surface_);
      addEntry(surfaceDisplacementWriter_, programState.timeStep,
               surfaceDisplacements);

      auto const surfaceVelocities = restrictToSurface(programState.v[id_], surface_);
      addEntry(surfaceVelocityWriter_, programState.timeStep, surfaceVelocities);
    }

private:
  size_t const id_;

  HDF5::Group group_;

  Patch const &surface_;

  HDF5::SequenceIO<2> surfaceDisplacementWriter_;
  HDF5::SequenceIO<2> surfaceVelocityWriter_;
};
#endif
