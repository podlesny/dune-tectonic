#include <future>
#include <utility>

/*
 * Class that encapsulates promise and future object,
 * provides API to set exit signal for thread
 */
class Task {
protected:
    std::promise<void> exitSignal;
    std::future<void> futureObj;

public:
    Task() :
        futureObj(exitSignal.get_future())
    {}

    Task(Task&& obj) :
        exitSignal(std::move(obj.exitSignal)), futureObj(std::move(obj.futureObj))
    {}

    Task& operator=(Task&& obj) {
        exitSignal = std::move(obj.exitSignal);
        futureObj = std::move(obj.futureObj);
        return *this;
    }

    virtual void run_task() = 0;

    // thread function to be executed by thread
    void operator()() {
        run_task();
    }

    bool stopRequested() {
        // checks if value in future object is available
        if (futureObj.wait_for(std::chrono::milliseconds(0)) == std::future_status::timeout)
            return false;
        return true;
    }

    // stop thread by setting value in promise object
    void stop() {
        exitSignal.set_value();
    }
};



