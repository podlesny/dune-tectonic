#ifndef SRC_EXPLICITGRID_HH
#define SRC_EXPLICITGRID_HH

#include "gridselector.hh"
#include "explicitvectors.hh"

#include <dune/contact/common/deformedcontinuacomplex.hh>

using LeafGridView = Grid::LeafGridView;
using LevelGridView = Grid::LevelGridView;

using DeformedGridComplex = typename Dune::Contact::DeformedContinuaComplex<Grid, Vector>;
using DeformedGrid = DeformedGridComplex::DeformedGridType;

using DefLeafGridView = DeformedGrid::LeafGridView;
using DefLevelGridView = DeformedGrid::LevelGridView;

#endif
