#include "../explicitvectors.hh"
#include "../explicitgrid.hh"

#include <dune/common/promotiontraits.hh>
#include <dune/contact/assemblers/dualmortarcoupling.hh>

#include "../data-structures/friction/frictioncouplingpair.hh"
#include "../spatial-solving/contact/dualmortarcoupling.hh"

using field_type = typename Dune::PromotionTraits<typename Vector::field_type,
                                            typename DeformedGrid::ctype>::PromotedType;

using MyContactCoupling = DualMortarCoupling<field_type, DeformedGrid>;
using MyFrictionCouplingPair = FrictionCouplingPair<DeformedGrid, LocalVector, field_type>;

template std::shared_ptr<StateUpdater<ScalarVector, Vector>>
initStateUpdater<ScalarVector, Vector, MyContactCoupling, MyFrictionCouplingPair>(
    Config::stateModel model,
    const std::vector<ScalarVector>& alpha_initial,
    const std::vector<std::shared_ptr<MyContactCoupling>>& contactCouplings,
    const std::vector<std::shared_ptr<MyFrictionCouplingPair>>& couplings);
