#ifndef SRC_TIME_STEPPING_ADAPTIVETIMESTEPPER_HH
#define SRC_TIME_STEPPING_ADAPTIVETIMESTEPPER_HH

#include <fstream>
#include <future>
#include <thread>

//#include "../multi-threading/task.hh"
#include "../spatial-solving/contact/nbodycontacttransfer.hh"

#include "coupledtimestepper.hh"
#include "timestepper.hh"
#include "stepbase.hh"



template <class Factory, class ContactNetwork, class Updaters, class ErrorNorms>
class AdaptiveTimeStepper : public TimeStepper<Updaters> {
  using Base = TimeStepper<Updaters>;
  using UpdatersWithCount = typename Base::UpdatersWithCount;

  using StepBase = StepBase<Factory, ContactNetwork, Updaters, ErrorNorms>;

  using NBodyAssembler = typename ContactNetwork::NBodyAssembler;
  using Vector = typename Factory::Vector;
  using Matrix = typename Factory::Matrix;

  using IgnoreVector = typename Factory::BitVector;

  using MyCoupledTimeStepper = CoupledTimeStepper<Factory, NBodyAssembler, Updaters, ErrorNorms>;

  using GlobalFriction = typename MyCoupledTimeStepper::GlobalFriction;
  using BitVector = typename MyCoupledTimeStepper::BitVector;
  using ExternalForces = typename MyCoupledTimeStepper::ExternalForces;

public:
  AdaptiveTimeStepper(const StepBase& stepBase,
                      ContactNetwork& contactNetwork,
                      Updaters &current,
                      double relativeTime,
                      double relativeTau,
                      double minTau,
                      std::function<bool(Updaters &, Updaters &)> mustRefine);

  bool reachedEnd() override;
  IterationRegister advance() override;

  const double minTau_;

private:
  void setDeformation(const Updaters& updaters);

  NBodyAssembler step(const NBodyAssembler& oldNBodyAssembler) const;

  int refine(UpdatersWithCount& R2);

  int coarsen();

  int determineStrategy(UpdatersWithCount& R2);

  const StepBase& stepBase_;
  ContactNetwork& contactNetwork_;

  UpdatersWithCount R1_;

  std::function<bool(Updaters &, Updaters &)> mustRefine_;
};

#endif
