#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "rate.hh"
#include "rate/backward_euler.hh"
#include "rate/newmark.hh"

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
auto initRateUpdater(Config::scheme scheme,
                const BoundaryFunctions& velocityDirichletFunctions,
                const BoundaryNodes& velocityDirichletNodes,
                const Matrices<Matrix,2>& matrices,
                const std::vector<Vector>& u_initial,
                const std::vector<Vector>& v_initial,
                const std::vector<Vector>& a_initial)
-> std::shared_ptr<RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>> {

  switch (scheme) {
    case Config::Newmark:
      return std::make_shared<Newmark<Vector, Matrix, BoundaryFunctions, BoundaryNodes>>(
          matrices, u_initial, v_initial, a_initial, velocityDirichletNodes,
          velocityDirichletFunctions);
    case Config::BackwardEuler:
      return std::make_shared<
          BackwardEuler<Vector, Matrix, BoundaryFunctions, BoundaryNodes>>(
          matrices, u_initial, v_initial, a_initial, velocityDirichletNodes,
          velocityDirichletFunctions);
    default:
      assert(false);
  }
}

#include "rate_tmpl.cc"
