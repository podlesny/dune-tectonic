#ifndef SRC_TIME_STEPPING_STATE_HH
#define SRC_TIME_STEPPING_STATE_HH

#include <memory>

#include "../data-structures/enums.hh"
#include "state/ageinglawstateupdater.hh"
#include "state/sliplawstateupdater.hh"
#include "state/stateupdater.hh"

template <class ScalarVector, class Vector, class ContactCoupling, class FrictionCouplingPair>
auto initStateUpdater(
        Config::stateModel model,
        const std::vector<ScalarVector>& alpha_initial,
        const std::vector<std::shared_ptr<ContactCoupling>>& contactCouplings, // contains nonmortarBoundary
        const std::vector<std::shared_ptr<FrictionCouplingPair>>& couplings) // contains frictionInfo
-> std::shared_ptr<StateUpdater<ScalarVector, Vector>>;
#endif
