#ifndef SRC_TIME_STEPPING_UNIFORMTIMESTEPPER_HH
#define SRC_TIME_STEPPING_UNIFORMTIMESTEPPER_HH

#include "timestepper.hh"
#include "stepbase.hh"

template <class Factory, class ContactNetwork, class Updaters, class ErrorNorms>
class UniformTimeStepper : public TimeStepper<Updaters> {
  using Base = TimeStepper<Updaters>;

  using UpdatersWithCount = typename Base::UpdatersWithCount;
  using StepBase = StepBase<Factory, ContactNetwork, Updaters, ErrorNorms>;

public:
  UniformTimeStepper(const StepBase& stepBase,
                      ContactNetwork& contactNetwork,
                      Updaters &current,
                      double relativeTime,
                      double relativeTau);

  bool reachedEnd() override;
  IterationRegister advance() override;

private:
  const StepBase& stepBase_;
  ContactNetwork& contactNetwork_;
};

#endif
