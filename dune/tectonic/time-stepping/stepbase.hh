#ifndef DUNE_TECTONIC_TIME_STEPPING_STEPBASE_HH
#define DUNE_TECTONIC_TIME_STEPPING_STEPBASE_HH

#include "coupledtimestepper.hh"

template <class Factory, class ContactNetwork, class Updaters, class ErrorNorms>
class StepBase {
protected:
  using NBodyAssembler = typename ContactNetwork::NBodyAssembler;
  using IgnoreVector = typename Factory::BitVector;

  using MyCoupledTimeStepper = CoupledTimeStepper<Factory, NBodyAssembler, Updaters, ErrorNorms>;

  using GlobalFriction = typename MyCoupledTimeStepper::GlobalFriction;
  using BitVector = typename MyCoupledTimeStepper::BitVector;
  using ExternalForces = typename MyCoupledTimeStepper::ExternalForces;

public:
  StepBase(
    Dune::ParameterTree const &parset,
    ContactNetwork& contactNetwork,
    const IgnoreVector& ignoreNodes,
    GlobalFriction& globalFriction,
    const std::vector<const BitVector*>& bodywiseNonmortarBoundaries,
    ExternalForces& externalForces,
    const ErrorNorms& errorNorms) :
        parset_(parset),
        finalTime_(parset_.get<double>("problem.finalTime")),
        contactNetwork_(contactNetwork),
        ignoreNodes_(ignoreNodes),
        globalFriction_(globalFriction),
        bodywiseNonmortarBoundaries_(bodywiseNonmortarBoundaries),
        externalForces_(externalForces),
        errorNorms_(errorNorms) {}

  Dune::ParameterTree const &parset_;
  double finalTime_;

  ContactNetwork& contactNetwork_;
  const IgnoreVector& ignoreNodes_;

  GlobalFriction& globalFriction_;
  const std::vector<const BitVector*>& bodywiseNonmortarBoundaries_;

  ExternalForces& externalForces_;
  const ErrorNorms& errorNorms_;
};

#endif
