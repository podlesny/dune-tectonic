#ifndef SRC_TIME_STEPPING_UPDATERS_HH
#define SRC_TIME_STEPPING_UPDATERS_HH

template <class RateUpdaterTEMPLATE, class StateUpdaterTEMPLATE>
struct Updaters {
  using RateUpdater = RateUpdaterTEMPLATE;
  using StateUpdater = StateUpdaterTEMPLATE;

  Updaters() {}

  Updaters(std::shared_ptr<RateUpdaterTEMPLATE> rateUpdater,
           std::shared_ptr<StateUpdaterTEMPLATE> stateUpdater)
      : rate_(rateUpdater), state_(stateUpdater) {}

  bool operator==(Updaters const &other) const {
    return other.rate_ == rate_ and other.state_ == state_;
  }

  Updaters<RateUpdater, StateUpdater> clone() const {
    return Updaters<RateUpdater, StateUpdater>(rate_->clone(), state_->clone());
  }

  std::shared_ptr<RateUpdaterTEMPLATE> rate_;
  std::shared_ptr<StateUpdaterTEMPLATE> state_;
};

#endif
