#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "rateupdater.hh"

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>::RateUpdater(
    const Matrices<Matrix,2>& _matrices, const std::vector<Vector>& _u_initial,
    const std::vector<Vector>& _v_initial, const std::vector<Vector>& _a_initial,
    const BoundaryNodes& _dirichletNodes,
    const BoundaryFunctions& _dirichletFunctions)
    : matrices(_matrices),
      u(_u_initial),
      v(_v_initial),
      a(_a_initial),
      dirichletNodes(_dirichletNodes),
      dirichletFunctions(_dirichletFunctions) {}

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
void RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>::nextTimeStep() {
    u_o = u;
    v_o = v;
    a_o = a;
    postProcessCalled = false;
}

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
void RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>::extractDisplacement(std::vector<Vector>& displacements) const {
    if (!postProcessCalled)
        DUNE_THROW(Dune::Exception, "It seems you forgot to call postProcess!");

    displacements = u;
}

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
void RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>::extractOldDisplacement(std::vector<Vector>& displacements) const {
    displacements = u_o;
}

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
void RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>::extractVelocity(std::vector<Vector>& velocity) const {
    if (!postProcessCalled)
        DUNE_THROW(Dune::Exception, "It seems you forgot to call postProcess!");

    velocity = v;
}

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
void RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>::extractOldVelocity(std::vector<Vector>& oldVelocity) const {
    oldVelocity = v_o;
}

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
void RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>::extractAcceleration(std::vector<Vector>& acceleration) const {
    if (!postProcessCalled)
        DUNE_THROW(Dune::Exception, "It seems you forgot to call postProcess!");

    acceleration = a;
}

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
const Matrices<Matrix,2>& RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>::getMatrices() const {
    return matrices;
}


#include "backward_euler.cc"
#include "newmark.cc"
#include "rateupdater_tmpl.cc"
