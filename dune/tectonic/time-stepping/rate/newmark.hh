#ifndef SRC_TIME_STEPPING_RATE_NEWMARK_HH
#define SRC_TIME_STEPPING_RATE_NEWMARK_HH

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
class Newmark : public RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes> {
public:
    Newmark(
            const Matrices<Matrix,2>& _matrices,
            const std::vector<Vector>& _u_initial,
            const std::vector<Vector>& _v_initial,
            const std::vector<Vector>& _a_initial,
            const BoundaryNodes& _dirichletNodes,
            const BoundaryFunctions& _dirichletFunctions);

    virtual void setup(
            const std::vector<Vector>&,
            double,
            double,
            std::vector<Vector>&,
            std::vector<Vector>&,
            std::vector<Matrix>&) override;

    virtual void velocityObstacles(const Vector& u0, const Vector& uObstacles, const Vector& v0, Vector& v1Obstacles) override;

    virtual void postProcess(const std::vector<Vector>&) override;

    virtual auto clone() const -> std::shared_ptr<RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>> override;
};
#endif
