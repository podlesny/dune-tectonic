#ifndef SRC_TIME_STEPPING_RATE_RATEUPDATER_HH
#define SRC_TIME_STEPPING_RATE_RATEUPDATER_HH

#include <memory>

#include <dune/common/bitsetvector.hh>

#include "../../data-structures/matrices.hh"

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
class RateUpdater {
protected:
    RateUpdater(
            const Matrices<Matrix,2>& _matrices,
            const std::vector<Vector>& _u_initial,
            const std::vector<Vector>& _v_initial,
            const std::vector<Vector>& _a_initial,
            const BoundaryNodes& _dirichletNodes,
            const BoundaryFunctions& _dirichletFunctions);

public:
    void nextTimeStep();
    void virtual setup(
            const std::vector<Vector>& ell,
            double _tau,
            double relativeTime,
            std::vector<Vector>& rhs,
            std::vector<Vector>& iterate,
            std::vector<Matrix>& AB) = 0;

  void virtual postProcess(const std::vector<Vector>& iterate) = 0;
  void virtual velocityObstacles(const Vector& u0, const Vector& uObstacles, const Vector& v0, Vector& v1Obstacles) = 0;
  void extractDisplacement(std::vector<Vector>& displacements) const;
  void extractOldDisplacement(std::vector<Vector>& displacements) const;
  void extractVelocity(std::vector<Vector>& velocity) const;
  void extractOldVelocity(std::vector<Vector>& velocity) const;
  void extractAcceleration(std::vector<Vector>& acceleration) const;

  const Matrices<Matrix,2>& getMatrices() const;

  std::shared_ptr<RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>> virtual clone() const = 0;

protected:
  const Matrices<Matrix,2>& matrices;
  std::vector<Vector> u, v, a;
  const BoundaryNodes& dirichletNodes;
  const BoundaryFunctions& dirichletFunctions;

  std::vector<Vector> u_o, v_o, a_o;
  double tau;

  bool postProcessCalled = true;
};
#endif
