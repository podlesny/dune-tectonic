#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"
#include "../../explicitvectors.hh"

#include "../../data-structures/network/contactnetwork.hh"

using MyContactNetwork = ContactNetwork<Grid, Vector>;

using BoundaryNodes = typename MyContactNetwork::BoundaryNodes;
using BoundaryFunctions = typename MyContactNetwork::BoundaryFunctions;

template class RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>;
template class Newmark<Vector, Matrix, BoundaryFunctions, BoundaryNodes>;
template class BackwardEuler<Vector, Matrix, BoundaryFunctions, BoundaryNodes>;
