#ifndef SRC_TIME_STEPPING_RATE_BACKWARD_EULER_HH
#define SRC_TIME_STEPPING_RATE_BACKWARD_EULER_HH

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
class BackwardEuler : public RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes> {
public:
    BackwardEuler(
            const Matrices<Matrix,2> &_matrices,
            const std::vector<Vector> &_u_initial,
            const std::vector<Vector> &_v_initial,
            const std::vector<Vector> &_a_initial,
            const BoundaryNodes& _dirichletNodes,
            const BoundaryFunctions& _dirichletFunctions);

    virtual void setup(
            const std::vector<Vector>&,
            double,
            double,
            std::vector<Vector>&,
            std::vector<Vector>&,
            std::vector<Matrix>&) override;

    virtual void velocityObstacles(const Vector&, const Vector&, const Vector&, Vector&) override;

    virtual void postProcess(const std::vector<Vector>&) override;

    virtual auto clone() const -> std::shared_ptr<RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>> override;
};
#endif
