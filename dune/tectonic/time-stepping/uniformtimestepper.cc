#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/iterationsteps/cgstep.hh>
#include <dune/solvers/solvers/loopsolver.hh>

#include "../spatial-solving/preconditioners/multilevelpatchpreconditioner.hh"

#include <dune/tectonic/utils/reductionfactors.hh>

#include "uniformtimestepper.hh"
#include "step.hh"


template <class Factory, class ContactNetwork, class Updaters, class ErrorNorms>
UniformTimeStepper<Factory, ContactNetwork, Updaters, ErrorNorms>::UniformTimeStepper(
        const StepBase& stepBase,
        ContactNetwork& contactNetwork,
        Updaters &current,
        double relativeTime,
        double relativeTau)
    : Base(current, relativeTime, relativeTau),
      stepBase_(stepBase),
      contactNetwork_(contactNetwork)
      {}

template <class Factory, class ContactNetwork, class Updaters, class ErrorNorms>
bool UniformTimeStepper<Factory, ContactNetwork, Updaters, ErrorNorms>::reachedEnd() {
  return this->relativeTime_ >= 1.0;
}

template <class Factory, class ContactNetwork, class Updaters, class ErrorNorms>
IterationRegister UniformTimeStepper<Factory, ContactNetwork, Updaters, ErrorNorms>::advance() {
  //std::cout << "AdaptiveTimeStepper::advance()" << std::endl;

  using Step = Step<Factory, ContactNetwork, Updaters, ErrorNorms>;
  this->iterationRegister_.reset();

  UpdatersWithCount N;

  auto step = Step(stepBase_, this->current_, contactNetwork_.nBodyAssembler(), this->relativeTime_, this->relativeTau_, this->iterationRegister_);
  step.run(Step::Mode::sameThread);
  N = step.get();

  this->current_ = N.updaters;
  this->iterationRegister_.registerFinalCount(N.count);
  this->relativeTime_ += this->relativeTau_;

  return this->iterationRegister_;
}

#include "uniformtimestepper_tmpl.cc"
