#ifndef SRC_TIME_STEPPING_TIMESTEPPER_HH
#define SRC_TIME_STEPPING_TIMESTEPPER_HH

#include "../spatial-solving/fixedpointiterator.hh"

struct IterationRegister {
    void registerCount(FixedPointIterationCounter count) {
        totalCount += count;
    }
    void registerFinalCount(FixedPointIterationCounter count) {
        finalCount = count;
    }

    void reset() {
        totalCount = FixedPointIterationCounter();
        finalCount = FixedPointIterationCounter();
    }

    FixedPointIterationCounter totalCount;
    FixedPointIterationCounter finalCount;
};

template <class Updaters>
struct UpdatersWithCount {
  Updaters updaters;
  FixedPointIterationCounter count;
};

template <class Updaters>
class TimeStepper {
public:
  using UpdatersWithCount = UpdatersWithCount<Updaters>;

  TimeStepper(Updaters &current, double relativeTime, double relativeTau) :
      current_(current),
      relativeTime_(relativeTime),
      relativeTau_(relativeTau)
  {}

  virtual bool reachedEnd() = 0;
  virtual IterationRegister advance() = 0;

  double relativeTime_;
  double relativeTau_;

protected:
  Updaters &current_;
  IterationRegister iterationRegister_;
};

#endif
