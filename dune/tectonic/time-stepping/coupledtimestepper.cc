#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "coupledtimestepper.hh"

#include "../utils/debugutils.hh"

template <class Factory, class NBodyAssembler, class Updaters, class ErrorNorms>
CoupledTimeStepper<Factory, NBodyAssembler, Updaters, ErrorNorms>::CoupledTimeStepper(
    double finalTime, Dune::ParameterTree const &parset,
    const NBodyAssembler& nBodyAssembler,
    const IgnoreVector& ignoreNodes,
    GlobalFriction& globalFriction,
    const std::vector<const BitVector*>& bodywiseNonmortarBoundaries,
    Updaters updaters,
    const ErrorNorms& errorNorms,
    ExternalForces& externalForces)
    : finalTime_(finalTime),
      parset_(parset),
      nBodyAssembler_(nBodyAssembler),
      ignoreNodes_(ignoreNodes),
      globalFriction_(globalFriction),
      bodywiseNonmortarBoundaries_(bodywiseNonmortarBoundaries),
      updaters_(updaters),
      externalForces_(externalForces),
      errorNorms_(errorNorms) {}

template <class Factory, class NBodyAssembler, class Updaters, class ErrorNorms>
template <class LinearSolver>
FixedPointIterationCounter
CoupledTimeStepper<Factory, NBodyAssembler, Updaters, ErrorNorms>::step(std::shared_ptr<LinearSolver>& linearSolver, double relativeTime,
                                                       double relativeTau) {

  //std::cout << "CoupledTimeStepper::step()" << std::endl;

  updaters_.state_->nextTimeStep();
  updaters_.rate_->nextTimeStep();

  auto const newRelativeTime = relativeTime + relativeTau;
  std::vector<Vector> ell(externalForces_.size());
  for (size_t i=0; i<externalForces_.size(); i++) {
    (*externalForces_[i])(newRelativeTime, ell[i]);
  }

  std::vector<Matrix> velocityMatrix;
  std::vector<Vector> velocityRHS;
  std::vector<Vector> velocityIterate;

  auto const tau = relativeTau * finalTime_;
  updaters_.state_->setup(tau); 
  updaters_.rate_->setup(ell, tau, newRelativeTime, velocityRHS, velocityIterate, velocityMatrix);

  std::vector<BitVector> nonmortarBoundaries;
  getNonmortarBoundaries(nonmortarBoundaries);

  updaters_.state_->setActiveNodes(nonmortarBoundaries);
  globalFriction_.setActiveNodes(nonmortarBoundaries);

/*  std::cout << "tau: " << tau << std::endl;
  print(ell, "ell: ");
  print(velocityRHS, "velocityRHS: ");
  print(velocityIterate, "velocityIterate: "); */


  FixedPointIterator fixedPointIterator(
      parset_, nBodyAssembler_, ignoreNodes_, globalFriction_, bodywiseNonmortarBoundaries_, errorNorms_);
  auto const iterations = fixedPointIterator.template run<LinearSolver>(updaters_, linearSolver,
                                                 velocityMatrix, velocityRHS, velocityIterate);
  return iterations;
}

template <class Factory, class NBodyAssembler, class Updaters, class ErrorNorms>
void CoupledTimeStepper<Factory, NBodyAssembler, Updaters, ErrorNorms>::getNonmortarBoundaries(std::vector<BitVector>& nonmortarBoundries) {
    // init nonmortarBoundries
    const auto dim = Vector::block_type::dimension;

    auto& grids = nBodyAssembler_.getGrids();
    nonmortarBoundries.resize(nBodyAssembler_.nGrids());
    for (size_t bodyIdx=0; bodyIdx<nonmortarBoundries.size(); bodyIdx++) {
      nonmortarBoundries[bodyIdx].resize(grids[bodyIdx]->size(dim));
      nonmortarBoundries[bodyIdx].unsetAll();
    }

    for (size_t i=0; i<nBodyAssembler_.nCouplings(); i++) {
        const auto& contactCoupling = nBodyAssembler_.getContactCouplings()[i]; // dual mortar object holding boundary patches
        const auto nonmortarIdx = nBodyAssembler_.getCoupling(i).gridIdx_[0];

        const auto& nmBoundaryVertices = *(contactCoupling->nonmortarBoundary().getVertices());
        /*const auto& hasObstacle = *(contactCoupling->hasObstacle());
        print(hasObstacle, "hasObstacle: ");*/

        auto& nmBoundary = nonmortarBoundries[nonmortarIdx];
        for (size_t j=0; j<nmBoundaryVertices.size(); j++) {
            if (nmBoundaryVertices[j][0]) {
                nmBoundary[j][0] = true;
            }
        }
    }
}

#include "coupledtimestepper_tmpl.cc"
