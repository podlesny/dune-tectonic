#ifndef SRC_TIME_STEPPING_COUPLEDTIMESTEPPER_HH
#define SRC_TIME_STEPPING_COUPLEDTIMESTEPPER_HH

#include <functional>
#include <memory>

#include <dune/common/parametertree.hh>

#include "../spatial-solving/fixedpointiterator.hh"

template <class Factory, class NBodyAssembler, class Updaters, class ErrorNorms>
class CoupledTimeStepper {
  using Vector = typename Factory::Vector;
  using Matrix = typename Factory::Matrix;
  using IgnoreVector = typename Factory::BitVector;
  using FixedPointIterator = FixedPointIterator<Factory, NBodyAssembler, Updaters, ErrorNorms>;

public:
  using GlobalFriction = typename FixedPointIterator::GlobalFriction;
  using BitVector = typename FixedPointIterator::BitVector;
  using ExternalForces = std::vector<std::unique_ptr<const std::function<void(double, Vector &)>>>;

public:
  CoupledTimeStepper(double finalTime,
                     Dune::ParameterTree const &parset,
                     const NBodyAssembler& nBodyAssembler,
                     const IgnoreVector& ignoreNodes,
                     GlobalFriction& globalFriction,
                     const std::vector<const BitVector*>& bodywiseNonmortarBoundaries,
                     Updaters updaters,
                     const ErrorNorms& errorNorms,
                     ExternalForces& externalForces);

  template <class LinearSolver>
  FixedPointIterationCounter step(std::shared_ptr<LinearSolver>& linearSolver, double relativeTime, double relativeTau);

private:
  void getNonmortarBoundaries(std::vector<BitVector>& nonmortarBoundries);

private:
  double finalTime_;
  Dune::ParameterTree const &parset_;
  const NBodyAssembler& nBodyAssembler_;
  const IgnoreVector& ignoreNodes_;

  GlobalFriction& globalFriction_;
  const std::vector<const BitVector*>& bodywiseNonmortarBoundaries_;

  Updaters updaters_;
  ExternalForces& externalForces_;
  const ErrorNorms& errorNorms_;
};

#endif
