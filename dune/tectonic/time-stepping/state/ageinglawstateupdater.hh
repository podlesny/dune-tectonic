#ifndef SRC_TIME_STEPPING_STATE_AGEINGLAWSTATEUPDATER_HH
#define SRC_TIME_STEPPING_STATE_AGEINGLAWSTATEUPDATER_HH

#include <dune/common/bitsetvector.hh>

#include "stateupdater.hh"

template <class ScalarVector, class Vector>
class AgeingLawStateUpdater : public LocalStateUpdater<ScalarVector, Vector> {
private:
    using BitVector = Dune::BitSetVector<1>;

public:
    AgeingLawStateUpdater(
            const ScalarVector& alpha_initial,
            const BitVector& nodes,
            const double L,
            const double V0);

  void nextTimeStep() override;
  void setup(double tau) override;
  void solve(const Vector& velocity_field) override;
  void extractAlpha(ScalarVector&) override;
  void setActiveNodes(const BitVector& active) override;

  auto clone() const -> std::shared_ptr<LocalStateUpdater<ScalarVector, Vector>> override;

private:
  std::vector<int> localToGlobal_;

  ScalarVector alpha_o_;
  ScalarVector alpha_;
  const BitVector& nodes_;
  const double L_;
  const double V0_;
  double tau_;
};
#endif
