#include <cmath>

#include "ageinglawstateupdater.hh"
#include "../../utils/tobool.hh"
#include "../../utils/debugutils.hh"

template <class ScalarVector, class Vector>
AgeingLawStateUpdater<ScalarVector, Vector>::AgeingLawStateUpdater(
        const ScalarVector& alpha_initial,
        const BitVector& nodes,
        const double L,
        const double V0) :
    nodes_(nodes),
    L_(L),
    V0_(V0) {

    localToGlobal_.resize(nodes_.count());
    size_t localIdx = 0;
    for (size_t i=0; i<nodes_.size(); i++) {
        if (not toBool(nodes_[i]))
            continue;

        localToGlobal_[localIdx] = i;
        localIdx++;
    }
    localToGlobal_.resize(localIdx);

    alpha_.resize(localToGlobal_.size());
    for (size_t i=0; i<alpha_.size(); i++) {
        alpha_[i] = alpha_initial[localToGlobal_[i]];
    }

    this->active_.resize(alpha_.size());
    this->active_.setAll();
}

template <class ScalarVector, class Vector>
void AgeingLawStateUpdater<ScalarVector, Vector>::nextTimeStep() {
  alpha_o_ = alpha_;
}

template <class ScalarVector, class Vector>
void AgeingLawStateUpdater<ScalarVector, Vector>::setup(double tau) {
  tau_ = tau;
}

/*
  Compute [ 1-\exp(c*x) ] / x under the assumption that x is
  non-negative
*/
auto liftSingularity(
        double c,
        double x) {

  if (x <= 0)
    return -c;
  else
    return -std::expm1(c * x) / x;
}

template <class ScalarVector, class Vector>
void AgeingLawStateUpdater<ScalarVector, Vector>::solve(const Vector& velocity_field) {

    for (size_t i=0; i<alpha_.size(); ++i) {
        if (this->active_[i][0]) {
            auto tangentVelocity = velocity_field[localToGlobal_[i]];
            tangentVelocity[0] = 0.0;

            double const V = tangentVelocity.two_norm();
            double const mtoL = -tau_ / L_;
            alpha_[i] = std::log(std::exp(alpha_o_[i] + V * mtoL) +
                          V0_ * liftSingularity(mtoL, V));
        } else {
            alpha_[i] = alpha_o_[i];
        }
    }
}

template <class ScalarVector, class Vector>
void AgeingLawStateUpdater<ScalarVector, Vector>::setActiveNodes(const BitVector& active) {
    for (size_t i=0; i<localToGlobal_.size(); i++) {
        this->active_[i] = active[localToGlobal_[i]];
    }
}

template <class ScalarVector, class Vector>
void AgeingLawStateUpdater<ScalarVector, Vector>::extractAlpha(
        ScalarVector& alpha) {

    //std::cout << "alpha size: " << alpha.size() << " nodes_.size() " << nodes_.size() << std::endl;
    if (alpha.size() != nodes_.size()) {
        alpha.resize(nodes_.size());
    }

    for (size_t i=0; i<localToGlobal_.size(); i++) {
        alpha[localToGlobal_[i]] = alpha_[i];
    }
}

template <class ScalarVector, class Vector>
auto AgeingLawStateUpdater<ScalarVector, Vector>::clone() const
 -> std::shared_ptr<LocalStateUpdater<ScalarVector, Vector>> {

  return std::make_shared<AgeingLawStateUpdater<ScalarVector, Vector>>(*this);
}
