#ifndef SRC_TIME_STEPPING_RATE_HH
#define SRC_TIME_STEPPING_RATE_HH

#include <memory>

#include "../data-structures/enums.hh"
#include "rate/rateupdater.hh"

template <class Vector, class Matrix, class BoundaryFunctions, class BoundaryNodes>
auto initRateUpdater(Config::scheme scheme,
                const BoundaryFunctions& velocityDirichletFunctions,
                const BoundaryNodes& velocityDirichletNodes,
                const Matrices<Matrix,2>& matrices,
                const std::vector<Vector>& u_initial,
                const std::vector<Vector>& v_initial,
                const std::vector<Vector>& a_initial)
-> std::shared_ptr<RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>>;
#endif
