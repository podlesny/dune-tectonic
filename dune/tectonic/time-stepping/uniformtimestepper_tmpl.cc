#ifndef MY_DIM
#error MY_DIM unset
#endif

#include <future>
#include <thread>

#include "../explicitgrid.hh"
#include "../explicitvectors.hh"

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>

#include "../spatial-solving/tnnmg/functional.hh"
#include "../spatial-solving/solverfactory.hh"

#include "../data-structures/network/contactnetwork.hh"
#include "../data-structures/friction/globalfriction.hh"
#include "../spatial-solving/tnnmg/zerononlinearity.hh"

#include "rate/rateupdater.hh"
#include "state/stateupdater.hh"
#include "updaters.hh"

using MyContactNetwork = ContactNetwork<Grid, Vector>;

using BoundaryNodes = typename MyContactNetwork::BoundaryNodes;
using BoundaryFunctions = typename MyContactNetwork::BoundaryFunctions;

using MyStateUpdater = StateUpdater<ScalarVector, Vector>;
using MyRateUpdater = RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>;
using MyUpdaters = Updaters<MyRateUpdater, MyStateUpdater>;

using LinearSolver = Dune::Solvers::LoopSolver<Vector>;
using MyNBodyAssembler = typename MyContactNetwork::NBodyAssembler;
using ErrorNorms = typename MyContactNetwork::StateEnergyNorms;

using MyGlobalFriction = GlobalFriction<Matrix, Vector>;
using MyFunctional = Functional<Matrix&, Vector&, MyGlobalFriction&, Vector&, Vector&, double>;
using MySolverFactory = SolverFactory<MyFunctional, BitVector>;

template class UniformTimeStepper<MySolverFactory, MyContactNetwork, MyUpdaters, ErrorNorms>;


using NoFriction = ZeroNonlinearity;
using NoFrictionFunctional = Functional<Matrix&, Vector&, NoFriction&, Vector&, Vector&, double>;
using NoFrictionSolverFactory = SolverFactory<NoFrictionFunctional, BitVector>;

template class UniformTimeStepper<NoFrictionSolverFactory, MyContactNetwork, MyUpdaters, ErrorNorms>;

/*
template std::packaged_task<typename AdaptiveTimeStepper<MySolverFactory, MyContactNetwork, MyUpdaters, ErrorNorms>::UpdatersWithCount()>
AdaptiveTimeStepper<MySolverFactory, MyContactNetwork, MyUpdaters, ErrorNorms>::step<LinearSolver>(
        const MyUpdaters&, const MyNBodyAssembler&, std::shared_ptr<LinearSolver>&, double, double); */
