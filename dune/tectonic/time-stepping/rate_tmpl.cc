#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../explicitgrid.hh"
#include "../explicitvectors.hh"
#include "../data-structures/network/contactnetwork.hh"

using MyContactNetwork = ContactNetwork<Grid, Vector>;

using BoundaryFunctions = typename MyContactNetwork::BoundaryFunctions;
using BoundaryNodes = typename MyContactNetwork::BoundaryNodes;

template
auto initRateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>(
    Config::scheme scheme,
    const BoundaryFunctions&  velocityDirichletFunctions,
    const BoundaryNodes& velocityDirichletNodes,
    const Matrices<Matrix, 2>& matrices,
    const std::vector<Vector>& u_initial,
    const std::vector<Vector>& v_initial,
    const std::vector<Vector>& a_initial)
-> std::shared_ptr<RateUpdater<Vector, Matrix, BoundaryFunctions, BoundaryNodes>>;
