#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"
#include "../../explicitvectors.hh"

template class LeafBody<Grid, Vector>;

template class Body<DefLeafGridView>;
template class Body<DefLevelGridView>;
