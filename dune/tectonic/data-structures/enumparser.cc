#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

// Copyright Carsten Graeser 2012

#include <dune/common/exceptions.hh>

#include "enumparser.hh"

template <class Enum>
typename std::enable_if<
    !std::is_base_of<Dune::NotImplemented, StringToEnum<Enum>>::value,
    std::istream &>::type
operator>>(std::istream &lhs, Enum &e) {
  std::string s;
  lhs >> s;

  try {
    e = StringToEnum<Enum>::convert(s);
  } catch (typename Dune::Exception) {
    lhs.setstate(std::ios_base::failbit);
  }
  return lhs;
}

Config::FrictionModel StringToEnum<Config::FrictionModel>::convert(
    std::string const &s) {
  if (s == "Truncated")
    return Config::Truncated;

  if (s == "Regularised")
    return Config::Regularised;

  if (s == "Tresca")
    return Config::Tresca;

  if (s == "None")
    return Config::None;

  DUNE_THROW(Dune::Exception, "failed to parse enum");
}

Config::scheme StringToEnum<Config::scheme>::convert(std::string const &s) {
  if (s == "newmark")
    return Config::Newmark;

  if (s == "backwardEuler")
    return Config::BackwardEuler;

  DUNE_THROW(Dune::Exception, "failed to parse enum");
}

Config::stateModel StringToEnum<Config::stateModel>::convert(
    std::string const &s) {
  if (s == "AgeingLaw")
    return Config::AgeingLaw;

  if (s == "SlipLaw")
    return Config::SlipLaw;

  DUNE_THROW(Dune::Exception, "failed to parse enum");
}

Config::PatchType StringToEnum<Config::PatchType>::convert(
    std::string const &s) {
  if (s == "Rectangular")
    return Config::Rectangular;

  if (s == "Trapezoidal")
    return Config::Trapezoidal;

  DUNE_THROW(Dune::Exception, "failed to parse enum");
}

Config::TimeSteppingMode StringToEnum<Config::TimeSteppingMode>::convert(
    std::string const &s) {
  if (s == "adaptive")
    return Config::Adaptive;

  if (s == "uniform")
    return Config::Uniform;

  DUNE_THROW(Dune::Exception, "failed to parse enum");
}

template std::istream &operator>>(std::istream &lhs, Config::FrictionModel &);
template std::istream &operator>>(std::istream &lhs, Config::stateModel &);
template std::istream &operator>>(std::istream &lhs, Config::scheme &);
template std::istream &operator>>(std::istream &lhs, Config::PatchType &);
template std::istream &operator>>(std::istream &lhs, Config::TimeSteppingMode &);
