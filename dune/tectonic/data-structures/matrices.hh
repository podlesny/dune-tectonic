#ifndef SRC_MATRICES_HH
#define SRC_MATRICES_HH

template <class Matrix, size_t n>
class Matrices {
public:
  std::vector<std::shared_ptr<Matrix>> elasticity;
  std::vector<std::shared_ptr<Matrix>> damping;
  std::vector<std::shared_ptr<Matrix>> mass;

  Matrices() {
    elasticity.resize(n);
    damping.resize(n);
    mass.resize(n);

      for (size_t i=0; i<n; i++) {
        elasticity[i] = std::make_shared<Matrix>();
        damping[i] = std::make_shared<Matrix>();
        mass[i] = std::make_shared<Matrix>();
      }
  }
};

template <class Matrix> 
class Matrices<Matrix, 1> {
public:
  std::shared_ptr<Matrix> elasticity;
  std::shared_ptr<Matrix> damping;
  std::shared_ptr<Matrix> mass;

  Matrices() :
    elasticity(std::make_shared<Matrix>()),
    damping(std::make_shared<Matrix>()),
    mass(std::make_shared<Matrix>())
  {}
};
#endif
