#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"
#include "../../explicitvectors.hh"

#include "../friction/frictioncouplingpair.hh"
#include "contactnetwork.hh"
#include "levelcontactnetwork.hh"

using MyContactNetwork = ContactNetwork<Grid, Vector>;

template class LevelContactNetwork<typename MyContactNetwork::GridType, typename MyContactNetwork::FrictionCouplingPair, typename MyContactNetwork::field_type>;
