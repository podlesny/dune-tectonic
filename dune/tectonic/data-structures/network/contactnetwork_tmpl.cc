#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../../explicitgrid.hh"
#include "../../explicitvectors.hh"

#include "contactnetwork.hh"

template class ContactNetwork<Grid, Vector>;
