#ifndef SRC_DATA_STRUCTURES_LEVELCONTACTNETWORK_HH
#define SRC_DATA_STRUCTURES_LEVELCONTACTNETWORK_HH

#include <dune/common/fvector.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/istl/bvector.hh>

#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/extractors/codim1extractor.hh>
#include <dune/grid-glue/merging/merger.hh>

#include <dune/contact/common/deformedcontinuacomplex.hh>
#include <dune/contact/common/couplingpair.hh>
#include <dune/contact/assemblers/nbodyassembler.hh>
//#include <dune/contact/assemblers/dualmortarcoupling.hh>
#include <dune/contact/projections/normalprojection.hh>

#include <dune/fufem/boundarypatch.hh>

#include "../../assemblers.hh"
#include "../enums.hh"
#include "../matrices.hh"

#include "../body/body.hh"
#include "../body/bodydata.hh"
#include "../friction/frictioncouplingpair.hh"
#include "../friction/globalfriction.hh"
#include "../friction/globalfrictiondata.hh"

template <class GridTypeTEMPLATE, class FrictionCouplingPair, class field_type>
class LevelContactNetwork {
public:
    using GridType = GridTypeTEMPLATE;

    enum {dim = GridType::dimension};
    enum {dimworld = GridType::dimensionworld};

    using ctype = typename GridType::ctype;

    using GridView = typename GridType::LevelGridView;
    using Body = Body<GridView>;

    using Function = Dune::VirtualFunction<double, double>;

    using BoundaryFunctions = std::vector<typename Body::BoundaryFunctions>;
    using BoundaryNodes = std::vector<typename Body::BoundaryNodes>;
    using BoundaryPatchNodes = std::vector<typename Body::BoundaryPatchNodes>;
    using BoundaryPatches = std::vector<typename Body::BoundaryPatches>;
    using BoundaryPatch = BoundaryPatch<GridView>;

    using Extractor = Dune::GridGlue::Codim1Extractor<GridView>;
    using Glue = Dune::GridGlue::GridGlue<Extractor, Extractor>;

public:
    LevelContactNetwork(int nBodies, int nCouplings, int level = 0);

    void setBodies(const std::vector<std::shared_ptr<Body>> bodies);
    void setCouplings(const std::vector<std::shared_ptr<FrictionCouplingPair>> couplings);

    void constructBody(
            const std::shared_ptr<BodyData<dimworld>>& bodyData,
            const std::shared_ptr<GridType>& grid,
            const size_t level,
            std::shared_ptr<Body>& body) const;

    void build(field_type overlap = 1e-2);

    // getter
    void totalNodes(
            const std::string& tag,
            Dune::BitSetVector<dimworld>& totalNodes) const;
    void boundaryPatches(
            const std::string& tag,
            BoundaryPatches& patches) const;
    void boundaryPatchNodes(
            const std::string& tag,
            BoundaryPatchNodes& nodes) const;
    void boundaryNodes(
            const std::string& tag,
            BoundaryNodes& nodes) const;
    void boundaryFunctions(
            const std::string& tag,
            BoundaryFunctions& functions) const;

    auto level() const -> int;

    auto nBodies() const -> size_t;
    auto nCouplings() const -> size_t;

    auto body(int i) const -> const std::shared_ptr<Body>&;

    auto coupling(int i) const -> const std::shared_ptr<FrictionCouplingPair>&;
    auto couplings() const -> const std::vector<std::shared_ptr<FrictionCouplingPair>>&;

    auto glue(int i) const -> const std::shared_ptr<Glue>&;

private:
    void prolong(const BoundaryPatch& coarsePatch, BoundaryPatch& finePatch, const size_t fineLevel);

    const int level_;

    std::vector<std::shared_ptr<Body>> bodies_;
    std::vector<std::shared_ptr<FrictionCouplingPair>> couplings_;

    std::vector<BoundaryPatch> nonmortarPatches_;
    std::vector<BoundaryPatch> mortarPatches_;
    std::shared_ptr< Dune::GridGlue::Merger<field_type, dim-1, dim-1, dim> > gridGlueBackend_;
    std::vector<std::shared_ptr<Glue>> glues_;
};
#endif
