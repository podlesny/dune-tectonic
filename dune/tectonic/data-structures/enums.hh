#ifndef SRC_ENUMS_HH
#define SRC_ENUMS_HH

struct Config {
  enum FrictionModel { Truncated, Regularised, Tresca, None };
  enum stateModel { AgeingLaw, SlipLaw };
  enum scheme { Newmark, BackwardEuler };
  enum PatchType { Rectangular, Trapezoidal };
  enum TimeSteppingMode { Adaptive, Uniform };
};

#endif
