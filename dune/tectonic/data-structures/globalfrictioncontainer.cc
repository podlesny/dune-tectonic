#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*
#include "globalfrictioncontainer.hh"

template <class BaseGlobalFriction, size_t depth>
auto GlobalFrictionContainer<BaseGlobalFriction, depth>::operator[](std::size_t i)
-> IndexObject& {
    return globalFriction_[i];
}

template <class BaseGlobalFriction, size_t depth>
auto GlobalFrictionContainer<BaseGlobalFriction, depth>::operator[](std::size_t i) const
-> const IndexObject& {
    return globalFriction_[i];
}

template <class BaseGlobalFriction, size_t depth>
auto GlobalFrictionContainer<BaseGlobalFriction, depth>::size() const
-> size_t {
    return globalFriction_.size();
}

template <class BaseGlobalFriction, size_t depth>
void GlobalFrictionContainer<BaseGlobalFriction, depth>::resize(std::list<size_t> list) {
    assert(list.size() <= depth);

    if (list.size() == 0) {
        globalFriction_.resize(0);
    } else {
        globalFriction_.resize(list.front());
        list.pop_front();

        for (size_t i=0; i<size(); i++) {
            globalFriction_[i].resize(list);
        }
    }
}

template <class BaseGlobalFriction, size_t depth>
template <class VectorContainer>
void GlobalFrictionContainer<BaseGlobalFriction, depth>::updateAlpha(const VectorContainer& newAlpha) {
    assert(newAlpha.size() == size());

    for (size_t i=0; i<size(); i++) {
        globalFriction_[i].updateAlpha(newAlpha[i]);
    }
}

template <class BaseGlobalFriction, size_t depth>
auto GlobalFrictionContainer<BaseGlobalFriction, depth>::globalFriction()
-> GlobalFriction& {
    return globalFriction_;
}

template <class BaseGlobalFriction, size_t depth>
auto GlobalFrictionContainer<BaseGlobalFriction, depth>::globalFriction() const
-> const GlobalFriction& {
    return globalFriction_;
}


template <class BaseGlobalFriction>
auto GlobalFrictionContainer<BaseGlobalFriction, 1>::operator[](std::size_t i)
-> IndexObject& {
    return globalFriction_[i];
}

template <class BaseGlobalFriction>
auto GlobalFrictionContainer<BaseGlobalFriction, 1>::operator[](std::size_t i) const
-> const IndexObject& {
    return globalFriction_[i];
}

template <class BaseGlobalFriction>
auto GlobalFrictionContainer<BaseGlobalFriction, 1>::size() const
-> size_t {
    return globalFriction_.size();
}

template <class BaseGlobalFriction>
void GlobalFrictionContainer<BaseGlobalFriction, 1>::resize(std::list<size_t> newSize) {
    if (newSize.size() > 0) {
        globalFriction_.resize(newSize.front(), nullptr);
    } else {
        globalFriction_.resize(0);
    }
}

template <class BaseGlobalFriction>
template <class Vector>
void GlobalFrictionContainer<BaseGlobalFriction, 1>::updateAlpha(const Vector& newAlpha) {
    for (size_t i=0; i<size(); i++) {
        globalFriction_[i]->updateAlpha(newAlpha);
    }
}

template <class BaseGlobalFriction>
auto GlobalFrictionContainer<BaseGlobalFriction, 1>::globalFriction()
-> GlobalFriction& {
    return globalFriction_;
}

template <class BaseGlobalFriction>
auto GlobalFrictionContainer<BaseGlobalFriction, 1>::globalFriction() const
-> const GlobalFriction& {
    return globalFriction_;
}


#include "globalfrictioncontainer_tmpl.cc"
*/
