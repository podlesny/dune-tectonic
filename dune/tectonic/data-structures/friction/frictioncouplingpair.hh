#ifndef SRC_FRICTIONCOUPLINGPAIR_HH
#define SRC_FRICTIONCOUPLINGPAIR_HH

#include <dune/fufem/geometry/convexpolyhedron.hh>
#include <dune/contact/common/couplingpair.hh>

#include "globalfrictiondata.hh"

template <class GridType, class LocalVectorType, class field_type = double>
class FrictionCouplingPair : public Dune::Contact::CouplingPair<GridType, GridType, field_type>{
private:
    static const int dim = GridType::dimensionworld;

    using Base = Dune::Contact::CouplingPair<GridType,GridType,field_type>;
    using LocalVector = LocalVectorType;

    // friction data
    std::shared_ptr<ConvexPolyhedron<LocalVector>> weakeningPatch_;
    std::shared_ptr<GlobalFrictionData<dim>> frictionData_;

public:
  void setWeakeningPatch(std::shared_ptr<ConvexPolyhedron<LocalVector>> weakeningPatch) {
      weakeningPatch_ = weakeningPatch;
  }

  void setFrictionData(std::shared_ptr<GlobalFrictionData<dim>> frictionData) {
      frictionData_ = frictionData;
  }

  const auto& weakeningPatch() const {
      return *weakeningPatch_;
  }

  const auto& frictionData() const {
      return *frictionData_;
  }
};
#endif
