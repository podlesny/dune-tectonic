#ifndef MY_DIM
#error MY_DIM unset
#endif

#define WANT_ALUGRID 0
#define WANT_UG 1

#define WANT_GRID WANT_UG

#if WANT_GRID == WANT_ALUGRID

#if !HAVE_ALUGRID
#error ALUGRID was requested but not found
#endif
#include <dune/grid/alugrid.hh>
using Grid = Dune::ALUGrid<MY_DIM, MY_DIM, Dune::simplex, Dune::nonconforming>;

#elif WANT_GRID == WANT_UG

#if !HAVE_UG
#error UG was requested but not found
#endif
#include <dune/grid/uggrid.hh>
using Grid = Dune::UGGrid<MY_DIM>;

#else

#error requested a grid that does not exist

#endif
