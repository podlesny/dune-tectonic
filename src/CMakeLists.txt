add_subdirectory("foam")
add_subdirectory("multi-body-problem")
add_subdirectory("strikeslip")

set(UGW_SOURCE_FILES
  ../dune/tectonic/assemblers.cc # FIXME
  #../dune/tectonic/io/uniform-grid-writer.cc
  ../dune/tectonic/io/vtk.cc
  ../dune/tectonic/problem-data/grid/mygrids.cc
)

foreach(_dim 2 3)
  #set(_ugw_target uniform-grid-writer-${_dim}D)

  #add_executable(${_ugw_target} ${UGW_SOURCE_FILES})
  
  #add_dune_ug_flags(${_ugw_target})

  #set_property(TARGET ${_ugw_target} APPEND PROPERTY COMPILE_DEFINITIONS "MY_DIM=${_dim}")
endforeach()
