add_custom_target(tectonic_src_strikeslip SOURCES
  strikeslip.cfg
  strikeslip-2D.cfg
) 

set(STRIKESLIP_SOURCE_FILES
  ../../dune/tectonic/assemblers.cc
  ../../dune/tectonic/data-structures/body/body.cc
  ../../dune/tectonic/data-structures/network/levelcontactnetwork.cc
  ../../dune/tectonic/data-structures/network/contactnetwork.cc
  ../../dune/tectonic/data-structures/enumparser.cc
  ../../dune/tectonic/factories/strikeslipfactory.cc
  #../../dune/tectonic/io/vtk.cc
  #../../dune/tectonic/io/hdf5/frictionalboundary-writer.cc
  #../../dune/tectonic/io/hdf5/iteration-writer.cc
  #../../dune/tectonic/io/hdf5/patchinfo-writer.cc
  #../../dune/tectonic/io/hdf5/restart-io.cc
  #../../dune/tectonic/io/hdf5/surface-writer.cc
  #../../dune/tectonic/io/hdf5/time-writer.cc
  ../../dune/tectonic/problem-data/grid/simplexmanager.cc
  ../../dune/tectonic/problem-data/grid/trianglegeometry.cc
  ../../dune/tectonic/problem-data/grid/trianglegrids.cc
  ../../dune/tectonic/spatial-solving/solverfactory.cc
  ../../dune/tectonic/spatial-solving/fixedpointiterator.cc
  ../../dune/tectonic/time-stepping/coupledtimestepper.cc
  ../../dune/tectonic/time-stepping/adaptivetimestepper.cc
  ../../dune/tectonic/time-stepping/rate.cc
  ../../dune/tectonic/time-stepping/rate/rateupdater.cc
  ../../dune/tectonic/time-stepping/state.cc
  ../../dune/tectonic/time-stepping/uniformtimestepper.cc
  strikeslip.cc
)

foreach(_dim 2 3)
  set(_strikeslip_target strikeslip-${_dim}D)

  add_executable(${_strikeslip_target} ${STRIKESLIP_SOURCE_FILES})

  add_dune_ug_flags(${_strikeslip_target})
  add_dune_hdf5_flags(${_strikeslip_target})

  set_property(TARGET ${_strikeslip_target} APPEND PROPERTY COMPILE_DEFINITIONS "MY_DIM=${_dim}")
  #set_property(TARGET ${_strikeslip_target} APPEND PROPERTY COMPILE_DEFINITIONS "NEW_TNNMG_COMPUTE_ITERATES_DIRECTLY=1")
endforeach()
