\def\fname{generated/3d-velocity-contours:rtol=1e-5_diam=1e-2}
\pgfplotstableread[col sep=comma]{\fname:times.csv}\myloadedtable

\pgfplotsarraynew\contourlevels{%
  1\\2\\3\\5\\%
  10\\20\\30\\50\\%
  100\\200\\300\\500\\%
  1000\\
}

\begin{tikzpicture}[trim axis group left, trim axis group right]
  \begin{groupplot}[
    group style={
      y descriptions at = edge left,
      group size=6 by 1,
      horizontal sep=0cm
    },
    ymin=-0.30, ymax= 0.30,
    enlarge x limits=false,
    colormap/jet,
    y tick label style={
      /pgf/number format/.cd,
      fixed, fixed zerofill, precision=2,
      /tikz/.cd
    },
    ytick = {-0.30,-0.20,-0.10,0.00,0.10,0.20,0.30},
    tick label style={font=\footnotesize},
    label style={font=\small},
    width = 3.5cm, height = 6cm,
    enlargelimits=false,
    contour/labels=false,
    %
    groupplot xlabel={distance from trench [\si{\meter}]},
    ]

    \nextgroupplot[semithick, xlabel = {
      \pgfplotstablegetelem{0}{times}\of\myloadedtable%
      $t_0 \approx
      \SI[round-mode=places,round-precision=0]{\pgfplotsretval}{\second}$
    },
    ylabel = width, y unit = m]
    \draw[color=gray!20] (0.162533,-0.30) -- (0.162533,+0.30);           % X
    \draw[color=gray!20] (0.362533+0.05,-0.30) -- (0.362533-0.05,+0.30); % Y
    \pgfplotsinvokeforeach{0,...,12} {
      \pgfplotscolormapaccess[0:14]{#1}{jet}
      \def\TEMP{\definecolor{mycolor#1}{rgb}}
      \expandafter\TEMP\expandafter{\pgfmathresult}

      \pgfplotstablegetelem{0}{timeSteps}\of\myloadedtable
      \edef\fnameX{\fname:%
        step:\pgfplotsretval:%
        level:\pgfplotsarrayvalueofelem#1\of\contourlevels.tex}
      \addplot[contour prepared={draw color=mycolor#1}] table \fnameX;
    };

    \nextgroupplot[semithick, xlabel = {
      \pgfplotstablegetelem{1}{timeOffsets}\of\myloadedtable%
      $t_0 +
      \SI[round-mode=places,round-precision=2]{\pgfplotsretval}{\second}$
    }]
    \draw[color=gray!20] (0.162533,-0.30) -- (0.162533,+0.30);           % X
    \draw[color=gray!20] (0.362533+0.05,-0.30) -- (0.362533-0.05,+0.30); % Y
    \pgfplotsinvokeforeach{0,...,12} { % level 13 and 14 are empty
      \pgfplotscolormapaccess[0:14]{#1}{jet}
      \def\TEMP{\definecolor{mycolor#1}{rgb}}
      \expandafter\TEMP\expandafter{\pgfmathresult}
      \pgfplotstablegetelem{1}{timeSteps}\of\myloadedtable
      \edef\fnameX{\fname:%
        step:\pgfplotsretval:%
        level:\pgfplotsarrayvalueofelem#1\of\contourlevels.tex}
      \addplot[contour prepared={draw color=mycolor#1}] table \fnameX;
    };

    \nextgroupplot[semithick, xlabel = {
      \pgfplotstablegetelem{2}{timeOffsets}\of\myloadedtable%
      $t_0 +
      \SI[round-mode=places,round-precision=2]{\pgfplotsretval}{\second}$
    }]
    \draw[color=gray!20] (0.162533,-0.30) -- (0.162533,+0.30);           % X
    \draw[color=gray!20] (0.362533+0.05,-0.30) -- (0.362533-0.05,+0.30); % Y
    \pgfplotsinvokeforeach{0,...,12} { % level 13 and 14 are empty
      \pgfplotscolormapaccess[0:14]{#1}{jet}
      \def\TEMP{\definecolor{mycolor#1}{rgb}}
      \expandafter\TEMP\expandafter{\pgfmathresult}
      \pgfplotstablegetelem{2}{timeSteps}\of\myloadedtable
      \edef\fnameX{\fname:%
        step:\pgfplotsretval:%
        level:\pgfplotsarrayvalueofelem#1\of\contourlevels.tex}
      \addplot[contour prepared={draw color=mycolor#1}] table \fnameX;
    };

    \nextgroupplot[semithick, xlabel = {
      \pgfplotstablegetelem{3}{timeOffsets}\of\myloadedtable%
      $t_0 +
      \SI[round-mode=places,round-precision=2]{\pgfplotsretval}{\second}$
    },
    legend columns=4,
    legend cell align=right,
    legend style={font=\small,
                  at={(0,1.05)},
                  anchor=south,
                  fill=none},
    ]
    \draw[color=gray!20] (0.162533,-0.30) -- (0.162533,+0.30);           % X
    \draw[color=gray!20] (0.362533+0.05,-0.30) -- (0.362533-0.05,+0.30); % Y
    \pgfplotsinvokeforeach{0,...,12} { % level 13 and 14 are empty
      \pgfplotscolormapaccess[0:14]{#1}{jet}
      \def\TEMP{\definecolor{mycolor#1}{rgb}}
      \expandafter\TEMP\expandafter{\pgfmathresult}
      \pgfplotstablegetelem{3}{timeSteps}\of\myloadedtable
      \edef\fnameX{\fname:%
        step:\pgfplotsretval:%
        level:\pgfplotsarrayvalueofelem#1\of\contourlevels.tex}
      \addplot[contour prepared={draw color=mycolor#1}, forget plot] table \fnameX;
      \addlegendimage{line legend,color=mycolor#1}
      \addlegendentry{%
        \SI{\pgfplotsarrayvalueofelem#1\of\contourlevels}{\micro\meter/\second}}
    };

    \nextgroupplot[semithick, xlabel = {
      \pgfplotstablegetelem{4}{timeOffsets}\of\myloadedtable%
      $t_0 +
      \SI[round-mode=places,round-precision=2]{\pgfplotsretval}{\second}$
    }]
    \draw[color=gray!20] (0.162533,-0.30) -- (0.162533,+0.30);           % X
    \draw[color=gray!20] (0.362533+0.05,-0.30) -- (0.362533-0.05,+0.30); % Y
    \pgfplotsinvokeforeach{0,...,12} { % level 13 and 14 are empty
      \pgfplotscolormapaccess[0:14]{#1}{jet}
      \def\TEMP{\definecolor{mycolor#1}{rgb}}
      \expandafter\TEMP\expandafter{\pgfmathresult}
      \pgfplotstablegetelem{4}{timeSteps}\of\myloadedtable
      \edef\fnameX{\fname:%
        step:\pgfplotsretval:%
        level:\pgfplotsarrayvalueofelem#1\of\contourlevels.tex}
      \addplot[contour prepared={draw color=mycolor#1}] table \fnameX;
    };

    \nextgroupplot[semithick, xlabel = {
      \pgfplotstablegetelem{5}{timeOffsets}\of\myloadedtable%
      $t_0 +
      \SI[round-mode=places,round-precision=2]{\pgfplotsretval}{\second}$
    }]
    \draw[color=gray!20] (0.162533,-0.30) -- (0.162533,+0.30);           % X
    \draw[color=gray!20] (0.362533+0.05,-0.30) -- (0.362533-0.05,+0.30); % Y
    \pgfplotsinvokeforeach{0,...,12} { % level 13 and 14 are empty
      \pgfplotscolormapaccess[0:14]{#1}{jet}
      \def\TEMP{\definecolor{mycolor#1}{rgb}}
      \expandafter\TEMP\expandafter{\pgfmathresult}
      \pgfplotstablegetelem{5}{timeSteps}\of\myloadedtable
      \edef\fnameX{\fname:%
        step:\pgfplotsretval:%
        level:\pgfplotsarrayvalueofelem#1\of\contourlevels.tex}
      \addplot[contour prepared={draw color=mycolor#1}] table \fnameX;
    };
  \end{groupplot}
\end{tikzpicture}
