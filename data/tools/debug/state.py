import numpy as np

def lift_singularity(c, x):
    def local_lift(y): 
        if (y <= 0):
            return -c
        else:
            return -np.expm1(c * y) / y
    
    return np.array([local_lift(y) for y in x])


def aging_law(params, initial_alpha, v, time_steps):
        #auto tangentVelocity = velocity_field[localToGlobal_[i]];
        #tangentVelocity[0] = 0.0;
        #double const V = tangentVelocity.two_norm();
        
    alpha = [initial_alpha]

    for i,tau in enumerate(time_steps):
        mtoL = -tau / params['L']
        next_alpha = np.log(np.exp(alpha[-1] + v[i] * mtoL) + params['V0'] * lift_singularity(mtoL, v[i]))
        alpha.append(next_alpha)

    return alpha