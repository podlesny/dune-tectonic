import numpy as np
import matplotlib.pyplot as plt

def diffplot(v1, v2, title):
    v_diff = v1 - v2
    t = np.linspace(0, len(v_diff)-1, len(v_diff), endpoint=True)

    # plot
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(t, v1, color='black', linestyle='--')
    ax.plot(t, v2, color='gray', linestyle='--')
    ax.plot(t, v_diff, color='red', linestyle='-')
    ax.set_ylabel(title)
    #-------------------------

    fig.canvas.draw()