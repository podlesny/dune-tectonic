import numpy as np

def outliers(data):
    index = [i for i,x in enumerate(data) if np.abs(x)==np.inf]
    val = np.zeros(len(index))
    return [index, val]