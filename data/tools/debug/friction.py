import numpy as np

def truncated_friction(params, v, alpha):
   vmin = params['V0'] / np.exp( ( params['mu0'] + params['b'] * np.array(alpha)) / params['a'] )
   clipped_v = (v/vmin).clip(1)
   return params['a'] * np.log(clipped_v)

   #return (params['a'] * np.log(v/params['V0']) - params['mu0'] - (params['b']/params['a'])*np.array(alpha) ).clip(0)