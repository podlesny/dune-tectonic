#!/usr/bin/env bash

set -e

rr() {
    echo "$(date)" Running: Rscript $@
    Rscript --vanilla --default-packages=grDevices,methods,stats,utils tools/$@
}

# contours
rr 2d-velocity-contours.R

# dip
rr 2d-dip-contours.R

# iterations / adaptivity
rr 2d-performance.R

# box plot (one half)
rr 2d-event-writer.R

# fpi data
rr 2d-fpi-tolerance.R

date
