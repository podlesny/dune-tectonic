source('tools/support/findQuakes.R')

finalTime           <- 1000 # s
convergenceVelocity <- 5e-5 # m/s

paste.    <- function(...) paste(..., sep='.')
pasteColon<- function(...) paste(..., sep=':')

directories <- ini::read.ini('config.ini')$directories
dir.create(directories[['output']], recursive=TRUE, showWarnings=FALSE)
for (basedir in c("rtol=1e-5_diam=1e-2")) {
  dir          <- file.path(directories[['simulation']],
                            '3d-lab', basedir)
  h5file       <- h5::h5file(file.path(dir, 'output.h5'), 'r')
  relativeTime <- h5file['relativeTime'][]
  realTime     <- finalTime * relativeTime

  velocityProxy<- h5file['/frictionalBoundary/velocity']

  ## We are interested in an enlarged time range around actual events here,
  ## (and no other quantities!) hence we pass a very low velocity here.
  quakes <- findQuakes(1e-6 + convergenceVelocity, velocityProxy,
                       ## Note: We only look at the last 2000 timesteps here because
                       ## we're only interested in the last few events. This
                       ## dramatically reduces RAM usage and runtime.
                       indices = seq(dim(velocityProxy)[1]-2000,
                                     dim(velocityProxy)[1]), c(1,3))
  quakes$beginning <- realTime[quakes$beginningIndex]
  quakes$ending    <- realTime[quakes$endingIndex]
  quakes$duration  <- quakes$ending - quakes$beginning
  numQuakes        <- nrow(quakes)

  relaxedTime <- extendrange(c(quakes[[numQuakes-2,'beginning']],
                               quakes[[numQuakes,  'ending']]), f=0.02)
  threeQuakeTimeMask <- (realTime > relaxedTime[[1]]) & (realTime < relaxedTime[[2]])
  plotMask   <- threeQuakeTimeMask
  plotIndices<- which(plotMask)
  plotIndices<- c(plotIndices[1]-1,plotIndices,plotIndices[length(plotIndices)]+1)

  write(relaxedTime[[1]],
        file.path(directories[['output']],
                  paste.(pasteColon('timeframe', 'min', 'threequakes',
                                    basedir), 'tex')))
  write(relaxedTime[[2]],
        file.path(directories[['output']],
                  paste.(pasteColon('timeframe', 'max', 'threequakes',
                                    basedir), 'tex')))

  timeWindow = realTime[plotIndices]

  relativeTau          <- h5file['relativeTimeIncrement'][]
  fixedPointIterations <- h5file['/iterations/fixedPoint/final'][]
  multiGridIterations  <- h5file['/iterations/multiGrid/final'][]
  write.csv(data.frame(time = timeWindow,
                       timeIncrement = finalTime * relativeTau[plotIndices],
                       fixedPointIterations = fixedPointIterations[plotIndices],
                       multiGridIterations = multiGridIterations[plotIndices]),
            file.path(directories[['output']],
                      paste.(pasteColon('3d-performance', basedir), 'csv')),
            row.names = FALSE, quote = FALSE)
  h5::h5close(h5file)
}
