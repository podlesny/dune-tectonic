import array as ar

def slip_beginnings(x):
    # returns indicies i for which x[i-1]=0 and x[i]>0
    starts = ar.array('i', (0 for i in range(len(x))))
    prev = False

    length = 0
    for i in range(len(x)):
        if (not prev and x[i]):
            starts[length] = i
            length += 1
        prev = x[i]
    return starts[:length]
