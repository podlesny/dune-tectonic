writeContours <- function (contours, level, file) {
  file.create(file)
  for (cl in contours[sapply(contours, function(x) x$level) == level]) {
    conn <- file(file, 'a')
    write.table(cbind(cl$x, cl$y, level),
                file = file, row.names = FALSE, col.names = FALSE,
                append = TRUE)
    writeLines("\n", conn)
    close(conn)
  }
}
