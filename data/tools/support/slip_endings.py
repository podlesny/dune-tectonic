import array as ar

def slip_endings(x):
    # returns indicies i for which x[i-1]>0 and x[i]=0
    ends = ar.array('i', (0 for i in range(len(x))))
    prev = False

    length = 0
    for i in range(len(x)):
        if (prev and not x[i]):
            ends[length] = i
            length += 1
        prev = x[i]
    return ends[:length]