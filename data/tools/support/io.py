import configparser as cp
import os
import h5py

def remove_comment(str, char = "#"):
    split_str = str.split(char, 1)
    return split_str[0]

# tag = 'simulation', 'experiment', 'output'
def read_h5file(tag = 'simulation'):
    # read config ini
    config = cp.ConfigParser()
    config_path = os.path.join('tools/config.ini')
    config.read(config_path)
    path = config.get('directories', tag)

    # read hdf5 output file
    h5path = os.path.join(path)

    return h5py.File(os.path.join(h5path, 'output.h5'), 'r')

# tag = 'simulation', 'experiment', 'output'
def read_params(file_name, tag = 'simulation'):
    # read config ini
    config = cp.ConfigParser()
    config_path = os.path.join('tools/config.ini')
    config.read(config_path)
    path = config.get('directories', tag)

    # read cfg parameter file
    params = cp.ConfigParser()
    params_path = os.path.join(path, file_name)
    params.read(params_path)

    #with open(params_path) as stream:
    #    params.read_string("[top]\n" + stream.read()) 

    out = {
        'L'  : float(remove_comment(params.get('boundary.friction', 'L'))),
        'V0' : float(remove_comment(params.get('boundary.friction', 'V0'))),
        'mu0': float(remove_comment(params.get('boundary.friction', 'mu0'))), 
        'a'  : float(remove_comment(params.get('boundary.friction.weakening', 'a'))), 
        'b'  : float(remove_comment(params.get('boundary.friction.weakening', 'b'))),
        'bodyCount' : int(remove_comment(params.get('problem', 'bodyCount'))),
        'finalTime' : float(remove_comment(params.get('problem', 'finalTime'))),
        'finalVelocity' : float(remove_comment(params.get('boundary.dirichlet', 'finalVelocity')))
    }

    return out