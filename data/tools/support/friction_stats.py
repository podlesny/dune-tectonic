import numpy as np
import matplotlib.pyplot as plt

def friction_stats(h5file, body_ID, FINAL_TIME, patch=[], interval=[], TANGENTIAL_COORDS=1):
    body = 'body' + str(body_ID)   # 'frictionalBoundary'     'body' + str(body_ID)

    coords = np.array(h5file[body + '/coordinates'])
    if len(patch) == 0:
        patch = np.linspace(0, len(coords)-1, len(coords), endpoint=True, dtype='int8')

    # read time
    time = np.array(h5file['relativeTime']) * FINAL_TIME
    time = np.delete(time, 0)
    if len(interval) == 0:
        interval = [0, FINAL_TIME]
    t = [i for i in range(len(time)) if time[i]>=interval[0] and time[i]<=interval[1]]
    #t = t[::5]
    time = time[t]

    fig = plt.figure()

    # velocity data
    v = abs(np.array(h5file[body + '/velocity']))
    v_t = v[t,:,TANGENTIAL_COORDS]
    v_tx = v_t[:,patch]
    # statistics
    avg_v = np.average(v_tx, axis=1)
    min_v = np.min(v_tx, axis=1)
    max_v = np.max(v_tx, axis=1)
    # plot
    ax_slip = fig.add_subplot(1, 1, 1)
    #ax_slip.plot(time, min_v, color='gray', linestyle='--')
    ax_slip.plot(time, avg_v, color='black', linestyle='-')
    #ax_slip.plot(time, max_v, color='gray', linestyle='--')
    ax_slip.set_ylabel('slip rate V [m/s]')
    ax_slip.set_xlabel('time t [s]')
    ax_slip.set_yscale('log')
    #ax_slip.set_ylim([1e-6,1e-2])
    #-------------------------

    print(np.min(min_v))
    print(np.average(avg_v))
    print(np.max(max_v))

    # state
    #states = np.array(h5file[body + '/state'])
    #states_t = states[t,:]
    #states_tx = states_t[:,patch]
    # statistics
    #avg_states = np.average(states_tx, axis=1)
    #min_states = np.min(states_tx, axis=1)
    #max_states = np.max(states_tx, axis=1)
    # plot
    #ax_state = fig.add_subplot(2, 1, 2)
    #ax_state.plot(time, min_states, color='gray', linestyle='--')
    #ax_state.plot(time, avg_states, color='black', linestyle='-')
    #ax_state.plot(time, max_states, color='gray', linestyle='--')
    #ax_state.set_ylabel('state')
    #ax_state.set_xlabel('time [s]')
    #-------------------------

    # friction coefficient
    # friction_coeff = np.array(h5file[body + '/coefficient'])
    # friction_coeff_t = friction_coeff[t,:]
    # friction_coeff_tx = friction_coeff_t[:,patch]
    # # statistics
    # avg_friction_coeff = np.average(friction_coeff_tx, axis=1)
    # min_friction_coeff = np.min(friction_coeff_tx, axis=1)
    # max_friction_coeff = np.max(friction_coeff_tx, axis=1)
    # # plot
    # ax_friction = fig.add_subplot(3, 1, 3)
    # ax_friction.plot(time, min_friction_coeff, color='gray', linestyle='--')
    # ax_friction.plot(time, avg_friction_coeff, color='black', linestyle='-')
    # ax_friction.plot(time, max_friction_coeff, color='gray', linestyle='--')
    # ax_friction.set_ylabel('friction coefficient')
    #-------------------------

    fig.canvas.draw()