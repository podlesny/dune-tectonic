from itertools import combinations


def square_distance(x, y):
    return sum([(xi-yi)**2 for xi, yi in zip(x, y)])


def max_distance(points):
    max_square_distance = 0
    max_pair = (0, 0)
    for pair in combinations(points, 2):
        sq_dist = square_distance(*pair)

        if sq_dist > max_square_distance:
            max_square_distance = sq_dist
            max_pair = pair
    return max_pair
