import array as ar
import numpy as np

from .slip_beginnings import slip_beginnings
from .slip_endings import slip_endings

def peak_slip(quake_start, quake_end, velocities):
    quake_v = velocities[quake_start-1:quake_end]
    max_v = np.max(quake_v, axis=1)
    return [quake_start+np.argmax(max_v), np.max(max_v)]

#def rupture_width

def find_quakes(rate, threshold_rate):
    slipping_times = rate > threshold_rate

    quake_starts = slip_beginnings(slipping_times)
    quake_ends = slip_endings(slipping_times)

    print(quake_starts)
    print(quake_ends)

    # remove incomplete quakes
    min_len = min(len(quake_starts), len(quake_ends))
    quake_ends = quake_ends[0:min_len]
    quake_starts = quake_starts[0:min_len]

    return [quake_starts, quake_ends]

    peak_v = np.zeros(min_len)
    quake_times = np.zeros(min_len)
    for i in range(min_len):
        [quake_times[i], peak_v[i]] = peak_slip(quake_starts[i], quake_ends[i], v_tx)
    print("peak slip velocity: " + str(peak_v.mean()) + " +- " + str(peak_v.std()))

    recurrence_time = np.zeros(min_len-1)
    for i in range(min_len-1):
        recurrence_time[i] = time[int(quake_times[i+1])] - time[int(quake_times[i])]
    print("recurrence time: " + str(recurrence_time.mean()) + " +- " + str(recurrence_time.std()))

   
