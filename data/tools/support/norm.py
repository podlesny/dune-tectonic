import numpy as np


def norm(x):
    size = x.shape
    res = np.zeros((size[0], size[1]))

    for time_step in range(size[0]):
        for vertex in range(size[1]):
            res[time_step, vertex] = np.linalg.norm(x[time_step, vertex, :])

    return res
