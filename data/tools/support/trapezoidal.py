# x: vector of domain values
# y: vector of function values f(x)
def trapezoidal(x, y) :
    # returns integral of (x, y = f(x)) calculated with trapezoidal rule 
	res = 0.0
	for i in range(1, len(x)) :
		res += (x[i] - x[i - 1]) * (y[i] + y[i - 1]) / 2.0
	return res