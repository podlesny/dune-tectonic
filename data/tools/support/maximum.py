import numpy as np


def maximum(x):
    size = x.shape
    res = np.zeros(size[0])

    for time_step in range(size[0]):
        res[time_step] = max(res[time_step], max(x[time_step, :]))

    return res
