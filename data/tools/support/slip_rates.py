import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

SORT_COORD = 0

def local_to_global(coords, SORT_COORD=0):
    return coords[:, SORT_COORD].argsort()

def slip_rates(h5file, body_ID, FINAL_TIME, patch=[], interval=[], TANGENTIAL_COORDS=1):
    body = 'body' + str(body_ID)   # 'frictionalBoundary'     'body' + str(body_ID)

    coords = np.array(h5file[body + '/coordinates'])
    if len(patch) == 0:
        patch = np.linspace(0, len(coords)-1, len(coords), endpoint=True, dtype='int8')

    # local to global
    local_global = local_to_global(coords, SORT_COORD) 
    local_global = local_global[patch]

    patch_size = coords[local_global[-1], SORT_COORD] - coords[local_global[0], SORT_COORD]

    # read time
    time = np.array(h5file['relativeTime']) * FINAL_TIME
    time = np.delete(time, 0)
    if len(interval) == 0:
        interval = [0, FINAL_TIME]
    t = [i for i in range(len(time)) if time[i]>=interval[0] and time[i]<=interval[1]]
    time = time[t]

    fig = plt.figure()

    # velocity data
    v = abs(np.array(h5file[body + '/velocity']))
    v_t = v[t,:,TANGENTIAL_COORDS]
    v_tx = v_t[:,local_global]

    # plot
    ax_slip = fig.add_subplot(1, 1, 1)
    pos = ax_slip.imshow(v_tx, interpolation='nearest', cmap=cm.Greys_r, aspect='auto', origin='lower', extent=[0, patch_size, time[0], time[-1]])
    #ax_slip.set_ylim([time[-1], time[0]])
    ax_slip.set_ylabel('time [s]')
    ax_slip.set_xlabel('horizontal coordinate [m]')
    fig.colorbar(pos, ax=ax_slip)

    #ax_slip.set_yscale('log')
   # ax_slip.set_ylim([1e-6,1e-2])
    #-------------------------

    fig.canvas.draw()