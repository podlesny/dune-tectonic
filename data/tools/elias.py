import configparser as cp
import os
import numpy as np
import csv
import h5py
import matplotlib.pyplot as plt

from debug.outliers import outliers
from debug.friction import truncated_friction
from debug.state import aging_law
from debug.diffplot import diffplot

from support.maximum import maximum
from support.norm import norm
from support.find_quakes import find_quakes
from support.slip_beginnings import slip_beginnings
from support.slip_endings import slip_endings
from support.max_distance import max_distance

from support.iterations import iterations
from support.friction_stats import friction_stats
from support.slip_rates import slip_rates

def build_patch(coords, percentage):
    x_coords = coords[:, 0]
    xmin = np.min(x_coords)
    xmax = np.max(x_coords)
    delta_x = (1 - percentage)*(xmax - xmin)/2

    xmin = xmin + delta_x
    xmax = xmax - delta_x

    return [i for i in range(len(x_coords)) if x_coords[i]>=xmin and x_coords[i]<=xmax]

FINAL_TIME = 1000  # s
FINAL_VELOCITY = 5e-5  # m/s
THRESHOLD_VELOCITY = 0.5*FINAL_VELOCITY  # 1000e-6 + FINAL_VELOCITY

TANGENTIAL_COORDS = 0

# friction params
params = {
    'L'  : 1e-5,
    'V0' : 1e-6,
    'mu0': 0.6,
    'a'  : 0.010,
    'b'  : 0.015
}

# read config ini
config = cp.ConfigParser()
config_path = os.path.join('tools/config.ini')
config.read(config_path)
sim_path = config.get('directories', 'simulation')
exp_path = config.get('directories', 'experiment')
out_path = config.get('directories', 'output')

# read hdf5 output file
h5path = os.path.join(sim_path)
h5file = h5py.File(os.path.join(h5path, 'output.h5'), 'r')

interval = [994, 996] 

print(list(h5file.keys()))
print(list(h5file['frictionalBoundary'].keys()))

iterations(h5file, FINAL_TIME, interval)

coords = np.array(h5file['frictionalBoundary/coordinates'])
patch = build_patch(coords, 1.0)

friction_stats(h5file, 0, FINAL_TIME, patch, interval, TANGENTIAL_COORDS)
slip_rates(h5file, 0, FINAL_TIME, patch, interval, TANGENTIAL_COORDS)

plt.show()

h5file.close()