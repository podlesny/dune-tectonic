#!/usr/bin/env bash

set -e

rr() {
    echo "$(date)" Running: Rscript $@
    Rscript --vanilla --default-packages=grDevices,methods,stats,utils tools/$@
}

# box plot (one half)
rr comparison:lab-sim.R

date
