import configparser as cp
import os
import numpy as np
import csv
import h5py
import matplotlib.pyplot as plt

from debug.outliers import outliers
from debug.friction import truncated_friction
from debug.state import aging_law
from debug.diffplot import diffplot

from support.maximum import maximum
from support.norm import norm
from support.find_quakes import find_quakes
from support.slip_beginnings import slip_beginnings
from support.slip_endings import slip_endings
from support.max_distance import max_distance

from support.io import read_h5file
from support.io import read_params

from support.iterations import iterations
from support.friction_stats import friction_stats
from support.slip_rates import slip_rates

from support.find_quakes import find_quakes

def build_patch(coords, percentage):
    x_coords = coords[:, 0]
    xmin = np.min(x_coords)
    xmax = np.max(x_coords)
    delta_x = (1 - percentage)*(xmax - xmin)/2

    xmin = xmin + delta_x
    xmax = xmax - delta_x

    return [i for i in range(len(x_coords)) if x_coords[i]>=xmin and x_coords[i]<=xmax]

# read problem parameters
params = read_params('strikeslip.cfg')

TANGENTIAL_COORDS = 1
FINAL_TIME = params['finalTime']
NBODIES = params['bodyCount']
THRESHOLD_VELOCITY = 1e-2 

h5file = read_h5file()
print(list(h5file.keys()))

interval = [0.5*FINAL_TIME, FINAL_TIME]

iterations(h5file, FINAL_TIME, interval)

for body_ID in range(NBODIES):
    body = 'body' + str(body_ID)

    if body not in h5file:
        continue

    coords = np.array(h5file[body + '/coordinates'])
    patch = build_patch(coords, 1.0)

    print("patch length: " + str(len(patch)))

    friction_stats(h5file, body_ID, FINAL_TIME, patch, interval)
    #slip_rates(h5file, body_ID, FINAL_TIME, patch, interval, TANGENTIAL_COORDS)

    #[quake_starts, quake_ends] = find_quakes(h5file, body_ID, FINAL_TIME, patch, interval, THRESHOLD_VELOCITY, TANGENTIAL_COORDS)
plt.show()

h5file.close()