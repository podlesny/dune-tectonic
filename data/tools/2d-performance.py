import ConfigParser as cp
import os
import numpy as np
import csv
import h5py

from support.find_quakes import find_quakes

NBODIES = 2
FINAL_TIME = 1000  # s
FINAL_VELOCITY = 1e-5  # m/s
THRESHOLD_VELOCITY = 0.5*FINAL_VELOCITY  # 1000e-6 + FINAL_VELOCITY

TANGENTIAL_COORDS = 1

# read config ini
config = cp.ConfigParser()
config_path = os.path.join('config.ini')
config.read(config_path)
sim_path = config.get('directories', 'simulation')
exp_path = config.get('directories', 'experiment')
out_path = config.get('directories', 'output')

# create output directory
out_dir = os.path.join(out_path)
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

h5path = os.path.join(sim_path)
h5file = h5py.File(os.path.join(h5path, 'output.h5'), 'r')

# read time
relative_time = np.array(h5file['relativeTime'])
real_time = relative_time * FINAL_TIME


  velocityProxy<- h5file['/frictionalBoundary/velocity']

  ## We are interested in an enlarged time range around actual events here,
  ## (and no other quantities!) hence we pass a very low velocity here.
  quakes <- findQuakes(1e-6 + convergenceVelocity, velocityProxy,
                       indices = 1:dim(velocityProxy)[1], 1)
  quakes$beginning <- realTime[quakes$beginningIndex]
  quakes$ending    <- realTime[quakes$endingIndex]
  quakes$duration  <- quakes$ending - quakes$beginning
  numQuakes        <- nrow(quakes)

  relaxedTime <- extendrange(c(quakes[[numQuakes-2,'beginning']],
                               quakes[[numQuakes,  'ending']]), f=0.02)
  threeQuakeTimeMask <- (realTime > relaxedTime[[1]]) & (realTime < relaxedTime[[2]])
  plotMask   <- threeQuakeTimeMask
  plotIndices<- which(plotMask)
  plotIndices<- c(plotIndices[1]-1,plotIndices,plotIndices[length(plotIndices)]+1)

  write(relaxedTime[[1]],
        file.path(directories[['output']],
                  paste.(pasteColon('timeframe', 'min', 'threequakes',
                                    basedir), 'tex')))
  write(relaxedTime[[2]],
        file.path(directories[['output']],
                  paste.(pasteColon('timeframe', 'max', 'threequakes',
                                    basedir), 'tex')))

  timeWindow = realTime[plotIndices]

  relativeTau          <- h5file['relativeTimeIncrement'][]
  fixedPointIterations <- h5file['/iterations/fixedPoint/final'][]
  multiGridIterations  <- h5file['/iterations/multiGrid/final'][]
  write.csv(data.frame(time = timeWindow,
                       timeIncrement = finalTime * relativeTau[plotIndices],
                       fixedPointIterations = fixedPointIterations[plotIndices],
                       multiGridIterations = multiGridIterations[plotIndices]),
            file.path(directories[['output']],
                      paste.(pasteColon('2d-performance', basedir), 'csv')),
            row.names = FALSE, quote = FALSE)
  h5::h5close(h5file)
